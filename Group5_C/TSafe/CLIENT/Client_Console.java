package tsafe.client;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import tsafe.common_datastructures.LatLonBounds;
import tsafe.common_datastructures.TSAFEProperties;
import tsafe.common_datastructures.communication.ComputationResults;
import tsafe.common_datastructures.Flight;
import tsafe.common_datastructures.FlightPlan;
import tsafe.common_datastructures.FlightTrack;
import tsafe.common_datastructures.Trajectory;
import tsafe.common_datastructures.Point4D;
import tsafe.common_datastructures.communication.UserParameters;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import javax.swing.undo.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;

public class Client_Console extends JFrame implements ActionListener{
	/**
     * Show options
     */
    public static final int SHOW_ALL        = 0;
    public static final int SHOW_SELECTED   = 1;
    public static final int SHOW_WITH_PLAN  = 2;
    public static final int SHOW_CONFORMING = 3;
    public static final int SHOW_BLUNDERING = 4;
    public static final int SHOW_NONE       = 5;
    
    /**
     * Drawing Constants
     */
    private static final Color FIX_COLOR      = Color.green;
    private static final Color NO_PLAN_COLOR  = Color.yellow;
    private static final Color BLUNDER_COLOR  = Color.red;
    private static final Color CONFORM_COLOR  = Color.white;
    private static final Color ROUTE_COLOR    = Color.blue;
    private static final Color TRAJ_COLOR     = Color.magenta;
    private static final int FLIGHT_RADIUS    = 5;
    private static final int FIX_RADIUS       = 3;
    private static final int ROUTE_FIX_RADIUS = 4;
    
    // Conversion constants
    private static final int MILLISECONDS_PER_HOUR = 60 * 60 * 1000;
    private static final double METERS_PER_NAUTICAL_MILE = 1852.0;
    private static final double FEET_PER_METER = 3.2808;
    private static final double KNOTS_PER_MACH = 644.62216;
    
    int fixOpt = 0;
    int flightOpt = 0;
    int routeOpt = 0;
    int trajOpt = 0;
    
    SimpleAttributeSet attrib = new SimpleAttributeSet();
    SimpleAttributeSet attribFlight = new SimpleAttributeSet();
    SimpleAttributeSet attribFlightLabel = new SimpleAttributeSet();
    SimpleAttributeSet attribFlightBlunderingLabel = new SimpleAttributeSet();
    SimpleAttributeSet attribBlunderingLabel = new SimpleAttributeSet();
    
    JTextPane textPane;
    AbstractDocument doc;
    String newline = "\n";
    JTextField command;
    
    InputParser parser;

    UserParameters parameters;
    
    ClientEngine client;
    
    String[] selectedFlights = null;
    
    /**
     * Data to draw to the screen
     */
    private Collection fixes;
    private Collection flights  = new LinkedList();
    private Collection blunders = new LinkedList();
    private Map flight2TrajMap = new HashMap(); 

    /**
     * Flags triggering the display of certain flight pane items
     */
    private int showFixes  = SHOW_ALL, showFlights      = SHOW_ALL,
                showRoutes = SHOW_ALL, showTrajectories = SHOW_ALL;

    /**
     * The selected flights
     */
    //private Collection selectedFlights = new Vector();
        
    public Client_Console(ClientEngine client) {
        super("TSafe Text Client");
	this.client = client;
	this.parameters = client.getParameters();
	//this.flightList = new FlightList();
	initAttributes();
        //Create the text pane and configure it.
        textPane = new JTextPane();
        textPane.setEditable(false);
        textPane.setMargin(new Insets(5,5,5,5));
        StyledDocument styledDoc = textPane.getStyledDocument();
        if (styledDoc instanceof AbstractDocument) {
            doc = (AbstractDocument)styledDoc;
        } else {
            System.err.println("Text pane's document isn't an AbstractDocument!");
            System.exit(-1);
        }
        JScrollPane scrollPane = new JScrollPane(textPane);
        scrollPane.setPreferredSize(new Dimension(800, 600));

        //JTextArea to hold command line
        command = new JTextField(76);
        command.setEditable(true);
        command.addActionListener(this);
        
        //JButton to process enters
        JButton enterButton = new JButton("Enter");
        enterButton.addActionListener(this);

        //JSplitPane for output and command line
        JSplitPane commandPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, command, enterButton);
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPane, commandPane);
        splitPane.setOneTouchExpandable(true);

        getContentPane().add(splitPane, BorderLayout.CENTER);
        //initDocument();
	
    
         //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Create and set up the window.
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				parameters.textOpen = false;
				
				if(!parameters.guiOpen)
				  System.exit(0);
			}
		});
		
        //Display the window.
        pack();
        setVisible(true);
	parser = new InputParser(this);
   
    }
    
    public void setFix(int fixType){
      fixOpt = fixType;
    }
    
    public void setFlight(int flightType){
      flightOpt = flightType;
    }
    
    public void setRoute(int routeType){
      routeOpt = routeType;    
    }
    
    public void setTraj(int trajType){
      trajOpt = trajType;    
    }
    
    public UserParameters getParameters(){
    	return parameters;
    }
	public void actionPerformed(ActionEvent e) {
		displayCommand(); 
	}
    
    private void displayCommand(){
    	SimpleAttributeSet attrib = new SimpleAttributeSet();
		StyleConstants.setFontFamily(attrib, "SansSerif");
        StyleConstants.setFontSize(attrib, 16);

		//appendText(command.getText() + newline, attrib);
		parser.parse(command.getText());
		command.setText("");
    }

    private void initAttributes(){
       	
	StyleConstants.setFontFamily(attrib, "SansSerif");
        StyleConstants.setFontSize(attrib, 12);

	StyleConstants.setFontFamily(attribFlight, "SansSerif");
        StyleConstants.setFontSize(attribFlight, 16);
	StyleConstants.setBold(attribFlight, true);
	
	StyleConstants.setFontFamily(attribFlightLabel, "SansSerif");
        StyleConstants.setFontSize(attribFlightLabel, 16);    
	StyleConstants.setBold(attribFlightLabel, true);
    
    	StyleConstants.setFontFamily(attribFlightBlunderingLabel, "SansSerif");
        StyleConstants.setFontSize(attribFlightBlunderingLabel, 16);    
	StyleConstants.setBold(attribFlightBlunderingLabel, true);
	StyleConstants.setForeground(attribFlightBlunderingLabel, Color.red );
    
        StyleConstants.setFontFamily(attribBlunderingLabel, "SansSerif");
        StyleConstants.setFontSize(attribBlunderingLabel, 12);    
	StyleConstants.setBold(attribBlunderingLabel, true);
	StyleConstants.setForeground(attribBlunderingLabel, Color.red );
    }
    
    protected void initDocument() {
        String initString[] =
                { "Welcome to TSafe.",
                  "Line 2.",
                  "Line 3.",
                  "Line 4.",
                  "Line 5.",
                  "Line 6." };

        SimpleAttributeSet[] attrs = initAttributes(initString.length);

        try {
            for (int i = 0; i < initString.length; i ++) {
                doc.insertString(doc.getLength(), initString[i] + newline,
                        attrs[i]);
            }
        } catch (BadLocationException ble) {
            System.err.println("Couldn't insert initial text.");
        }
    }

    protected SimpleAttributeSet[] initAttributes(int length) {
        //Hard-code some attributes.
        SimpleAttributeSet[] attrs = new SimpleAttributeSet[length];

        attrs[0] = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attrs[0], "SansSerif");
        StyleConstants.setFontSize(attrs[0], 16);

        attrs[1] = new SimpleAttributeSet(attrs[0]);
        StyleConstants.setBold(attrs[1], true);

        attrs[2] = new SimpleAttributeSet(attrs[0]);
        StyleConstants.setItalic(attrs[2], true);

        attrs[3] = new SimpleAttributeSet(attrs[0]);
        StyleConstants.setFontSize(attrs[3], 20);

        attrs[4] = new SimpleAttributeSet(attrs[0]);
        StyleConstants.setFontSize(attrs[4], 12);

        attrs[5] = new SimpleAttributeSet(attrs[0]);
        StyleConstants.setForeground(attrs[5], Color.red);

        return attrs;
    }
    
    private void appendText(String appendMe, SimpleAttributeSet attrib){
    	try {
            doc.insertString(doc.getLength(), appendMe, attrib);
	    
	} catch (BadLocationException ble) {
            System.err.println("Couldn't append text.");
        }
    }

	//public FlightMap getFlightMap(){
	//	return guiClient.getFlightMap();
	//}
	
    public void updateWindow(ComputationResults results) {
				
//System.out.println(parameters.cmLateralThreshold);
		setFlights(results.getFlights());
		setBlunders(results.getBlunders());
		setFlightTrajectoryMap(results.getFlight2TrajectoryMap());
//	appendText("test" + newline, attrib);
//	System.out.println("In Console Update");

	
		Iterator i = (results.getFlights()).iterator();
		
		while(i.hasNext()){
			Flight iFlight = (Flight)i.next();
			//System.out.println("in Text Console Checking Flight " + iFlight.getAircraftId());
			switch(flightOpt){
				case SHOW_ALL:
					showFlight(results, iFlight);
					break;
					
				case SHOW_SELECTED:
					if(selectedFlights != null){
					  for(int x=0; x < selectedFlights.length; x++){ 
					    if(iFlight.getAircraftId().equals(selectedFlights[x])){
					      showFlight(results, iFlight);
					      x = selectedFlights.length;
					    }
					  }
					}
					break;
					
				case SHOW_WITH_PLAN:
					if(iFlight.getFlightPlan() != null)
						showFlight(results, iFlight);
					break;
				case SHOW_CONFORMING:
					if(!(blunders.contains(iFlight)))
						showFlight(results, iFlight);
					break;

				case SHOW_BLUNDERING:
					if(blunders.contains(iFlight))
						showFlight(results, iFlight);
					break;
					
				case SHOW_NONE:
					break;
					
			}
		}	
  	}
	
	private void showFlight(ComputationResults results, Flight iFlight){
			FlightTrack iFlightTrack = iFlight.getFlightTrack();
			
		
			appendText("Flight: ", attribFlightLabel);
			if(blunders.contains(iFlight))
			  appendText(iFlight.getAircraftId(), attribFlightBlunderingLabel);
			else
			  appendText(iFlight.getAircraftId(), attribFlight);
			appendText("\t" + getLat(iFlightTrack.getLatitude()), attrib);
			appendText("\t" + getLon(iFlightTrack.getLongitude()), attrib);
			appendText("\t" + getAlt(iFlightTrack.getAltitude()), attrib);
			appendText("\t" + getSpeed(iFlightTrack.getSpeed()), attrib);
			appendText("\t" + getHeading(iFlightTrack.getHeading()), attrib);
			appendText(newline, attrib);
			
			switch(routeOpt){
				case SHOW_ALL:
					showRoute(results, iFlight);
					break;
					
				case SHOW_SELECTED:
					if(selectedFlights != null){
					  for(int x=0; x < selectedFlights.length; x++){ 
					    if(iFlight.getAircraftId().equals(selectedFlights[x])){
					      showRoute(results, iFlight);
					      x = selectedFlights.length;
					    }
					  }
					}
					break;
					
				case SHOW_CONFORMING:
					if(!(blunders.contains(iFlight)))
						showRoute(results, iFlight);
					break;

				case SHOW_BLUNDERING:
					if(blunders.contains(iFlight))
						showRoute(results, iFlight);
					break;
					
				case SHOW_NONE:
					break;
					
			}

			switch(trajOpt){
				case SHOW_ALL:
					showTraj(results, iFlight);
					break;
					
				case SHOW_SELECTED:
					if(selectedFlights != null){
					  for(int x=0; x < selectedFlights.length; x++){ 
					    if(iFlight.getAircraftId().equals(selectedFlights[x])){
					      showTraj(results, iFlight);
					      x = selectedFlights.length;
					    }
					  }
					}
							
					break;
					
				case SHOW_WITH_PLAN:
					if(iFlight.getFlightPlan() != null)
						showTraj(results, iFlight);
					break;
				case SHOW_CONFORMING:
					if(!(blunders.contains(iFlight)))
						showTraj(results, iFlight);
					break;
					
				case SHOW_BLUNDERING:
					if(blunders.contains(iFlight))
						showTraj(results, iFlight);
					break;
					
				case SHOW_NONE:
				//System.out.println("show none?");
					break;
					
			}
			

			appendText(newline, attrib);					
	
	}

	private void showTraj(ComputationResults results, Flight iFlight){
			List iPoints = ((Trajectory)((results.getFlight2TrajectoryMap()).get(iFlight))).pointList();
			appendText("Trajectories:", attrib);
			
			Iterator j = iPoints.iterator();
			
			while( j.hasNext() ){
				Point4D jPoint = (Point4D)j.next();
				appendText("\t" + getLat(jPoint.getLatitude()), attrib);
				appendText("  " + getLon(jPoint.getLongitude()), attrib);			
			}
			appendText(newline, attrib);
			if(blunders.contains(iFlight)){
				appendText("Conformance Status:\t", attrib);
				appendText("blundering", attribBlunderingLabel);
				appendText(newline, attrib);				
			}
	}
	
	private void showRoute(ComputationResults results, Flight iFlight){
			FlightPlan iFlightPlan = iFlight.getFlightPlan();
			
			if(iFlightPlan != null){
				appendText("Flight Plan:\t", attrib);
				appendText("\t" + getAlt(iFlightPlan.getAssignedAltitude()), attrib);
				appendText("\t" + getSpeed(iFlightPlan.getAssignedSpeed()), attrib);
				appendText(newline + "\t\t" + (iFlightPlan.getRoute()).toString(), attrib);
				appendText(newline, attrib);
			}	
	
	}	
	String getLat(double dbl){
	
		Double d = new Double(dbl);
		String degrees = d.toString();
		
		degrees =  degrees.substring(0, ((degrees.indexOf("."))+2));
/*		
		String degrees = (d.toString()).substring(0,2);
		
		d = new Double(dbl*60);
		String dString = d.toString();
		String minutes = dString.substring(((dString.indexOf("."))+1), dString.length());
		if(minutes.length() > 2)
		  minutes = minutes.substring(0,2);
		
		if(dbl < 0)
		  return (degrees.substring(1,degrees.length()) + "\u00B0 " + minutes + "'S");
		else
		  return (degrees + "\u00B0 " + minutes + "'N");
*/
		if(dbl < 0)
		  return (degrees.substring(1,degrees.length()) + "\u00B0S");
		else
		  return (degrees + "\u00B0N");
	}
	
	String getLon(double dbl){
	
		Double d = new Double(dbl);

		String degrees = d.toString();
		
		degrees =  degrees.substring(0, ((degrees.indexOf("."))+2));
/*
		String degrees = (d.toString()).substring(0,3);
		
		d = new Double(dbl*60);
		String dString = d.toString();
		String minutes = dString.substring(((dString.indexOf("."))+1), dString.length());
		if(minutes.length() > 2)
		  minutes = minutes.substring(0,2);
		
		if(dbl < 0)
		  return (degrees.substring(1,degrees.length()) + "\u00B0 " + minutes + "'W");
		else
		  return (degrees + "\u00B0 " + minutes + "'E");
*/
		if(dbl < 0)
		  return (degrees.substring(1,degrees.length()) + "\u00B0W");
		else
		  return (degrees + "\u00B0E");
	}
	
	String getAlt(double dbl){
	
		Double d = new Double(dbl);
		String dString = d.toString();
		
		String alt = dString.substring(0, ((dString.indexOf("."))+2)) + " m";

		
		return alt;
	}
	
	String getSpeed(double dbl){
        //return METERS_PER_NAUTICAL_MILE * knots /*(nautical miles per hour)*/ / MILLISECONDS_PER_HOUR	
	  Double dKnots = new Double(dbl * MILLISECONDS_PER_HOUR / METERS_PER_NAUTICAL_MILE);
	  Double dKPH = new Double(dbl * MILLISECONDS_PER_HOUR / 1000);
	  String sKnots = dKnots.toString();
	  String sKPH = dKPH.toString();
	  
	  sKnots = sKnots.substring(0, ((sKnots.indexOf("."))+2));
	  sKPH = sKPH.substring(0, ((sKPH.indexOf("."))+2));
	  return (sKPH + " km/h | " + sKnots + " knots");
	}
	
	String getHeading(double dbl){
	
		Double d = new Double((dbl/Math.PI)*180);
		String degrees = d.toString();
		
		degrees =  degrees.substring(0, ((degrees.indexOf("."))+2));
		
		return (degrees + "\u00B0");
		
	}
	
	public String[] getSelectedFlights(){
		return selectedFlights;
	}
	
	public void setSelectedFlights( String[] s ){
		//this.selectedFlights.clear();
		this.selectedFlights = s;
	}
	
	public void setSelectedFlightsAll(){

	  Iterator i = flights.iterator();
	  selectedFlights = new String[flights.size()];
	  
	  for(int x=0; x < flights.size(); x++){
	    selectedFlights[x] = new String(((Flight)i.next()).getAircraftId());
	  }
	}
	public void setFlights(Collection f){
		this.flights = f;
	}
		
	public void setBlunders(Collection b){
		this.blunders = b;
	}
	
	public void setFlightTrajectoryMap(Map f){
		this.flight2TrajMap = f;
	}
	
	public void show(String option, int action){
		if(option.equals("Fixes")){
			fixOpt = action;
		
		}else if(option.equals("Flight")){
			flightOpt = action;
		
		}else if(option.equals("Route")){
			routeOpt = action;
			
		}else if(option.equals("Trajectory")){
			trajOpt = action;
			
		}
	}

	public void throwError(){
		appendText(newline + "InvalidCommand" + newline + newline, attribBlunderingLabel);
	}	
	
	public void test(){
    	SimpleAttributeSet attrib = new SimpleAttributeSet();
	StyleConstants.setFontFamily(attrib, "SansSerif");
        StyleConstants.setFontSize(attrib, 16);
		  
		appendText("test" + newline, attrib);
	}
}
