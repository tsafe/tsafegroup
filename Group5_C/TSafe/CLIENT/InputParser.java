/*
	InputParser.Java
	By Daniel Ahrnsbrak
*/

package tsafe.client;
import java.util.*;
import tsafe.common_datastructures.communication.UserParameters;
import tsafe.common_datastructures.Flight;
import java.util.Comparator;

public class InputParser {
    //private static final Comparator FLIGHT_COMPARATOR = new FlightComparator();
    /**
     * Show options
     */
    public static final int SHOW_ALL        = 0;
    public static final int SHOW_SELECTED   = 1;
    public static final int SHOW_WITH_PLAN  = 2;
    public static final int SHOW_CONFORMING = 3;
    public static final int SHOW_BLUNDERING = 4;
    public static final int SHOW_NONE       = 5;
    
    public static final int OPT_NOTHING         = 0;
	public static final int OPT_SELECT_ALL      = 1;
    public static final int OPT_SELECT_INPUT    = 2;
    public static final int OPT_SHOW_FIXES      = 3;
    public static final int OPT_SHOW_FLIGHTS    = 4;
    public static final int OPT_SHOW_ROUTES     = 5;
    public static final int OPT_SHOW_TRAJ       = 6;
    
	String input;
	LinkedList tokens;
	boolean isValid;

	Client_Console client;

	UserParameters parameters;
	
	//FlightMap flightMap;
		
public InputParser(Client_Console client){
  this.client = client;
  this.parameters = client.getParameters();
  //this.flightMap = client.getFlightMap();
}

public void test(){
  client.test();
}

/**
 * Sets the flights to be shown in the list
 */
public void setFlights(Collection flights) {
    // Sort the flights and set the data of the JList
    //Collection newFlights = flights;
    //Collections.sort(newFlights, FLIGHT_COMPARATOR);
    //client.setSelectedFlights(newFlights);
    //client.setGuiSelectedFlights();
}

    /**
     * Comparator used for sorting flights by their ids
     */
    private static class FlightComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            String flightId1 = ((Flight)o1).getAircraftId();
            String flightId2 = ((Flight)o2).getAircraftId();
            return flightId1.compareTo(flightId2);
        }
    }
public void parse(String in)
{
	tokens = new LinkedList();
	isValid = true;
	input = in;
	Tokenizer(input, " ");

	//System.out.println(tokens);
	if ((((String)tokens.get(0)).equals("select")) && (tokens.size() >= 2))
	{
		if (((String)tokens.get(1)).length() > 0)
		{
			if (((String)tokens.get(1)).equals("*"))
			{
				//client.setGuiAllSelectedFlights();
			    parameters.textUpdate = OPT_SELECT_ALL;
			    client.setSelectedFlightsAll();
			}
			else
			{
				String[] selectedFlightID = null;
				String[] temp = null;
				for (int i = 1; i < tokens.size(); i++)
				{
					temp = new String[i];
					if(i != 1){
					  for(int x=0; x < i-1; x++){
					    temp[x] = selectedFlightID[x];
					  }
					  
					  temp[i-1] = ((String)tokens.get(i));
		  
					  selectedFlightID = temp;
					  
					}else{
					  selectedFlightID = new String[1];
					  selectedFlightID[0] = ((String)tokens.get(1));
					}
					//selectedFlightID = new Vector();
					//selectedFlightID[i-1] = new String(((String)tokens.get(i)));
				}
				client.setSelectedFlights(selectedFlightID);
				//client.setGuiSelectedFlights();
				parameters.textUpdate = OPT_SELECT_INPUT;
			}
		}
		else
		{
			isValid = false;
		}
	}
	else if ((((String)tokens.get(0)).equals("show")) && (tokens.size() == 3))
	{
		if (((String)tokens.get(1)).equals("fixes"))
		{
			if (((String)tokens.get(2)).equals("all"))
			{
			    parameters.showOption = SHOW_ALL;
			}
			else if (((String)tokens.get(2)).equals("none"))
			{
			    parameters.showOption = SHOW_NONE;
			}
			else 
			{
				isValid = false;
			}
			
			if (isValid){
			    client.setFix(parameters.showOption);
			    parameters.textUpdate = OPT_SHOW_FIXES;
			}
		}
		else if (((String)tokens.get(1)).equals("flights"))
		{
			if (((String)tokens.get(2)).equals("all"))
			{
			    parameters.showOption = SHOW_ALL;
			}
			else if (((String)tokens.get(2)).equals("none"))
			{
			    parameters.showOption = SHOW_NONE;
			}
			else if (((String)tokens.get(2)).equals("selected"))
			{
				parameters.showOption = SHOW_SELECTED;
			}
			else if (((String)tokens.get(2)).equals("withplans"))
			{
				//client.setGuiShowFlights( SHOW_WITH_PLAN );
				parameters.showOption = SHOW_WITH_PLAN;
			}
			else if (((String)tokens.get(2)).equals("conforming"))
			{
				//client.setGuiShowFlights( SHOW_CONFORMING );
				parameters.showOption = SHOW_CONFORMING;
			}
			else if (((String)tokens.get(2)).equals("blundering"))
			{
				//client.setGuiShowFlights( SHOW_BLUNDERING );
				parameters.showOption = SHOW_BLUNDERING;
			}
			else
			{
				isValid = false;
			}
			
			if (isValid){
			    client.setFlight(parameters.showOption);
			    parameters.textUpdate = OPT_SHOW_FLIGHTS;
			}
		}
		else if (((String)tokens.get(1)).equals("routes"))
		{
			if (((String)tokens.get(2)).equals("all"))
			{
				parameters.showOption = SHOW_ALL;
			}
			else if (((String)tokens.get(2)).equals("none"))
			{
				parameters.showOption = SHOW_NONE;
			}
			else if (((String)tokens.get(2)).equals("selected"))
			{
				parameters.showOption = SHOW_SELECTED;
			}
			else if (((String)tokens.get(2)).equals("conforming"))
			{
				parameters.showOption = SHOW_CONFORMING;
			}
			else if (((String)tokens.get(2)).equals("blundering"))
			{
				parameters.showOption = SHOW_BLUNDERING;
			}
			else
			{
				isValid = false;
			}
			
			if (isValid){
			    client.setRoute(parameters.showOption);
			    parameters.textUpdate = OPT_SHOW_ROUTES;
			}
		}
			
		else if (((String)tokens.get(1)).equals("trajectories"))
		{
			if (((String)tokens.get(2)).equals("all"))
			{
			    parameters.showOption = SHOW_ALL;
			}
			else if (((String)tokens.get(2)).equals("none"))
			{
			    parameters.showOption = SHOW_NONE;
			}
			else if (((String)tokens.get(2)).equals("selected"))
			{
				parameters.showOption = SHOW_SELECTED;
			}
			else if (((String)tokens.get(2)).equals("conforming"))
			{
				parameters.showOption = SHOW_CONFORMING;
			}
			else if (((String)tokens.get(2)).equals("blundering"))
			{
			    parameters.showOption = SHOW_BLUNDERING;
			}
			else if (((String)tokens.get(2)).equals("withplans"))
			{
			    parameters.showOption = SHOW_WITH_PLAN;
			}
			else
			{
				isValid = false;
			}
			
			if (isValid){
			    client.setTraj(parameters.showOption);
			    parameters.textUpdate = OPT_SHOW_TRAJ;
			}
		}else{
		  isValid = false;
		}
			
	}
	else if ((tokens.size() == 4) && (((String)tokens.get(0)).equals("set")) && (((String)tokens.get(1)).equals("parameter")))
	{
			if (((String)tokens.get(2)).equals("threslat"))
			{
			  if(tokens.size() == 4){
			    String value = (String)tokens.get(3);
			    try{
			      parameters.cmLateralThreshold = Double.parseDouble(value);
			    }catch(NumberFormatException e){
			      isValid = false;
			    }
			  }else{
			    isValid = false;
			  }
			  
			}		
			else if (((String)tokens.get(2)).equals("thresver"))
			{
			  if(tokens.size() == 4){
			    String value = (String)tokens.get(3);
			    try{
			      parameters.cmVerticalThreshold = Double.parseDouble(value);
			    }catch(NumberFormatException e){
			      isValid = false;
			    }
			  }else{
			    isValid = false;
			  }
			  			
			}
			else if (((String)tokens.get(2)).equals("thresang"))
			{
			  if(tokens.size() == 4){
			    String value = (String)tokens.get(3);
			    try{
			      parameters.cmAngularThreshold = Double.parseDouble(value);
			    }catch(NumberFormatException e){
			      isValid = false;
			    }
			  }else{
			    isValid = false;
			  }
			  			
			}
			else if (((String)tokens.get(2)).equals("thresspe"))
			{
			  if(tokens.size() == 4){
			    String value = (String)tokens.get(3);
			    try{
			      parameters.cmSpeedThreshold = Double.parseDouble(value);
			    }catch(NumberFormatException e){
			      isValid = false;
			    }
			  }else{
			    isValid = false;
			  }
			  			
			}
			else if (((String)tokens.get(2)).equals("thresres"))
			{
			  if(tokens.size() == 4){
			    String value = (String)tokens.get(3);
			    try{
			      parameters.cmResidualThreshold = Double.parseDouble(value);
			    }catch(NumberFormatException e){
			      isValid = false;
			    }
			  }else{
			    isValid = false;
			  }
			  
			}
			else if (((String)tokens.get(2)).equals("horiztim"))
			{
			  if(tokens.size() == 4){
			    String value = (String)tokens.get(3);
			    try{
			      parameters.tsTimeHorizon = Long.parseLong(value);
			    }catch(NumberFormatException e){
			      isValid = false;
			    }
			  }else{
			    isValid = false;
			  }
			  			
			}
			else
			{
				isValid = false;
			}
				
	}
	else if ((tokens.size() == 3) && (((String)tokens.get(0)).equals("enable")) && (((String)tokens.get(1)).equals("parameter")))
	{
			if (((String)tokens.get(2)).equals("threslat"))
			{
			  parameters.cmLateralWeightOn = true;
			}		
			else if (((String)tokens.get(2)).equals("thresver"))
			{
			  parameters.cmVerticalWeightOn = true;
			}
			else if (((String)tokens.get(2)).equals("thresang"))
			{
			  parameters.cmAngularWeightOn = true;
			}
			else if (((String)tokens.get(2)).equals("thresspe"))
			{
			  parameters.cmSpeedWeightOn = true;
			}
			else if (((String)tokens.get(2)).equals("thresres"))
			{
			  parameters.cmLateralWeightOn = true;
			}
			else
			{
				isValid = false;
			}
	}
	else if ((tokens.size() == 3) && (((String)tokens.get(0)).equals("disable")) && (((String)tokens.get(1)).equals("parameter")))
	{
			if (((String)tokens.get(2)).equals("threslat"))
			{
			  parameters.cmLateralWeightOn = false;
			}		
			else if (((String)tokens.get(2)).equals("thresver"))
			{
			  parameters.cmVerticalWeightOn = false;
			}
			else if (((String)tokens.get(2)).equals("thresang"))
			{
			  parameters.cmAngularWeightOn = false;
			}
			else if (((String)tokens.get(2)).equals("thresspe"))
			{
			  parameters.cmSpeedWeightOn = false;
			}
			else if (((String)tokens.get(2)).equals("thresres"))
			{
			  parameters.cmLateralWeightOn = false;
			}
			else
			{
				isValid = false;
			}	
	}
	else
	{
	isValid = false;
	}
	
	if(!isValid)
		client.throwError();
}

	
public void Tokenizer(String s, String delimiter)
{
   String sub = null;
   int i =0;
   int j =s.indexOf(delimiter);  // First substring

	//System.out.println("Parsing" + s + " AND " + delimiter);
   while( j >= 0) {
   	tokens.add(s.substring(i,j));
   	i = j + 1;
	  	j = s.indexOf(delimiter, i);   // Rest of substrings
   }
   tokens.add(s.substring(i)); // Last substring
}


}