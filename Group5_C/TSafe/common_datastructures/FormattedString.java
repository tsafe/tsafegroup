
package tsafe.common_datastructures;

import javax.swing.text.SimpleAttributeSet;

/**
* Stores a string and some attributes for text formatting.
*/
public class FormattedString {

   private String[] strings;
   private SimpleAttributeSet[] attributes;

   public FormattedString(String stringsIn[], SimpleAttributeSet attributesIn[]) {
       strings = new String[] stringsIn;
       attributes = new SimpleAttributeSet[] attributesIn;
   }

   public String[] getStrings()  {return strings;}
   public SimpleAttributeSet[] getAttributes() {return attributes;}
}
