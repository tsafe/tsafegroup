/*
 * Created on Dec 3, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package tsafe.client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author ppuszkie
 *
 * A window adapter used to notify the server that a client was closed down.
 * If all clients close, the server shuts down.
 */
public class CloseWindowAdapter extends WindowAdapter
{
	private ClientInterface client ;
	
	/**
	 * Builds a new CloseWindow Adapter
	 * 
	 * @param client The Client Interface of the window associated with this listener.
	 */
	public CloseWindowAdapter( ClientInterface client ) 
	{
		super() ;
		this.client = client ;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	public void windowClosing(WindowEvent e) {
		client.closeGUI() ;
	}
}
