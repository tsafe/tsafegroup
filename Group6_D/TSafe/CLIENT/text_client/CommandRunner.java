package tsafe.client.text_client;

import java.util.Map;
import java.util.TreeMap;

import tsafe.client.text_client.command_handlers.Command_Handler;

/*
 * Created on Nov 30, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
/**
 * @author ppuszkie
 *
 * This class serves two functions: 
 *    1) It keeps a list of all command_handlers associated with its
 * 		TextWindow.
 * 	  2) It parses raw input strings and passes argument arrays to the
 * 		appropriate command_handler.
 */



public class CommandRunner {
	
	Map commands = new TreeMap() ;
	
	/**
	 * Constructs a new CommandRunner
	 * 
	 * @param chIn an array of Command_Handler objects. These are the 
	 * 		Command_Handlers that the CommandRunner will use.
	 */
	public CommandRunner(Command_Handler [] chIn )
	{
		if ( chIn != null )
		{
			for ( int x = 0 ; x < chIn.length ; x ++ )
			{
				Command_Handler cur = chIn[x] ;
				if ( commands.containsKey( cur.command ))
					commands.remove(cur.command) ;
				commands.put(cur.command, cur) ;
			}
		}
	}
	
	/**
	 * This function parses the input string and, if it is potentially
	 * valid, passes the string to a CommandRunner
	 * 
	 * @param the user's input string
	 * @return an error or the CommandRunner's return value
	 */
	public String parseAndRun(String input) 
	{
		String[] splitInput = input.split("\\s+") ;
		if ( splitInput.length > 0 )
		{
			String command = splitInput[0];
			String [] arguments = new String [splitInput.length - 1] ;
			for (int x = 0 ; x < splitInput.length - 1 ; x ++ ) 
			{
				arguments[x] = splitInput [x+1] ;
			}
			
			if ( commands.containsKey(command) )
			{
				return ( ((Command_Handler)commands.get(command)).result(arguments) ) ;
			}
			else
			{
				return "ERROR: COMMAND \"" + command + "\" IS NOT IN MY DICTIONARY." ;
			}
		}
		else
		{
			return "ERROR: INPUT DID NOT CONTAIN A COMMAND" ;
		}
	}
}
