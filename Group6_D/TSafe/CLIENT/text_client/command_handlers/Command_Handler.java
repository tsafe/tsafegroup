/*
 * Created on Nov 30, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package tsafe.client.text_client.command_handlers;

import tsafe.client.text_client.TextWindow;

/**
 * @author ppuszkie
 *
 * Abstract class to be used as an interface for all Command Handlers.
 * Extend this class in order to allow TSAFE to handle new commands.
 * 
 */
public abstract class Command_Handler {
	
	/**
	 * The <code>TextWindow</code> object with which this command
	 * handler is interacting.
	 */
	public TextWindow tw ;
	
	/**
	 * The string which triggers this command. It must be equal to the
	 * first word in a string of user input which this handler is to
	 * interperet. For example, if this command handler were to be 
	 * reacting to "set fixes none" then this string would be set to "set"
	 */
	public String command ;
	
	/**
	 * This constructor requires the command string and TextWindow
	 * associated with this handler.
	 * 
	 * @param command
	 * @param tw
	 */
	public Command_Handler( String command, TextWindow tw ) 
	{
		this.command = command ;
		this.tw = tw ;
	}
	/**
	 * This method must be overloaded by all extentions of the Command_Handler
	 * class. It uses the Arguments array to alter the state of <code>tw</code>
	 * and returns output for the user. 
	 * 
	 * @param Arguments This array of Strings contains all arguments which the 
	 * TextWindow recieved from the user. More specifically, it contains all 
	 * sets of non-whitespace characters, less the first word, which were entered
	 * by the user. e.g.: Given a user input of "set fixes none" this array would
	 * contain two elements as so: ["fixes"]["none"]
	 * 
	 * @return The output to be displayed for the user.
	 */
	public abstract String result(String [] Arguments) ;
}
