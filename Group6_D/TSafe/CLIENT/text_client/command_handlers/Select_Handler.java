/*
 * Created on Nov 30, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package tsafe.client.text_client.command_handlers;

import java.util.LinkedList;

import tsafe.client.text_client.TextWindow;

/**
 * @author ppuszkie
 *
 * This Command_Handler extention manages all versions of the 'select' command.
 */
public class Select_Handler extends Command_Handler {
	/**
	 * Default Constructor. Sets the Command_Handlers command string to "select" 
	 * 
	 * @param tw the TextWindow associated with this
	 * instantiation.
	 */
	public Select_Handler(TextWindow tw) 
	{
		super("select", tw) ;
	}
	/**
	 * This method processes all "select" events. It will select all flights
	 * listed in the arguments array or all flights if Arguments[0]='*'
	 * 
	 * @param Arguments This array of Strings contains all arguments which the 
	 * TextWindow recieved from the user. A valid input array will take the following form:
	 * <table border=1><tr><th>Array Index</th><th>Possible Values</th></tr>
	 * <tr><td align="center"> 0 </td><td align="left"><DL>
	 * 			<B>*</B> - Select All Flights<br>
	 * 			<B><flight1></B> - Select the flight whose id is <flight1><br>
	 * </table>
	 * <tr><td align="center"> 1 - N </td><td align="left"> if Arguments[0] contains<br>a flightID, you can continue <BR>filling further arguments with flightIDs.<BR>All flightIDs in the array<BR>will be selected."
	 * An array with size <1 will produce an error in the return value
	 * An array with Arguments[0]='*' and size != 1 will produce an error in the return value
	 * 
	 * @return The output to be displayed for the user.
	 */
	public String result(String[] Arguments) {
		if ( Arguments.length <= 0 )
		{
			return "ERROR: Too few arguemtns for SELECT command\n" +
					"TRY:\t\"select <flightid>\"\n" +
					"\t\"select <flightid1> <flightid2> ... <flightidN>\"\n" +
					"\t\"select *\"\n" +
					"\n\t* words between < and > are parameters / variables ";
		}
		else
		{
			if ( Arguments.length == 1 && Arguments[0].equals("*"))
			{
				tw.setFlights(null) ;
				return "SELECTING ALL FLIGHTS" ;
			}
			else
			{
				LinkedList flights = new LinkedList() ;
				
				String retVal = "" ;
				for ( int x = 0 ; x < Arguments.length ; x ++ )
				{
					retVal += "SELECTING " + Arguments[x] + "\n" ;
					flights.add(Arguments[x]) ;
				}
				tw.setFlights(flights) ;
				return retVal ;
			}
		}
	}
}
