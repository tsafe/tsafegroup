/*
 * Created on Dec 3, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package tsafe.client.text_client.command_handlers;

import tsafe.client.text_client.TextWindow;

/**
 * @author ppuszkie
 *
 * This Command_Handler extention manages all versions of the 'set' command.
 */
public class Set_Handler extends Command_Handler {
	
		/**
		 * Default Constructor. Sets the Command_Handlers command string to "set" 
		 * 
		 * @param tw the TextWindow associated with this
		 * instantiation.
		 */
		public Set_Handler(TextWindow tw)
		{
			super("set", tw) ;
		}
		
		/**
		 * This method processes all "set" events. It will set the parameter
		 * in Arguments[1] to the value in Arguments[2].
		 * 
		 * 
		 * @param Arguments This array of Strings contains all arguments which the 
		 * TextWindow recieved from the user. A valid input array will take the following form:
		 * <table border=1><tr><th>Array Index</th><th>Possible Values</th></tr>
		 * <tr><td align="center"> 0 </td><td align="center"> parameter </td></tr> 
		 * <tr><td align="center"> 1 </td><td align="left"><DL>
		 * 			<B>threslat</B> - Lateral Threshold<br>
		 * 			<B>thresver</B> - Vertical Threshold<br>
		 * 			<B>thresang</B> - Angular Threshold<br>
		 * 			<B>thresspe</B> - Speed Threshold<br>
		 * 			<B>thresres</B> - Residual Threshold<br>
		 * 			<B>horiztim</B> - Time Horizon<br></dt></tr>
		 * <tr><td align="center"> 2 </td><td align="center"> a number </td></tr>
		 * </table>
		 * Arguments[0] must be the string "parameter".
		 * Arguments[2] must be a valid integer or decimal number.
		 * An array with size != 3 will produce an error in the return value
		 * 
		 * @return The output to be displayed for the user.
		 */
		public String result(String[] Arguments) {
			double argVal = Double.NaN;
			double prevVal = 0 ;
			
			if ( Arguments.length != 3 )
			{
				return "ERROR: Incorrect arguments for set paramete command\n" +
						"TRY:\t\"set parameter <parameter name> <value>\"\n" +
						"\twhere <parameter name> can be replaced with any of the following:\n" +
						"\t\tthreslat (Lateral Threshold)\n" +
						"\t\tthresver (Vertical Threshold)\n" +
						"\t\tthresang (Angular Threshold)\n" +
						"\t\tthresspe (Speed Threshold)\n" +
						"\t\tthresres (Residual Threshold)\n" +
						"\t\thoriztim (Time Horizon)\n" +
						"\tand <value> is the numerical value for that parameter.\n" ;
			}
			try
			{
				argVal = Double.valueOf(Arguments[2] ).doubleValue() ;
			}
			catch ( NumberFormatException e)
			{
				return  "ERROR: Incorrect arguments for set paramete command\n" +
						"The <value> argument must be a number. \n" ;
			}
			if ( argVal != Double.NaN && Arguments[1].equals("parameter"))
			{
				String retVal = new String() ;
				if ( Arguments[1].equals("threslat"))
				{
					prevVal = tw.client.getParameters().cmLateralThreshold ;
					tw.client.getParameters().cmLateralThreshold = argVal ;
					return "LATERAL THRESHOLD CHANGED FROM\t" + prevVal + "\tTO\t" + argVal ;
				}
				else if ( Arguments[1].equals("thresver"))
				{
					prevVal = tw.client.getParameters().cmVerticalThreshold ;
					tw.client.getParameters().cmVerticalThreshold = argVal ;
					return "VERTICAL THRESHOLD CHANGED FROM\t" + prevVal + "\tTO\t" + argVal ;
				}
				else if ( Arguments[1].equals("thresang"))
				{
					prevVal = tw.client.getParameters().cmAngularThreshold ;
					tw.client.getParameters().cmAngularThreshold = argVal ;
					return "ANGULAR THRESHOLD CHANGED FROM\t" + prevVal + "\tTO\t" + argVal ;
				}
				else if ( Arguments[1].equals("thresspe"))
				{
					prevVal = tw.client.getParameters().cmSpeedThreshold ;
					tw.client.getParameters().cmSpeedThreshold = argVal ;
					return "SPEED THRESHOLD CHANGED FROM\t" + prevVal + "\tTO\t" + argVal ;
				}
				else if ( Arguments[1].equals("thresres"))
				{
					prevVal = tw.client.getParameters().cmResidualThreshold ;
					tw.client.getParameters().cmResidualThreshold = argVal ;
					return "RESIDUAL THRESHOLD CHANGED FROM\t" + prevVal + "\tTO\t" + argVal ;
				}
				else if ( Arguments[1].equals("horiztim"))
				{
					prevVal = tw.client.getParameters().tsTimeHorizon ;
					tw.client.getParameters().tsTimeHorizon = (long)argVal ;
					return "TIME HORIZON CHANGED FROM\t" + prevVal + "\tTO\t" + argVal ;
				}
				else
				{
					return 	"ERROR: Incorrect arguments for set paramete command\n" +
							"TRY:\t\"set parameter <parameter name> <value>\"\n" +
							"\twhere <parameter name> can be replaced with any of the following:\n" +
							"\t\tthreslat (Lateral Threshold)\n" +
							"\t\tthresver (Vertical Threshold)\n" +
							"\t\tthresang (Angular Threshold)\n" +
							"\t\tthresspe (Speed Threshold)\n" +
							"\t\tthresres (Residual Threshold)\n" +
							"\t\thoriztim (Time Horizon)\n" +
							"\tand <value> is the numerical value for that parameter.\n" ;
				}
			}
			else
				return "ERROR: Incorrect arguments for set paramete command\n" +
				"TRY:\t\"set parameter <parameter name> <value>\"\n" +
				"\twhere <parameter name> can be replaced with any of the following:\n" +
				"\t\tthreslat (Lateral Threshold)\n" +
				"\t\tthresver (Vertical Threshold)\n" +
				"\t\tthresang (Angular Threshold)\n" +
				"\t\tthresspe (Speed Threshold)\n" +
				"\t\tthresres (Residual Threshold)\n" +
				"\t\thoriztim (Time Horizon)\n" +
				"\tand <value> is the numerical value for that parameter.\n" ;
		}
}
