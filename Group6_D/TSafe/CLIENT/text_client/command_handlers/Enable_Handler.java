/*
 * Created on Dec 3, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package tsafe.client.text_client.command_handlers;

import tsafe.client.text_client.TextWindow;

/**
 * @author ppuszkie
 *
 * This Command_Handler extention manages all versions of the 'enable' command.
 */
public class Enable_Handler extends Command_Handler {

		/**
		 * Default Constructor. Sets the Command_Handlers command string to "disable" 
		 * 
		 * @param tw the TextWindow associated with this
		 * instantiation.
		 */
		public Enable_Handler(TextWindow tw)
		{
			super("enable", tw) ;
		}
		
		/**
		 * This method processes all "enable" events. It will enable the parameter
		 * in Arguments[1].
		 * 
		 * @param Arguments This array of Strings contains all arguments which the 
		 * TextWindow recieved from the user. A valid input array will take the following form:
		 * <table border=1><tr><th>Array Index</th><th>Possible Values</th></tr>
		 * <tr><td align="center"> 0 </td><td align="center"> parameter </td></tr> 
		 * <tr><td align="center"> 1 </td><td align="left"><DL>
		 * 			<B>threslat</B> - Lateral Threshold<br>
		 * 			<B>thresver</B> - Vertical Threshold<br>
		 * 			<B>thresang</B> - Angular Threshold<br>
		 * 			<B>thresspe</B> - Speed Threshold<br></dt></tr>
		 * </table>
		 * Arguments[0] must be the string "parameter".
		 * An array with size != 2 will produce an error in the return value
		 * 
		 * @return The output to be displayed for the user.
		 */
		public String result(String[] Arguments) {
			
			if ( Arguments.length != 2 )
			{
				return "ERROR: Incorrect arguments for enable command\n" +
						"TRY:\t\"enable parameter <parameter name>\"\n" +
						"\twhere <parameter name> can be replaced with any of the following:\n" +
						"\t\tthreslat (Lateral Threshold)\n" +
						"\t\tthresver (Vertical Threshold)\n" +
						"\t\tthresang (Angular Threshold)\n" +
						"\t\tthresspe (Speed Threshold)\n" ;
			}
			else if ( Arguments[0].equals("parameter"))
			{
				String retVal = new String() ;
				if ( Arguments[1].equals("threslat"))
				{
					tw.client.getParameters().cmLateralWeightOn = true ;
					return "LATERAL THRESHOLD ENABLED" ;
				}
				else if ( Arguments[1].equals("thresver"))
				{
					tw.client.getParameters().cmVerticalWeightOn = true ;
					return "VERTICAL THRESHOLD ENABLED" ;
				}
				else if ( Arguments[1].equals("thresang"))
				{
					tw.client.getParameters().cmAngularWeightOn = true ;
					return "ANGULAR THRESHOLD ENABLED" ;
				}
				else if ( Arguments[1].equals("thresspe"))
				{
					tw.client.getParameters().cmSpeedWeightOn = true ;
					return "SPEED THRESHOLD ENABLED" ;
				}
				else
				{
					return "ERROR: Incorrect arguments for enable command\n" +
					"TRY:\t\"enable parameter <parameter name>\"\n" +
					"\twhere <parameter name> can be replaced with any of the following:\n" +
					"\t\tthreslat (Lateral Threshold)\n" +
					"\t\tthresver (Vertical Threshold)\n" +
					"\t\tthresang (Angular Threshold)\n" +
					"\t\tthresspe (Speed Threshold)\n" ;
				}
			}
			else
				return "ERROR: Incorrect arguments for enable command\n" +
				"TRY:\t\"enable parameter <parameter name>\"\n" +
				"\twhere <parameter name> can be replaced with any of the following:\n" +
				"\t\tthreslat (Lateral Threshold)\n" +
				"\t\tthresver (Vertical Threshold)\n" +
				"\t\tthresang (Angular Threshold)\n" +
				"\t\tthresspe (Speed Threshold)\n" ;
		}
}	


