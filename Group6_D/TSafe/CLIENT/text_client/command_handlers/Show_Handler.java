/*
 * Created on Nov 30, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package tsafe.client.text_client.command_handlers;

import tsafe.client.text_client.TextWindow;

/**
 * @author ppuszkie
 *
 * This Command_Handler extention manages all versions of the 'show' command.
 */
public class Show_Handler extends Command_Handler {
	/**
	 * Default Constructor. Sets the Command_Handlers command string to "show" 
	 * 
	 * @param tw the TextWindow associated with this
	 * instantiation.
	 */
    public static final int SHOW_ALL        = 0;
    public static final int SHOW_SELECTED   = 1;
    public static final int SHOW_WITH_PLAN  = 2;
    public static final int SHOW_CONFORMING = 3;
    public static final int SHOW_BLUNDERING = 4;
    public static final int SHOW_NONE       = 5;
	
	private String fixesArgs 		= "[all|none]" ;
	private String flightsArgs		= "[all|selected|withplans|conforming|blundering|none]" ;
	private String routesArgs		= "[all|selected|conforming|blundering|none]" ;
	private String trajectoriesArgs	= "[all|selected|withplans|conforming|blundering|none]" ;
	
	public Show_Handler(TextWindow tw)
	{
		super("show", tw) ;
	}
	
	/**
	 * This method processes all "show" events. Arguments[0]defines the dataset,
	 * Arguments[1] defines the subset to be displayed.
	 * 
	 * @param Arguments This array of Strings contains all arguments which the 
	 * TextWindow recieved from the user. A valid input array will take the following form:
	 * <table border=1><tr><th>Arguments[0]</th><th>Arguments[1]</th></tr>
	 * <tr><td align="center"> fixes </td><td> [ all | none ] </td></tr>
	 * <tr><td align="center"> flights </td><td> [ all | selected | withplans | conforming | blundering | none ] </td></tr>
	 * <tr><td align="center"> routes </td><td> [ all | selected | conforming | blundering | none ] </td></tr>
	 * <tr><td align="center"> trajectories </td><td> [ all | selected | withplans | conforming | blundering | none ] </td></tr>
	 * </table>
	 * <tr><td align="center"> 1 - N </td><td align="left"> if Arguments[0] contains<br>a flightID, you can continue <BR>filling further arguments with flightIDs.<BR>All flightIDs in the array<BR>will be selected."
	 * An array with size <1 will produce an error in the return value
	 * An array with Arguments[0]='*' and size != 1 will produce an error in the return value
	 * 
	 * @return The output to be displayed for the user.
	 */
	public String result(String[] Arguments) {
		if ( Arguments.length <= 0 )
		{
			return 	"ERROR: Too few arguemtns for SHOW command\n" +
					"TRY:\t\"show fixes " + fixesArgs + "\"\n" +
					"\t\"show flights " + flightsArgs + "\"\n" +
					"\t\"show routes " + routesArgs + "\"\n" +
					"\t\"show trajectories " + trajectoriesArgs + "\"\n" +
					"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
					
		}
		else
		{
			String showWhat = Arguments[0] ;
			String retVal = "" ; 
			
			if (showWhat.equals("fixes"))
			{
				if ( Arguments.length != 2 )
				{
					retVal = 	"ERROR: show fixes only takes 1 argument, not " + Arguments.length + ".\n" +
								"\tPlease try one of these instead:\n\n" +
								"\t\t\"show fixes " + fixesArgs + "\"\n" +
								"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
				}
				else
				{
					String myArg = Arguments[1] ;
					if ( myArg.equals("all"))
					{
						tw.setShowFixes(SHOW_ALL) ;
					}
					else if ( myArg.equals("none"))
					{
						tw.setShowFixes(SHOW_NONE) ;
					}
					else
					{
						retVal = 	"ERROR: I don't know how to show fixes \"" + myArg + "\".\n" +
									"\tPlease try one of these instead:\n\n" +
									"\t\t\"show fixes " + fixesArgs + "\"\n" +
									"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
						
					}
				}
			} else if (showWhat.equals("flights"))
			{
				if ( Arguments.length != 2 )
				{
					retVal = "ERROR: show flights only takes 1 argument.\n" +
								"\tPlease try one of these instead:\n\n" +
								"\t\t\"show flights " + flightsArgs + "\"\n" +
								"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
				}
				else
				{
					String myArg = Arguments[1] ;
					if ( myArg.equals("all"))
					{
						tw.setShowFlights(SHOW_ALL) ;
					}
					else if ( myArg.equals("selected"))
					{
						tw.setShowFlights(SHOW_SELECTED) ;
					}
					else if ( myArg.equals("withplans"))
					{
						tw.setShowFlights(SHOW_WITH_PLAN) ;
					}
					else if ( myArg.equals("conforming"))
					{
						tw.setShowFlights(SHOW_CONFORMING) ;
					}
					else if ( myArg.equals("blundering"))
					{
						tw.setShowFlights(SHOW_BLUNDERING) ;
					}
					else if ( myArg.equals("none"))
					{
						tw.setShowFlights(SHOW_NONE) ;
					}
					else
					{
						retVal = "ERROR: I don't know how to show flights \"" + myArg + "\".\n" +
									"\tPlease try one of these instead:\n\n" +
									"\t\t\"show flights " + flightsArgs + "\"\n" +
									"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
					}
				}
				
			} else if (showWhat.equals("routes"))
			{
				if ( Arguments.length != 2 )
				{
					retVal = "ERROR: show routes only takes 1 argument.\n" +
					"\tPlease try one of these instead:\n\n" +
					"\t\t\"show routes " + routesArgs + "\"\n" +
					"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
				}
				else
				{
					String myArg = Arguments[1] ;
					if ( myArg.equals("all"))
					{
						tw.setShowRoutes(SHOW_ALL) ;
					}
					else if ( myArg.equals("selected"))
					{
						tw.setShowRoutes(SHOW_SELECTED);
					}
					else if ( myArg.equals("conforming"))
					{
						tw.setShowRoutes(SHOW_CONFORMING);
					}
					else if ( myArg.equals("blundering"))
					{
						tw.setShowRoutes(SHOW_BLUNDERING);
					}
					else if ( myArg.equals("none"))
					{
						tw.setShowRoutes(SHOW_NONE);
					}
					else
					{
						retVal = "ERROR: I don't know how to show routes \"" + myArg + "\".\n" +
									"\tPlease try one of these instead:\n\n" +
									"\t\t\"show routes " + routesArgs + "\"\n" +
									"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
					}
				}
			} else if (showWhat.equals("trajectories"))
			{
				if ( Arguments.length != 2 )
				{
					retVal = "ERROR: show trajectories only takes 1 argument.\n" +
					"\tPlease try one of these instead:\n\n" +
					"\t\t\"show trajectories " + trajectoriesArgs + "\"\n" +
					"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
				}
				else
				{
					String myArg = Arguments[1] ;
					if ( myArg.equals("all"))
					{
						tw.setShowTrajectories(SHOW_ALL) ;
					}
					else if ( myArg.equals("selected"))
					{
						tw.setShowTrajectories(SHOW_SELECTED);
					}
					else if ( myArg.equals("withplans"))
					{
						tw.setShowTrajectories(SHOW_WITH_PLAN);
					}
					else if ( myArg.equals("conforming"))
					{
						tw.setShowTrajectories(SHOW_CONFORMING);
					}
					else if ( myArg.equals("blundering"))
					{
						tw.setShowTrajectories(SHOW_BLUNDERING);
					}
					else if ( myArg.equals("none"))
					{
						tw.setShowTrajectories(SHOW_NONE);
					}
					else
					{
						retVal = "ERROR: I don't know how to show trajectories \"" + myArg + "\".\n" +
									"\tPlease try one of these instead:\n\n" +
									"\t\t\"show trajectories " + trajectoriesArgs + "\"\n" +
									"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
					}
				}
			} else
			{
				retVal = "ERROR: I don't know how to show \"" + showWhat + "\". \n" +
						"\tPlease try one of these instead:\n\n" +
						"\t\t\"show fixes " + fixesArgs + "\"\n" +
						"\t\t\"show flights " + flightsArgs + "\"\n" +
						"\t\t\"show routes " + routesArgs + "\"\n" +
						"\t\t\"show trajectories " + trajectoriesArgs + "\"\n" +
						"\n\t* [ � | � ] signifies that you  have to choose one of the items in square\n\t brackets, e.g. select fixes [ all | none] can be select fixes all\n\t or select fixes none " ;
			}
			
			String summaryOfShow = 	"\n------------------------------------------------\n" +
								   	"| FIXES:\t\t|\t" 		+ tw.getShowFixes() + "\n" +
								   	"| FLIGHTS:\t\t|\t" 		+ tw.getShowFlights() + "\n" +
								   	"| ROUTES:\t\t|\t" 		+ tw.getShowRoutes() + "\n" +
								   	"| TRAJECTORIES:\t|\t" + tw.getShowTrajectories() + "\n" +
								   	"| \n" +
								   	"| THRESHOLDS\n" +
								   	"| LATERAL:\t|\t" + tw.client.getParameters().cmLateralWeightOn + "\n" +
								   	"| VERTICAL:\t|\t" + tw.client.getParameters().cmVerticalWeightOn + "\n" +
								   	"| ANGULAR:\t|\t" + tw.client.getParameters().cmAngularWeightOn + "\n" +
								   	"| SPEED:\t|\t" + tw.client.getParameters().cmSpeedWeightOn + "\n" +
									"--------------------------------------------------\n" ;
			
			return retVal + summaryOfShow ;
		}
	}
}
