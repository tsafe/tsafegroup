
package tsafe.client.text_client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collection;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import tsafe.client.CloseWindowAdapter;
import tsafe.client.text_client.command_handlers.Command_Handler;
import tsafe.client.text_client.command_handlers.Disable_Handler;
import tsafe.client.text_client.command_handlers.Enable_Handler;
import tsafe.client.text_client.command_handlers.Select_Handler;
import tsafe.client.text_client.command_handlers.Set_Handler;
import tsafe.client.text_client.command_handlers.Show_Handler;
import tsafe.common_datastructures.Fix;
import tsafe.common_datastructures.Flight;
import tsafe.common_datastructures.FlightPlan;
import tsafe.common_datastructures.FlightTrack;
import tsafe.common_datastructures.Point4D;
import tsafe.common_datastructures.Route;
import tsafe.common_datastructures.Trajectory;
import tsafe.common_datastructures.client_server_communication.ComputationResults;
/*
 * Created on Nov 27, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author superduperman
 *
 * The text window to be displayed, along with all back-end logic.
 */
public class TextWindow extends JFrame implements ActionListener, KeyListener {
	
	/**
     * Show options
     */
    public static final int SHOW_ALL        = 0;
    public static final int SHOW_SELECTED   = 1;
    public static final int SHOW_WITH_PLAN  = 2;
    public static final int SHOW_CONFORMING = 3;
    public static final int SHOW_BLUNDERING = 4;
    public static final int SHOW_NONE       = 5;
    
    /**
     * Data to draw to the screen
     */
    private Collection flights  = null ;
    private Vector selectFlight = null;
    /**
     * Flags triggering the display of certain flight pane items
     */
    private int showFixes  = SHOW_NONE, showFlights      = SHOW_ALL,
                showRoutes = SHOW_ALL, showTrajectories = SHOW_ALL;
	
	public JComponent consolePanel;
	public JTextPane textPane;
	public JTextField entryField = new JTextField();
	public JButton entryButton = new JButton("Enter");
	public TextClient client;
	
	CommandRunner cr ;
	
	public TextWindow(TextClient client) {
		super("TSafe2d TextWindow");
		
		this.client = client ; 
		selectFlight = new Vector();
		JFrame.setDefaultLookAndFeelDecorated(true);
		this.getContentPane().add(makeConsole());
		this.pack();
		//      this.setVisible(true);
		this.setLocationRelativeTo(null);
		
		entryButton.addActionListener(this);
		entryButton.addKeyListener(this);
		entryField.addKeyListener(this);
		
		Command_Handler [] commands = new Command_Handler[5] ;
		commands[0] = new Select_Handler(this) ;
		commands[1] = new Show_Handler(this) ;
		commands[2] = new Set_Handler(this) ;
		commands[3] = new Enable_Handler(this) ;
		commands[4] = new Disable_Handler(this) ;
		cr = new CommandRunner(commands) ;
		
		addWindowListener( new CloseWindowAdapter(client) ) ;
	}
	
	public void startWindow() {
		this.setVisible(true);
	}
	
	public void updateWindow(ComputationResults results) {

		

		//add sat 11/04/04
		Update(results);
	}
	

	
	double round(double value, int decimalPlace) {
        double power_of_ten = 1;
        while (decimalPlace-- > 0)
           power_of_ten *= 10.0;
        return Math.round(value * power_of_ten) 
           / power_of_ten;
        }
	
	String printFlight(Flight F)
	{
		String out;
		FlightTrack FT = F.getFlightTrack();
		out = lat(FT.getLatitude()) +"\t";
		
	
		out += lon(FT.getLongitude()) + "\t";
		
		
		out+= round(FT.getAltitude(),2) + "m\t";
		//out+= round(FT.getSpeed(),2)+"sp\t";
		out+= round(FT.getSpeed()*3.6*1000,2) + "km/h|";
		
		out+= round(FT.getSpeed() * 1.94384449*1000, 2) + "knots\t";
		out += round(FT.getHeading()*180/3.1415926,2) + "\u00B0\n";
		return out;
	}
	
	String printFlightPlan(Flight F)
	{
		FlightPlan FP = F.getFlightPlan();
		if ( FP == null)
			return null;
		
		String out = "Flight plan:\t\t";
		out+= round(FP.getAssignedAltitude(),2)+"m\t";
		//out+= round(FP.getAssignedSpeed(),2)+"sp\t";
		out+= round(FP.getAssignedSpeed()*3.6*1000,2) + "km/h|";
		out+= round(FP.getAssignedSpeed() * 1.94384449*1000, 2) + "knots\t\n";
		
		return out;
		
	}
	
	String printRoute(Flight F)
	{
		FlightPlan FP = F.getFlightPlan();
		try{
		Route R = FP.getRoute();
		
		
		if (R == null)
		{
			return null;
		}
		if (R.isEmpty())
		{
			return null;
		}
		
		String out = new String();
		Vector Fixs = new Vector(R.fixList());
		for(int i=0; i < Fixs.size(); i++)
		{
			//System.out.println(( (Fix)Fixs.get(i)).getId() );
			out += ((Fix)Fixs.get(i)).getId() + " ";
			//out += round( ((Fix)Fixs.get(i)).getLatitude(),2 ) + "";
			//out += round( ((Fix)Fixs.get(i)).getLongitude(),2 ) + "";
			//out += "\t";
		}
		
		return out;
		}
		catch(java.lang.NullPointerException E)
		{
			return null;
		}
		
	}
	String printTraj(Map M)
	{
		Vector points = new Vector(M.values());
		String out = new String();;
		out = "Trajectories:\t\t";
		for(int j= 0 ; j < points.size(); j++)
		{
			try{
				Trajectory TJ =  (Trajectory )points.get(j);
			
				Vector point = new Vector( TJ.pointList());
				for(int i= 0 ; i < point.size(); i++)
				{
					Point4D P = (Point4D)point.get(i);
					out += lat(P.getLatitude())+ " ";
					out += lon(P.getLongitude())+"   \t";
				}
			}
			catch( Exception E)
			{
				System.out.println(E);
			}
			
		}
		//System.out.println(points);
		return out+"\n";
		
		
		//return out;
	}
	
	String lon(double lon)
	{
		if(lon < 0)
		{
			return round(lon*(-1),2)+"W";
		}
		else
		{
			return round(lon,2)+"E";
		}
	}
	String lat(double lat)
	{
		if(lat < 0)
		{
			return round(lat*(-1),2)+"S";
		}
		else
			return round(lat,2)+"N";
	}
	void ResultFlight(ComputationResults results)
	{
		Vector FlightList = new Vector(results.getFlights());
	
		
		//show selected flights
		if (flights != null)
		{
			Vector Flightname = new Vector(this.flights);
			for(int i = 0; i < FlightList.size(); i++ )
			{
				for (int j = 0; j < Flightname.size(); j++)
				{
					if( ((Flight)FlightList.get(i)).getAircraftId().compareTo((String)Flightname.get(j)) == 0)
					{
						
						selectFlight.add ((Flight)FlightList.get(i));
					}
				}
			}
		}
	
		
	}
	private void Update(ComputationResults results) {
 
        /**
         * Draw Flights
         */
		Vector fixList = new Vector(this.client.getFixes());
		try{
		if (showFixes  == SHOW_ALL)
		{
			textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
					"fixs:\t\t" , textPane.getStyledDocument().getStyle("whiteRegular"));
			
			for(int i=0; i < fixList.size(); i++)
			{
				Fix F = (Fix)fixList.get(i);
				textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
						lat(F.getLatitude()) + " " + lon(F.getLongitude()) + "   \t", textPane.getStyledDocument().getStyle("whiteRegular"));
				if (i%3 == 0 && i != 0)
				{
					textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
							"\n" , textPane.getStyledDocument().getStyle("whiteRegular"));
				}
			}
			textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
					"\n" , textPane.getStyledDocument().getStyle("whiteRegular"));
		}
		
		Vector blunders = new Vector(results.getBlunders());
		
		Vector FlightList = new Vector(results.getFlights());
		
		ResultFlight(results);
		
		for(int i =0 ; i < FlightList.size(); i++)
		{
			Flight flight = (Flight)FlightList.get(i);
			 boolean hasFlightPlan = flight.getFlightPlan() != null;
	         boolean isBlundering = hasFlightPlan && blunders.contains(flight);
	         boolean isConforming = hasFlightPlan && !isBlundering;
	         boolean isSelected = selectFlight.contains(flight);
	         
	         if (showFlights == SHOW_ALL || 
	         	(showFlights == SHOW_SELECTED && isSelected)||
	         	(showFlights == SHOW_CONFORMING && isConforming) ||
	            (showFlights == SHOW_BLUNDERING && isBlundering) 	||
				showFlights == SHOW_WITH_PLAN && hasFlightPlan )
	         {
	         	
	         	
					textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
							"FLIGHT: " , textPane.getStyledDocument().getStyle("whiteLarge"));
					if (isBlundering )
					{
						textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
								flight.getAircraftId() +"\t", textPane.getStyledDocument().getStyle("redLarge"));
					}
					else
					{
						textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
								flight.getAircraftId() +"\t", textPane.getStyledDocument().getStyle("whiteLarge"));
					}
					textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
							printFlight(flight) , textPane.getStyledDocument().getStyle("whiteRegular"));
					
					if (printFlightPlan(flight) != null)
					textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
							printFlightPlan(flight) , textPane.getStyledDocument().getStyle("whiteRegular"));
	         	
					if ( showRoutes == SHOW_ALL ||
							(showRoutes == SHOW_CONFORMING && isConforming) ||
				            (showRoutes == SHOW_BLUNDERING && isBlundering) ||
							(showRoutes == SHOW_SELECTED && isSelected))
					{
						if (printRoute(flight) != null)
						textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
								"\t\t"+printRoute(flight)+"\n" , textPane.getStyledDocument().getStyle("whiteRegular"));
					}
					if (showTrajectories  == SHOW_ALL || 
							(showTrajectories == SHOW_CONFORMING && isConforming) ||
				            (showTrajectories == SHOW_BLUNDERING && isBlundering) ||
				            (showTrajectories == SHOW_SELECTED && isSelected) ||
							showTrajectories == SHOW_WITH_PLAN && hasFlightPlan)
					{
						textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
								printTraj(results.getFlight2TrajectoryMap()) , textPane.getStyledDocument().getStyle("whiteRegular"));
					}
					if(isBlundering)
					{
						textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
								"Conformance Status" , textPane.getStyledDocument().getStyle("whiteRegular"));
						textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
								"\tblundering\n\n" , textPane.getStyledDocument().getStyle("redRegular"));
					}
					else
					{
						textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
								"\n" , textPane.getStyledDocument().getStyle("redRegular"));
					}
	         }
       
            
		}
		}
	 catch (BadLocationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	 }
	}
                
           

          
	JComponent makeConsole() {
		
		textPane = new JTextPane();
		textPane.setPreferredSize(new Dimension(900,400));
		JScrollPane paneScrollPane = new JScrollPane(textPane);
		//      paneScrollPane.setVerticalScrollBarPolicy(
		//                      JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		paneScrollPane.setPreferredSize(new Dimension(900, 400));
		paneScrollPane.setMinimumSize(new Dimension(400,150));
		textPane.setBackground(Color.BLACK);
		StyledDocument doc = textPane.getStyledDocument();
		addStylesToDocument(doc);
		
		GridBagConstraints gbc = new GridBagConstraints();
		GridBagLayout gridbag = new GridBagLayout();
		
		JPanel bottomPanel = new JPanel(gridbag);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = .95;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gridbag.setConstraints(entryField, gbc);
		bottomPanel.add(entryField);
		
		gbc.gridx++;
		gbc.weightx = .05;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gridbag.setConstraints(entryButton, gbc);
		bottomPanel.add(entryButton);
		
		JPanel finalPanel = new JPanel(gridbag);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.BOTH;
		gridbag.setConstraints(paneScrollPane, gbc);
		finalPanel.add(paneScrollPane);
		
		gbc.gridy++;
		gbc.weighty = 0;
		gridbag.setConstraints(bottomPanel,gbc);
		finalPanel.add(bottomPanel);
		
		paneScrollPane.setHorizontalScrollBarPolicy(
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		return finalPanel;
	}
	
	protected void addStylesToDocument(StyledDocument doc) {
		//Initialize some styles.
		Style def = StyleContext.getDefaultStyleContext().
		getStyle(StyleContext.DEFAULT_STYLE);
		Style regular = doc.addStyle("reg", def);
		
		Style s = doc.addStyle("whiteRegular", regular);
		StyleConstants.setForeground(s, Color.WHITE);
		
		s = doc.addStyle("whiteLarge", regular);
		StyleConstants.setForeground(s, Color.WHITE);
		StyleConstants.setFontSize(s, 16);
		StyleConstants.setBold(s, true);
		
		s = doc.addStyle("redLarge", regular);
		StyleConstants.setForeground(s, Color.RED);
		StyleConstants.setFontSize(s, 16);
		StyleConstants.setBold(s, true);
		
		s = doc.addStyle("redRegular", regular);
		StyleConstants.setForeground(s, Color.RED);
		
	}
	
	/**
	 * Event handler for dealing with key presses
	 */
	public void keyTyped(KeyEvent e) {}
	
	/**
	 * Event handler for dealing with key releases
	 */
	public void keyReleased(KeyEvent e) {}
	
	/** Handle the key pressed event from the text field. */
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == 10) {   //enter pressed 
			String text = cr.parseAndRun(entryField.getText());  // text in entry field
			StyledDocument doc = textPane.getStyledDocument();
			try {
				String[] splitInput = text.split("\\s+") ;
				if ( splitInput[0].equals("ERROR:"))
				{
					textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
							text + "\n", textPane.getStyledDocument().getStyle("redRegular"));
				}
				else
				{
					textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
							text + "\n", textPane.getStyledDocument().getStyle("whiteRegular"));
				}
			} catch (BadLocationException ble) {
				System.err.println("Couldn't insert initial text into text pane.");
			}
			entryField.setText("");
		}
		
	}
	
	/**
	 * Event handler for all actions.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == entryButton) {
			String text = entryField.getText();
			StyledDocument doc = textPane.getStyledDocument();
			try {
				textPane.getStyledDocument().insertString(textPane.getStyledDocument().getLength(),
						text + "\n",
						textPane.getStyledDocument().getStyle("regular"));
			} catch (BadLocationException ble) {
				System.err.println("Couldn't insert initial text into text pane.");
			}
		}
		entryField.setText("");
	}
	/**
	 * @param flights The {@link #flights flights} to set.
	 */
	public void setFlights(Collection flights) {
		this.flights = flights;
	}
	/**
	 * @return Returns the {@link #showFixes showFixes}.
	 */
	public int getShowFixes() {
		return showFixes;
	}
	/**
	 * @param showFixes The {@link #showFixes showFixes} to set.
	 */
	public void setShowFixes(int showFixes) {
		this.showFixes = showFixes;
	}
	/**
	 * @return Returns the {@link #showFlights showFlights}.
	 */
	public int getShowFlights() {
		return showFlights;
	}
	/**
	 * @param showFlights The {@link #showFlights showFlights} to set.
	 */
	public void setShowFlights(int showFlights) {
		this.showFlights = showFlights;
	}
	/**
	 * @return Returns the {@link #showRoutes showRoutes}.
	 */
	public int getShowRoutes() {
		return showRoutes;
	}
	/**
	 * @param showRoutes The {@link #showRoutes showRoutes} to set.
	 */
	public void setShowRoutes(int showRoutes) {
		this.showRoutes = showRoutes;
	}
	/**
	 * @return Returns the {@link #showTrajectories showTrajectories}.
	 */
	public int getShowTrajectories() {
		return showTrajectories;
	}
	/**
	 * @param showTrajectories The {@link #showTrajectories showTrajectories} to set.
	 */
	public void setShowTrajectories(int showTrajectories) {
		this.showTrajectories = showTrajectories;
	}
}
