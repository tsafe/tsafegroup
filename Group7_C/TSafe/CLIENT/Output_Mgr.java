/*
 * Created on Dec 1, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package tsafe.client;

import java.util.*;

import javax.swing.*;
import javax.swing.text.*;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import java.text.DecimalFormat;
import tsafe.common_datastructures.*;
import tsafe.common_datastructures.communication.ComputationResults;

public class Output_Mgr extends JPanel {
    private Collection flights  = new LinkedList();
    private Collection blunders = new LinkedList();
    private Map TrajMap		 	= new HashMap();
    
    private String showFixes		 = "all", 
				   showFlights		 = "all",
				   showRoutes		 = "all", 
				   showTrajectories  = "all";
    
    private Collection selectedFlights = new Vector();
    private String[] selectedID = new String[0];
    
    Client_Text client;
    
    //GUI VARIABLES
    DefaultStyledDocument doc = new DefaultStyledDocument();
	JTextPane textPane;
	JScrollPane textScroll;
    SimpleAttributeSet big = new SimpleAttributeSet();
    SimpleAttributeSet bigred = new SimpleAttributeSet();
    SimpleAttributeSet small = new SimpleAttributeSet();
    SimpleAttributeSet smallred = new SimpleAttributeSet();
    SimpleAttributeSet smallgreen = new SimpleAttributeSet();
	SimpleAttributeSet smallyellow = new SimpleAttributeSet();
    SimpleAttributeSet smallgray = new SimpleAttributeSet();
    SimpleAttributeSet smallorange = new SimpleAttributeSet();
    
	
    public Output_Mgr(Client_Text client)
    {
    	super();
    	this.client = client;
    	textPane = new JTextPane(doc);
		textPane.setBackground(Color.BLACK);
		textScroll = new JScrollPane(textPane,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		textScroll.setPreferredSize(new Dimension(800,400));
		textScroll.getViewport().getView().addComponentListener(new ScrollListener());
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		add(textScroll,c);
		try {
			StyleConstants.setForeground(big, Color.WHITE);
			StyleConstants.setFontSize(big, 16);
			StyleConstants.setBold(big, true);
			
			StyleConstants.setForeground(bigred, Color.RED);
			StyleConstants.setFontSize(bigred, 16);
			StyleConstants.setBold(bigred, true);
			
			StyleConstants.setForeground(small, Color.WHITE);
			
			StyleConstants.setForeground(smallred, Color.RED);
			StyleConstants.setBold(smallred, true);
			
			StyleConstants.setForeground(smallgreen, Color.GREEN);
			
			StyleConstants.setForeground(smallyellow, Color.YELLOW);
			StyleConstants.setItalic(smallyellow, true);
			
			StyleConstants.setForeground(smallgray, Color.GRAY);
			StyleConstants.setForeground(smallorange, Color.ORANGE);
		}
		catch(Exception e) {}
	}
    
    public void helpMessage()
    {
    	try {
    		StringBuffer strbuf = new StringBuffer();
    		doc.insertString(doc.getLength(), "Help Menu:	Yellow words are keywords for commands.\n", small);
    		doc.insertString(doc.getLength(), "Select one or more flights from all avaliable flights:\n", small);
    		doc.insertString(doc.getLength(), "select ", smallyellow);     					//SELECT 
    		doc.insertString(doc.getLength(), "<flightid1> <flightid2> <flightid3>...\n", small);
    		
    		doc.insertString(doc.getLength(), "Select all flights:\n", small);
    		doc.insertString(doc.getLength(), "select ", smallyellow);
    		doc.insertString(doc.getLength(), "*\n\n", small);

    		doc.insertString(doc.getLength(), "Change output settings:\n", small);
    		doc.insertString(doc.getLength(), "show fixes ", smallyellow);
    		doc.insertString(doc.getLength(), "[ all | none ]\n", small);
    		
    		doc.insertString(doc.getLength(), "show flight ", smallyellow);
    		doc.insertString(doc.getLength(), "[ all | selected | withplans | conforming | blundering | none ]\n", small);
    		
    		doc.insertString(doc.getLength(), "show routes ", smallyellow);
    		doc.insertString(doc.getLength(), "[ all | selected | conforming | blundering | none ]\n", small);
    	
    		doc.insertString(doc.getLength(), "show trajectories ", smallyellow);
    		doc.insertString(doc.getLength(), "[ all | selected | withplans | conforming | blundering | none ]\n\n\n", small);
    	}
    	catch(BadLocationException e) {System.err.println(e);}
    }
    
    private void setting() throws BadLocationException
    {
    	StringBuffer strbuf = new StringBuffer();
    	
    	doc.insertString(doc.getLength(), "=========================================================================\n", smallgray);
    	doc.insertString(doc.getLength(), "= ", smallgray);
    	doc.insertString(doc.getLength(), "Settings:\t\t\t\t\t", big);
    	doc.insertString(doc.getLength(), "=\n", smallgray);
    	
    	strbuf.append("= show fixes:\t\t" + showFixes + "\t[");
    	if (client.getParameters().cmLateralWeightOn) {
    		strbuf.append("on");
    	}
    	else {
    		strbuf.append("off");
    	}
    	strbuf.append("]\tthreslat:\t" + client.getParameters().cmLateralThreshold + "\t=\n");
    	strbuf.append("= show flights:\t" + showFlights + "\t[");
    	if (client.getParameters().cmVerticalWeightOn) {
    		strbuf.append("on");
    	}
    	else {
    		strbuf.append("off");
    	}
    	strbuf.append("]\tthresver:\t" + client.getParameters().cmVerticalThreshold + "\t=\n");
    	strbuf.append("= show routes:\t" + showRoutes + "\t[");
    	if (client.getParameters().cmAngularWeightOn) {
    		strbuf.append("on");
    	}
    	else {
    		strbuf.append("off");
    	}
    	strbuf.append("]\tthresang:\t" + client.getParameters().cmAngularThreshold + "\t=\n");
    	strbuf.append("= show trajectories:\t" + showTrajectories + "\t[");
    	if (client.getParameters().cmSpeedWeightOn) {
    		strbuf.append("on");
    	}
    	else {
    		strbuf.append("off");
    	}
    	strbuf.append("]\tthresspe:\t" + client.getParameters().cmSpeedThreshold + "\t=\n");
    	strbuf.append("= selection:\t\t");
    	boolean displayedHoriz = false;
    	if (selectedFlights.size() > 0) {
    		Iterator i = selectedFlights.iterator();
    		Flight fl = (Flight)i.next();
    		strbuf.append(fl.getAircraftId() + "\t\tthresres:\t" + client.getParameters().cmResidualThreshold + "\t=\n");
    		while(i.hasNext()) {
    			fl = (Flight)i.next();
    			strbuf.append("\t\t" + fl.getAircraftId());
    			if (!displayedHoriz) {
    				strbuf.append("\t\thoriztim:\t" + client.getParameters().tsTimeHorizon + "\t=\n");
    				displayedHoriz = true;
    			}
    			else {
    				strbuf.append("\t\t\t\t=\n");
    			}
    		}
    	}
    	else {
    		strbuf.append("none" + "\t\tthresres:\t" + client.getParameters().cmResidualThreshold + "\t=\n");
    	}
		if (!displayedHoriz) {
			strbuf.append("\t\t\t\thoriztim:\t" + client.getParameters().tsTimeHorizon + "\t=\n");
			displayedHoriz = true;
		}
    	strbuf.append("=========================================================================\n");
    	doc.insertString(doc.getLength(), strbuf.toString() + "\n", smallgray);
    }
    
    //UPDATE FLIGHTS CURRENTLY ON THE SYSTEM
    public void setFlights(Collection flights) {
    	this.flights = new LinkedList(flights);
    }
    public void setBlunders(Collection blunders) {
        this.blunders = new LinkedList(blunders);
    }
    public void setFlightTrajectoryMap(Map flight2TrajMap) {
        this.TrajMap = new HashMap(flight2TrajMap);
    }
    
    //DISPLAY OPTIONS
    public Collection getSelectedFlights()
    {
    	return selectedFlights;
    }
    public void setSelectedFlights(Collection selectedFlights) 
    {
        this.selectedFlights = selectedFlights;
    }
    public void selectFlights(String[] IDs) 
    {	
    	selectedFlights = new Vector();
    	ArrayList nonExists = new ArrayList();
    	//StringBuffer nonExists = new StringBuffer();
    	boolean selected = false;
    	
    	for(int i = 1; i < IDs.length; i++)
    	{
    		selected = false;
    		if(IDs[i].equals("*"))
    		{
    			selectedFlights = new Vector(flights);
    			break;
    		}
    		else {
	    		Iterator it = flights.iterator();
	    		while(it.hasNext())
	    		{
	    			Flight curr = (Flight)it.next();
	    			if(IDs[i].equals(curr.getAircraftId())) {
	    				selectedFlights.add(curr);
	    				selected = true;
	    				break;
	    			}
	    		}
	    		if (!selected) {
	    			nonExists.add(IDs[i]);
	    		}
    		}
    	}
    	client.selectionChanged(flights, selectedFlights);
    	if(nonExists.size() > 0)
    	{
    		try {
    			doc.insertString(doc.getLength(), "Flight", smallorange);
    			if (nonExists.size() > 1) {
    				doc.insertString(doc.getLength(),"s",smallorange);
    			}
    			doc.insertString(doc.getLength()," ",smallorange);
				for (int i = 0; i < nonExists.size(); i++) {
					doc.insertString(doc.getLength(),"\"" + (String)nonExists.get(i) + "\"" + " ",smallorange);
					if (i == nonExists.size()-2) {
						doc.insertString(doc.getLength(),"and ",smallorange);
					}
				}
				if (nonExists.size() > 1) {
					doc.insertString(doc.getLength(),"do",smallorange);
				}
				else {
					doc.insertString(doc.getLength(),"does",smallorange);
				}
				doc.insertString(doc.getLength()," not exist.\n",smallorange);
    		}
    		catch(BadLocationException e){
    			System.err.println(e);
    		}
    	}	
    }
//------------------------------------------------------------------------------------
    public void showFixes(String str) {
        this.showFixes = str.toLowerCase();
    }
    public void showFlights(String str) {
    	this.showFlights = str.toLowerCase();
    }
    public void showRoutes(String str) {
    	this.showRoutes = str.toLowerCase();
    }
    public void showTrajectories(String str){
    	this.showTrajectories = str.toLowerCase();
    }
    
    
    //OTHER METHODS
    public void echoCommand(String s, boolean bool)
    {
    	try {
    		if(bool)   
    			doc.insertString(doc.getLength(), "Command: "+s+"\n", smallgreen);
    		else
    			doc.insertString(doc.getLength(), "Invalid Command: "+s+"\n", smallgreen);
    	}
    	catch(Exception e){System.err.println("echo error");}
    }
//-------------------------------------------------------------------------
    public void updateNeeded(ComputationResults results)
    {
    	try {
    		setting();
    	}
    	catch(BadLocationException e) {System.err.println(e);}
    	if (results != null) {
			flights  = results.getFlights();
			blunders = results.getBlunders();
			TrajMap	 = results.getFlight2TrajectoryMap();
    	
	    	Iterator iter = flights.iterator();
	    	while(iter.hasNext())
	    	{
	    		Flight flight = (Flight)iter.next();
	    		boolean hasFlightPlan = flight.getFlightPlan() != null;
	            boolean isBlundering = hasFlightPlan && blunders.contains(flight);
	            boolean isConforming = hasFlightPlan && !isBlundering;
	            boolean isSelected = selectedFlights.contains(flight);
	
	            if(showFlights.equals("all") ||
	               (showFlights.equals("withplans")  && hasFlightPlan) ||
				   (showFlights.equals("conforming") && isConforming) ||
				   (showFlights.equals("blundering") && isBlundering) ||
				   (showFlights.equals("selected")	 && isSelected)) 
	            {
	            	printflights(flight, isBlundering);
	            	if(hasFlightPlan &&
	            	   (showRoutes.equals("all") ||
	    		        (showRoutes.equals("conforming") && isConforming) ||
				        (showRoutes.equals("blundering") && isBlundering) ||
				        (showRoutes.equals("selected")    && isSelected))) {
	            		printroutes(flight.getFlightPlan());
	            	}
	            	if(showTrajectories.equals("all") ||
	            	   (showTrajectories.equals("withplans")  && hasFlightPlan) ||
	            	   (showTrajectories.equals("conforming") && isConforming) ||
	            	   (showTrajectories.equals("blundering") && isBlundering) ||
	            	   (showTrajectories.equals("selected")   && isSelected)) {
	            		printtrajectories((Trajectory)TrajMap.get(flight));
	            	}
	            }
	            if(isBlundering) {
	            	printblunder();
	            }
	            try {
	            	doc.insertString(doc.getLength(),"\n",small);
	            }
	            catch(BadLocationException e){ System.err.println(e);}
	        }
    	}
    }
    
    //HELPER METHODS

//------------------------------------------------------------------------------
    private void printflights(Flight f, boolean blundering)
    {
    	try{
    		int degree = 0, min = 0;
    		double kph, knots;
    		StringBuffer strbuf = new StringBuffer();
    		FlightTrack data = f.getFlightTrack();
    		DecimalFormat df = new DecimalFormat("###,###,###.00");
    		doc.insertString(doc.getLength(), "Flight: ", big);								//FIELD TITLE
    		if(blundering)
    			doc.insertString(doc.getLength(), f.getAircraftId()+"	", bigred);   	//FLIGHT ID
    		else
    			doc.insertString(doc.getLength(), f.getAircraftId()+"	", big);
    		
    		
    		degree = (int)data.getLatitude();				//LATITUDE
    		min    = (int)((data.getLatitude()-degree)*60);
    		strbuf.append(Math.abs(degree)+"�"+min+"'");	
    		if(degree > 0)
    			strbuf.append("N	");
    		else strbuf.append("S	");
    		degree = (int)data.getLongitude();				//LONGITUDE
    		min	   = (int)((data.getLatitude()-degree)*60);
    		strbuf.append(Math.abs(degree)+"�"+min+"'");	
    		if(degree > 0)
    			strbuf.append("E	");
    		else strbuf.append("W	");
    		strbuf.append(df.format(data.getAltitude())+" m	");	//ALATITUDE
    		kph = data.getSpeed()*60*60;						//SPEED
    		knots = kph / 1.835;
    		strbuf.append(df.format(kph)+" km/h|"+df.format(knots)+" knots	");  
    		strbuf.append(df.format(data.getHeading())+"�"); 	//HEADING
    		
    		doc.insertString(doc.getLength(), strbuf.toString()+"\n", small);
    	}
    	catch(Exception e){System.err.println("Error: printflights	" + e);}
    }
//------------------------------------------------------------------------------
    private void printroutes(FlightPlan plan)
    {
    	try {
    		double kph, knots;
    		StringBuffer strbuf = new StringBuffer();
    		Route route = plan.getRoute();
    		DecimalFormat df = new DecimalFormat("###,###,###,00");
    		doc.insertString(doc.getLength(), "Flight Plan:		", small); 	//FIELD TITLE
    		strbuf.append(df.format(plan.getAssignedAltitude())+" m	");		//ALTITUDE
    		kph = plan.getAssignedSpeed()*60*60;
    		knots = kph / 1.835;
    		strbuf.append(df.format(kph)+" km/h|"+df.format(knots)+" knots	");	//SPEED
    		strbuf.append("\n" + "		" + route.toString());
    		
    		
    		doc.insertString(doc.getLength(), strbuf.toString()+"\n", small);
    	}
    	catch(Exception e){System.err.println("Error: printroutes	" + e);}
    }
//------------------------------------------------------------------------------
    private void printtrajectories(Trajectory t)
    {
    	try{
    		int degree, min;
    		StringBuffer strbuf = new StringBuffer();
    		Iterator it = t.pointIterator();
    		
    		
    		doc.insertString(doc.getLength(), "Trajectories:		", small);	//FIELD TITLE
    		while(it.hasNext())
    		{	
    			Point4D data = (Point4D)it.next();
    			degree = (int)data.getLatitude();				//LATITUDE
    			min    = (int)((data.getLatitude()-degree)*60);
    			strbuf.append(Math.abs(degree)+"�"+min+"'");	
    			if(degree > 0)
    				strbuf.append("N ");
    			else strbuf.append("S ");
    			degree = (int)data.getLongitude();				//LONGITUDE
    			min	   = (int)((data.getLatitude()-degree)*60);
    			strbuf.append(Math.abs(degree)+"�"+min+"'");	
    			if(degree > 0)
    				strbuf.append("E	");
    			else strbuf.append("W	");
    		}
    		doc.insertString(doc.getLength(), strbuf.toString()+"\n", small);
    	}
    	catch(Exception e){System.err.println("Error: printtrajectories	" + e);}
    }
//------------------------------------------------------------------------------
    private void printblunder()
    {
    	try {
    		doc.insertString(doc.getLength(), "Conformance Status:	", small);	//FIELD TITLE
    		doc.insertString(doc.getLength(), "blundering\n", smallred);		//blundering bold and red
    	}
    	catch(Exception e){System.err.println("Error: printblunder	" + e);}
    }
//------------------------------------------------------------------------------
  
//------------------------------------------------------------------------------
	private class ScrollListener implements ComponentListener {
		public void componentResized (ComponentEvent e) {
			textScroll.getViewport().setViewPosition(new Point(0,(int)textScroll.getViewport().getViewSize().getHeight()));
		}
		
		public void componentShown (ComponentEvent e) {}
		public void componentHidden (ComponentEvent e) {}
		public void componentMoved (ComponentEvent e) {}
	}
   
}