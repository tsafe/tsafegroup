/*
 * Created on Dec 2, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author Jenni Lai
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package tsafe.client;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import tsafe.common_datastructures.communication.UserParameters;
import tsafe.common_datastructures.communication.ComputationResults;

public class Client_Text extends JFrame implements ActionListener {

	/*
	 * SplitPane to hold input, output panes of text console
	 */
	private JSplitPane console;
	
	/*
	 * Input Panel of console, to hold command prompt & <Enter> button
	 */
	private JPanel inputPane;	
	
	/*
	 * Extracts and displays content to output
	 */
	private Output_Mgr outputPane;
	
	/*
	 * Text field command prompt used to parse input from user
	 */
	private JTextField cmmdPrompt;
	
	/* 
	 * Communicates with server for most current flight information
	 */
	private ClientEngine client;
	
	/* 
	 * Stores most current flight information
	 */
	private ComputationResults results;
	
	/*
	 * Parameters for all clients
	 */
	private UserParameters parameters;
	
	private InputParser parser;

	public Client_Text(ClientEngine eng) {
		// Construct JFrame object
		super("TSAFE Flights Information - Text Console");
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JFrame.setDefaultLookAndFeelDecorated(true);
		
		client = eng;
		
		// Construct input, output panes
		this.outputPane = new Output_Mgr(this);
		this.inputPane = new JPanel();
		Dimension min1 = new Dimension(200,600);
		Dimension min2 = new Dimension(200,40);
		outputPane.setMinimumSize(min1);
		inputPane.setMinimumSize(min2);

		//Construct input components: Command Prompt, <Enter> Button
		this.cmmdPrompt = new JTextField("Type command then hit <Enter> key or click 'Enter' button.", 75);
		this.cmmdPrompt.selectAll();
		JButton enter = new JButton("ENTER");
		// Add action listeners to both...
		cmmdPrompt.addActionListener(this);
		enter.addActionListener(this);
		// Add components to inputPane
		inputPane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		inputPane.add(cmmdPrompt,c);
		c.gridx = 1;
		c.weightx = 0;
		c.fill = GridBagConstraints.VERTICAL;
		inputPane.add(enter,c);
		
		parameters = client.getParameters();
		
		getContentPane().setLayout(new GridBagLayout());
		c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		getContentPane().add(outputPane,c);
		c.gridy = 1;
		c.weighty = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		getContentPane().add(inputPane,c);
		
		outputPane.helpMessage();
		
		parser = InputParser.getInstance();
	}
	
	/*
	 * Starts parsing the feed and displays the window
	 * 
	 * @overrides Window.show()
	 */
	public void show() {
		super.show();
	}

	/*private void refreshWindow() {
		// Parameters changed, update contentPane
		outputPane.updateNeeded(results);
	}*/
	
	public Output_Mgr getOutputMgr () {
		return outputPane;
	}

	/* 
	 * Called by client to update flight information.
	 */
	public void updateWindow(ComputationResults results) {
		this.results = results;
		if (results.getFlights().size() != 0) {
			outputPane.updateNeeded(results);
		}
	}
	
	public void forceUpdateWindow () {
		outputPane.updateNeeded(results);
	}

	public void startWindow() {
		this.pack();
		this.show();
	}
	
	public void setSelection (Collection c) {
		outputPane.setSelectedFlights(c);
	}
	
	public void selectionChanged(Collection flights, Collection c) {
		client.setWindowSelection(flights, c);
	}
	
	/* Called to proccess user commands users either
	 * hit <Enter> key or click <Enter> button.
	 */
	public void actionPerformed(ActionEvent e) {
		String echo = this.cmmdPrompt.getText();
		
		String[] cmmd;
		cmmd = parser.getCommand(echo);
		
		if (cmmd == null) 
			// echo invalid command and dislay error message
			outputPane.echoCommand(echo,false);
		else {
			// echo valid command and execute
			outputPane.echoCommand(echo,true);

			if (cmmd[0].equalsIgnoreCase("select"))
				outputPane.selectFlights(cmmd);
			else if (cmmd[0].equalsIgnoreCase("show")) {
				if (cmmd[1].equalsIgnoreCase("fixes")) {
					outputPane.showFixes(cmmd[2]);
					this.client.getGuiClient().changeShowMenu("Fixes",cmmd[2].toLowerCase());
				}
				else if (cmmd[1].equalsIgnoreCase("flights")) {
					outputPane.showFlights(cmmd[2]);
					this.client.getGuiClient().changeShowMenu("Flights",cmmd[2].toLowerCase());
				}
				else if (cmmd[1].equalsIgnoreCase("routes")) {
					outputPane.showRoutes(cmmd[2]);
					this.client.getGuiClient().changeShowMenu("Routes",cmmd[2].toLowerCase());
				}
				else if (cmmd[1].equalsIgnoreCase("trajectories")) {
					outputPane.showTrajectories(cmmd[2]);
					this.client.getGuiClient().changeShowMenu("Trajectories",cmmd[2].toLowerCase());
				}
			}
			else
				// if valid command but none of the above, must be parameters command
				changeParameters(cmmd);
		}
		forceUpdateWindow();
		cmmdPrompt.selectAll(); // so user can type in new command
	}
	
	private void changeParameters(String[] command) {
	    if(command[0].equals("set"))
	    {
	        setParameter(command);
	    }
	    else if(command[0].equals("enable"))
	    {
	        enableParameter(command);
	    }
	    else if(command[0].equals("disable"))
	    {
	        disableParameter(command);
	    }
	}
	
	private void setParameter(String[] command)
	{
        if(command[2].equalsIgnoreCase("threslat"))
        {
            parameters.cmLateralThreshold = Double.parseDouble(command[3]);
        }
        else if(command[2].equalsIgnoreCase("thresver"))
        {
            parameters.cmVerticalThreshold = Double.parseDouble(command[3]);
        }
        else if(command[2].equalsIgnoreCase("thresang"))
        {
            parameters.cmAngularThreshold = Double.parseDouble(command[3]);
        }
        else if(command[2].equalsIgnoreCase("thresspe"))
        {
            parameters.cmSpeedThreshold = Double.parseDouble(command[3]);
        }
        else if(command[2].equalsIgnoreCase("thresres"))
        {
            parameters.cmResidualThreshold = Double.parseDouble(command[3]);
        }
        else if(command[2].equalsIgnoreCase("horiztim"))
        {
            parameters.tsTimeHorizon = Long.parseLong(command[3]);
        }
	}
	
	void enableParameter(String[] command)
	{
        if(command[2].equalsIgnoreCase("threslat"))
        {
            parameters.cmLateralWeightOn = true;
        }
        else if(command[2].equalsIgnoreCase("thresver"))
        {
            parameters.cmVerticalWeightOn = true;
        }
        else if(command[2].equalsIgnoreCase("thresang"))
        {
            parameters.cmAngularWeightOn = true;
        }
        else if(command[2].equalsIgnoreCase("thresspe"))
        {
            parameters.cmSpeedWeightOn = true;
        }
	}
	
	void disableParameter(String[] command)
	{
	    if(command[2].equalsIgnoreCase("threslat"))
	    {
	        parameters.cmLateralWeightOn = false;
	    }
	    else if(command[2].equalsIgnoreCase("thresver"))
	    {
	        parameters.cmVerticalWeightOn = false;
	    }
	    else if(command[2].equalsIgnoreCase("thresang"))
	    {
	        parameters.cmAngularWeightOn = false;
	    }
	    else if(command[2].equalsIgnoreCase("thresspe"))
	    {
	        parameters.cmSpeedWeightOn = false;
	    }
	}
	
	public UserParameters getParameters () {
		return parameters;
	}
}
