/*
 * Created on Nov 29, 2004
 */
 
package tsafe.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nick Kleinschmidt
 */
public class InputParser {

	static private InputParser instance = null;
	
	static private ArrayList validParams;
	static private ArrayList validShowFixOptions;
	static private ArrayList validShowRouteOptions;
	static private ArrayList validShowFlTrajOptions;
	static private Hashtable commandsCanBeEnabledHash;
	static private Hashtable parameterTypesHash;
	
	static private String[] validShowMainOptionsArray = {"all","none"};
	static private String[] validShowNonFixOptionsArray = {"selected","conforming","blundering"};
	static private String[] validShowFlTrajOptionsArray = {"withplans"};
	static private String[] validParamsArray = 			{"threslat",
														"thresver",
														"thresang",
														"thresspe",
														"thresres",
														"horiztim"};
	static private boolean[] paramsCanBeEnabledArray = {true,
														true,
														true,
														true,
														false,
														false};
	static private Object[] typesOfParamsArray = {		new Double(0),
														new Double(0),
														new Double(0),
														new Double(0),
														new Double(0),
														new Long(0)};
	
    private InputParser() 
    {
    	validParams = new ArrayList();
    	validShowFixOptions = new ArrayList();
    	validShowRouteOptions = new ArrayList();
    	validShowFlTrajOptions = new ArrayList();
    	commandsCanBeEnabledHash = new Hashtable();
    	parameterTypesHash = new Hashtable();
    	
    	for (int i = 0; i < validParamsArray.length; i++) {
    		validParams.add(validParamsArray[i]);
    		commandsCanBeEnabledHash.put(validParamsArray[i],new Boolean(paramsCanBeEnabledArray[i]));
    		parameterTypesHash.put(validParamsArray[i], typesOfParamsArray[i]);
    	}
    	
    	for (int i = 0; i < validShowMainOptionsArray.length; i++) {
    		validShowFixOptions.add(validShowMainOptionsArray[i]);
    		validShowRouteOptions.add(validShowMainOptionsArray[i]);
    		validShowFlTrajOptions.add(validShowMainOptionsArray[i]);
    	}
    	
    	for (int i = 0; i < validShowNonFixOptionsArray.length; i++) {
    		validShowRouteOptions.add(validShowNonFixOptionsArray[i]);
    		validShowFlTrajOptions.add(validShowNonFixOptionsArray[i]);
    	}
    	
    	for (int i = 0; i < validShowFlTrajOptionsArray.length; i++) {
    		validShowFlTrajOptions.add(validShowFlTrajOptionsArray[i]);
    	}
    }
    
    static public InputParser getInstance()
    {
        if ( instance == null ) {
        	instance = new InputParser();
        }
        
        return instance;
    }
    
    public String[] getCommand (String s) {
    	String[] args = tokenizeString(s);
    	
    	// no command given, only whitespace
    	if (args.length == 0) {
    		return null;
    	}
    	
    	// move the inital command to lowercase
    	args[0] = args[0].toLowerCase();
    	
    	String command = args[0];
    	
    	if (command.equals("select")) {
    		// must select something
    		if (args.length == 1) {
    			return null;
    		}
    	}
    	else if (command.equals("show")) {
    		// must have something to show and a setting
    		if (args.length != 3) {
    			return null;
    		}
    		String toBeShown = args[1];
    		String setting = args[2];
    		
    		if (toBeShown.equals("fixes")) {
    			if (!isFixOption(setting)) {
    				return null;
    			}

    		}
    		else if (toBeShown.equals("flights") || toBeShown.equals("trajectories")) {
    			if (!isFlTrajOption(setting)) {
    				return null;
    			}
    		}
    		else if (toBeShown.equals("routes")) {
    			if (!isRouteOption(setting)) {
    				return null;
    			}
    		}
    		else {
    			return null;
    		}
    	}
    	else if (command.equals("set") || command.equals("enable") || command.equals("disable")) {
    		if (args.length < 3) {
    			return null;
    		}
    		if (!args[1].toLowerCase().equals("parameter")) {
    			return null;
    		}
    		if (!isValidParamName(args[2])) {
    			return null;
    		}
    		if (command.equals("set")) {
    			if (args.length != 4) {
    				return null;
    			}
    			
    			Object paramType = parameterType(args[2]);
    			
    			if (paramType instanceof Double) {
    				try {
    					double val = Double.parseDouble(args[3]);
    				}
    				catch (NumberFormatException e) {
    					return null;
    				}
    			}
    			else if (paramType instanceof Long) {
    				try {
    					long val = Long.parseLong(args[3]);
    				}
    				catch (NumberFormatException e) {
    					return null;
    				}
    			}
    		}
    		else {
    			if (args.length != 3) {
    				return null;
    			}
    			
    			// check if it can be enabled or disabled
    			if (!commandCanBeEnabled(args[2])) {
    				return null;
    			}
    		}
    	}
    	else {
    		// invalid command
    		return null;
    	}
    	
    	return args;
    }
    
    private String[] tokenizeString (String s) {
    	ArrayList list = new ArrayList();
    	Pattern p = Pattern.compile("\\s*(\\S+)\\s*(.*)");
    	Matcher m = p.matcher(s);
    	while (m.matches()) {
    		list.add(m.group(1));
    		s = m.group(2);
    		m = p.matcher(s);
    	}
    	return (String[])list.toArray(new String[list.size()]);
    }
    
    private boolean isValidParamName (String s) {
    	s = s.toLowerCase();
    	return validParams.contains(s);
    }
    
    private boolean commandCanBeEnabled (String s) {
    	return ((Boolean)commandsCanBeEnabledHash.get(s)).booleanValue();
    }
    
    private Object parameterType (String s) {
    	return parameterTypesHash.get(s);
    }
    
    private boolean isFixOption (String s) {
    	return validShowFixOptions.contains(s);
    }
    
    private boolean isRouteOption (String s) {
    	return validShowRouteOptions.contains(s);
    }
    
    private boolean isFlTrajOption (String s) {
    	return validShowFlTrajOptions.contains(s);
    }
    
    // FOR TESTING ONLY
    public static void main (String[] args) {
    	InputParser p = InputParser.getInstance();

    	InputStreamReader inputStreamReader = new InputStreamReader ( System.in );
        BufferedReader stdin = new BufferedReader ( inputStreamReader );
        
        String in = "sl";
        
        while (!in.equals("")) {
        	try {
        	in = stdin.readLine();
        	}
        	catch (IOException e) {
        		;
        	}
        	
        	System.out.println("==> " + in);
        	
        	String[] s = p.getCommand(in);
        	
        	if (s == null) {
        		System.out.println("NULL");
        	}
        	else {
        		for (int i = 0; i < s.length; i++) {
        			System.out.print(s[i] + " ");
        		}
        		System.out.println();
        	}
        }
    }
}