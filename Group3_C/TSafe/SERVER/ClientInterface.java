/*
 * tsafe2c/tsafe.server
 * ClientInterface
 * ClientInterface.java
 * Created on Dec 2, 2004
 */
package tsafe.server;

import tsafe.common_datastructures.*;
import tsafe.common_datastructures.communication.*;

/**
 * Generic interface for TSafe clients ... created to abstract the Client functionality from the Client-specific
 * setup that the GUI has to allow for additional clients (namely the Console) to be integrated into the
 * TSafe server.
 * @author Dan
 */
public interface ClientInterface extends Runnable {
	/**
	 * Alerts the client to periodically update update
	 *
	 */
    void notifyClient();
	
    /**
     * Sets the bondaries for the client
     * @param bounds the new boundaries
     */
	void setBounds(LatLonBounds bounds);
	
	/**
	 * Sets the client's ServerEngine object
	 * @param engine the engine
	 */
	void setServer(ServerEngine engine);
	
	/**
	 * Instructs client to accept a new UserParameters that has
	 * been propogated by the server.
	 * @param params the new UserParameters
	 */
	void propogateSettings(UserParameters params);
}
