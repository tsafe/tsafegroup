/*
 * tsafe2c/console
 * ConsoleGUI
 * ConsoleGUI.java
 * Created on Nov 27, 2004
 */
package tsafe.console;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.text.*;

import tsafe.common_datastructures.*;
import tsafe.common_datastructures.communication.*;

/**
 * @author Dan
 */
public class ConsoleGUI extends JFrame {
	
	// Default GUI settings
	public static Color DEFAULT_BACKGROUND_COLOR =	Color.black;
	
	// The formatting AttributeSet for standard text
	public static AttributeSet TextAttribute;
	// The formatting AttributeSet for highlighted standard text
	public static AttributeSet HighlightTextAttribute;
	// The formatting AttributeSet for message text
	public static AttributeSet MessageAttribute;
//	 The formatting AttributeSet for message text
	public static AttributeSet ErrorAttribute;
	// The formatting AttributeSet for title text
	public static AttributeSet TitleAttribute;
	// The formatting AttributeSet for highlighted title text
	public static AttributeSet HighlightTitleAttribute;
	
	// Statically initialize the formatting attributes
	static
	{
		SimpleAttributeSet s = new SimpleAttributeSet();
		s.addAttribute(StyleConstants.FontSize, new Integer(16));
		s.addAttribute(StyleConstants.Foreground, Color.white);
		TextAttribute = s;
		
		s = new SimpleAttributeSet(TextAttribute);
		s.addAttribute(StyleConstants.Foreground, Color.red);		
		HighlightTextAttribute = s;
		
		s = new SimpleAttributeSet();
		s.addAttribute(StyleConstants.FontSize, new Integer(20));
		s.addAttribute(StyleConstants.Foreground, Color.white);
		s.addAttribute(StyleConstants.Bold, Boolean.TRUE);
		TitleAttribute = s;
		
		s = new SimpleAttributeSet(TitleAttribute);
		s.addAttribute(StyleConstants.Foreground, Color.red);
		HighlightTitleAttribute = s;
		
		s = new SimpleAttributeSet();
		s.addAttribute(StyleConstants.FontSize, new Integer(12));
		s.addAttribute(StyleConstants.Italic, Boolean.TRUE);
		s.addAttribute(StyleConstants.Foreground, Color.green);
		MessageAttribute = s;
		
		s = new SimpleAttributeSet();
		s.addAttribute(StyleConstants.FontSize, new Integer(12));
		s.addAttribute(StyleConstants.Italic, Boolean.TRUE);
		s.addAttribute(StyleConstants.Foreground, Color.red);
		ErrorAttribute = s;
	}

	private JTextPane		console;
	private JTextField		input;
	private InputParser		parser;
	private ArrayList 		mem;  //Cute!
	private int				memIdx;
	
	/**
	 * Constructs a basic ConsoleGUI.  This ConsoleGUI must have an InputParser associated with it
	 * via the setParser(InputParser) function before it is used.
	 * @see InputParser
	 * @see #setParser(InputParser)
	 */
	public ConsoleGUI()
	{
		this(null);
	}
	
	/**
	 * Constructs a basic ConsoleGUI with a specific InputParser.
	 * @param parser the InputParser to use
	 */
	public ConsoleGUI(InputParser parser)
	{
		super("TSafe Console Client");
		this.parser = parser;
		this.mem = new ArrayList();
		this.memIdx = 0;
		
		addWindowListener(new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				ConsoleGUI.this.input.requestFocusInWindow();
			}
			
			public void windowClosing(WindowEvent e) {
				exitConsole();
			}
		});		
		
		// Layout the GUI
		getContentPane().setLayout(new BorderLayout());
		
		console = new JTextPane()
		{
			public void setSize(Dimension d)
			{
				if (d.width < getParent().getSize().width)
					d.width = getParent().getSize().width;
 
				super.setSize(d);
			}
 
			public boolean getScrollableTracksViewportWidth()
			{
				return false;
			}
		};
		
		console.setBackground(DEFAULT_BACKGROUND_COLOR);
		console.setEditable(false);
				
		// Set up the tab stops
		ArrayList tabs = new ArrayList(6);
		tabs.add(new TabStop(175));
		tabs.add(new TabStop(225));
		tabs.add(new TabStop(275));
		tabs.add(new TabStop(325));
		tabs.add(new TabStop(375));
		Style style = console.getLogicalStyle();
		StyleConstants.setTabSet(style, new TabSet((TabStop[])(tabs.toArray(new TabStop[0]))));
		console.setLogicalStyle(style);
		
		// Never show horizontal scrollbar (commented out just in case)
		//getContentPane().add(new JScrollPane(m_list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
		getContentPane().add(new JScrollPane(console, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
		
		JPanel inputPanel = new JPanel(new BorderLayout(5, 5));
		inputPanel.add(new JLabel(">>", JLabel.CENTER), BorderLayout.WEST);
		
		input = new JTextField();
		input.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {
				
			}

			public void keyPressed(KeyEvent e) {
				analyzeKeyInput(e);
			}

			public void keyReleased(KeyEvent e) {
				
			}
			
			private void analyzeKeyInput(KeyEvent e)
			{
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					executeInput();
					e.consume();
				}
				else if(e.getKeyCode() == KeyEvent.VK_UP)
				{
					if(memIdx > 0)
					{
						--memIdx;
					}
					
					if(memIdx < mem.size())
					{
						input.setText((String)mem.get(memIdx));						
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_DOWN)
				{
					if((memIdx + 1) < mem.size())
					{
						++memIdx;
					}
					
					if(memIdx < mem.size())
					{
						input.setText((String)mem.get(memIdx));
					}
				}
			}
		});
		
		inputPanel.add(input, BorderLayout.CENTER);
		
		inputPanel.add(new JButton(new AbstractAction("Enter") {
			public void actionPerformed(ActionEvent e) {
				executeInput();
			}
		}), BorderLayout.EAST);
		
		getContentPane().add(inputPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * Causes the contents of the JTextField <code>m_input</code> to be executed.
	 */
	private void executeInput()
	{
		executeInput(this.input.getText());
		this.input.setText("");
	}
	
	/**
	 * Appends the given text to the bottom of the console window and scrolls the window to the bottom.
	 */
	private void appendTextLn(String text, AttributeSet attr)
	{
		appendText(text + "\n", attr);
	}
	
	/**
	 * Appends the given text to the bottom of the console window and scrolls the window to the bottom.
	 */
	private void appendText(String text, AttributeSet attr)
	{
		try
		{
			this.console.getStyledDocument().insertString(this.console.getStyledDocument().getLength(), text, attr);
		}
		catch(BadLocationException e) {
			e.printStackTrace();
		}
		
		// Scroll to bottom
		Rectangle bounds = this.console.getBounds();
		this.console.scrollRectToVisible(new Rectangle(bounds.x, bounds.height + 1, bounds.width, bounds.height + 1));
	}
	
	/**
	 * Instructs the ConsoleGUI to terminate
	 */
	public void exitConsole()
	{
		setVisible(false);
	}
	
	public void setParser(InputParser parser)
	{
		this.parser = parser;
	}
	
	public InputParser getParser()
	{
		return this.parser;
	}
	
	/**
	 * Invokes execution of the inputted line
	 * @param inputLine the command line to be executed
	 */
	public void executeInput(String inputLine)
	{
		// Add to memory
		mem.add(inputLine);
		memIdx = mem.size();
		
		if(parser == null)
		{
			writeErrorMessage("You must set the parser object using the setParser(InputParser) method prior " +
					"to processing any input.");
			return;
		}
		
		try
		{
			String res = parser.parse(inputLine);
			
			if(res != null)
			{
				writeTextMessage(res);
			}
		}
		catch(ParseException e)
		{
			if(e.getErrorOffset() >= 0)
			{
				writeErrorMessage(e.getMessage() + " at position " + e.getErrorOffset());				
			}			
			else
			{
				writeErrorMessage(e.getMessage());
			}
		}
	}
	
	/**
	 * Main method to output the data of a Flight.
	 * @param f the Flight to output
	 * @param blundering 'true' if this Flight is Blundering, 'false' otherwise.
	 * @param traj The flight's Trajectory, or 'null' if no Trajectory is present.
	 */
	public void writeFlight(Flight f, boolean blundering, Trajectory traj, ConsoleParameters params)
	{
		if(f == null)
		{
			throw new IllegalArgumentException("'f' Flight argument in ConsoleGUI.writeFlight cannot be null.");
		}
		
		// Halt output if this Flight does not match output criteria
		if(!params.displayFlight(ConsoleParameters.SHOW_FLIGHTS, f, blundering))
		{
			return;
		}
		
		appendText("Flight:", TitleAttribute);
		if(blundering)
		{
			appendText(f.getAircraftId() + "\t", HighlightTitleAttribute);
		}
		else
		{
			appendText(f.getAircraftId() + "\t", TitleAttribute);
		}
		
		// Add flight track, if present
		if(f.getFlightTrack() != null)
		{
			writeFlightTrack(f.getFlightTrack());
		}
		
		// Add flight plan, if present
		if(f.getFlightPlan() != null &&
				params.displayFlight(ConsoleParameters.SHOW_ROUTES, f, blundering))
		{
			writeFlightPlan(f.getFlightPlan());
		
			if(f.getFlightPlan().getRoute() != null && 
					params.displayFlight(ConsoleParameters.SHOW_FIXES, f, blundering))
			{
				writeRoute(f.getFlightPlan().getRoute());			
			}		
		}
		
		// Add the trajectory, if present
		if(traj != null &&
				params.displayFlight(ConsoleParameters.SHOW_TRAJECTORIES, f, blundering))
		{
			writeTrajectory(traj);
		}
		
		if(blundering)
		{
			appendText("Conformance Status:\t", TextAttribute);
			appendTextLn("blundering", HighlightTextAttribute);
		}
		
		appendText("\n", TitleAttribute);
	}
	
	/**
	 * Output method for a simple text message.
	 * @param message The text message to output.
	 */
	public void writeTextMessage(String message)
	{
		appendTextLn(message, MessageAttribute);
	}
	
	/**
	 * Output method for an error message.
	 * @param message The text message to output.
	 */
	public void writeErrorMessage(String message)
	{
		appendTextLn(message, ErrorAttribute);
	}
	
	/**
	 * Formats and writes the content of a FlightTrack to the output.  Called as a subordinate to
	 * <code>writeFlight</code>. 
	 * @param ft the FlightTrack to write
	 * @see FlightTrack
	 * @see #writeFlight(Flight, boolean, Trajectory, ConsoleParameters)
	 */
	private void writeFlightTrack(FlightTrack ft)
	{
		StringBuffer sb = new StringBuffer();
		sb.append(constructDegreeMinuteString(ft.getLatitude(), 'N', 'S'));
		sb.append("  ");
		sb.append(constructDegreeMinuteString(ft.getLongitude(), 'N', 'S'));
		sb.append("  ");
		sb.append(constructAltitudeString(ft.getAltitude()));
		sb.append("  ");
		sb.append(constructSpeedString(ft.getSpeed()));
		sb.append("  ");
		sb.append(constructHeadingString(ft.getHeading()));
		
		appendTextLn(sb.toString(), TextAttribute);
	}
	
	/**
	 * Formats and writes the contents of a FlightPlan to the output.  Called as a subordinate to
	 * <code>writeFlight</code>.
	 * @param fp the FlightPlan to write
	 * @see FlightPlan
	 * @see #writeFlight(Flight, boolean, Trajectory, ConsoleParameters)
	 */
	private void writeFlightPlan(FlightPlan fp)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Flight Plan:\t");
		sb.append(constructAltitudeString(fp.getAssignedAltitude()));
		sb.append("  ");
		sb.append(constructSpeedString(fp.getAssignedSpeed()));			
		appendTextLn(sb.toString(), TextAttribute);
	}
	
	/**
	 * Formats and writes the contents of a Route to the output.  Called as a subordinate to
	 * <code>writeFlight</code>.
	 * @param route the route
	 * @see Route
	 * @see #writeFlight(Flight, boolean, Trajectory, ConsoleParameters)
	 */
	private void writeRoute(Route route)
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append("\t");
		
		Iterator it = route.fixIterator();
		while(it.hasNext())
		{
			Fix fix = ((Fix)it.next());
			sb.append(fix.getId());
			
			if(it.hasNext()) sb.append(' ');
		}
		
		appendTextLn(sb.toString(), TextAttribute);
	}
	
	/**
	 * Formats and writes the contents of a Trajectory to the output.  Called as a subordinate to
	 * <code>writeFlght</code>.
	 * @param t the Trajectory to write
	 * @see Trajectory
	 * @see #writeFlight(Flight, boolean, Trajectory, ConsoleParameters)
	 */
	private void writeTrajectory(Trajectory t)
	{
		if(!t.isEmpty())
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("Trajectories:\t");
			
			Iterator it = t.pointIterator();
			while(it.hasNext())
			{
				Point4D curPoint = ((Point4D)it.next());
				sb.append(constructDegreeMinuteString(curPoint.getLatitude(), 'N', 'S'));
				sb.append("   ");
				sb.append(constructDegreeMinuteString(curPoint.getLongitude(), 'E', 'W'));
				
				if(it.hasNext())
				{
					sb.append("        ");
				}
			}
			
			appendTextLn(sb.toString(), TextAttribute);
		}
	}
	
	/**
	 * Constructs and formats a Degree/Minute String from a radian <code>double</code> value.
	 * @param val the radian value
	 * @param posChr The character to postpend if the calculated degrees are positive ('N','E')
	 * @param negChr The character to postpend if the calculated degrees are negative ('S','W')
	 * @return a String containing the formatted radian value in degrees with the proper indicator.
	 */
	private String constructDegreeMinuteString(double val, char posChr, char negChr)
	{
		double abs = Math.abs(val);
		double degrees = Math.floor(abs);
		double minutes = Math.round((abs - degrees) * 60.0);
		
		NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(1);
		nf.setMaximumIntegerDigits(3);
						
		return (nf.format(degrees) + "� " +
				nf.format(minutes) + "' " +
				((val >= 0.0D) ? posChr : negChr));
	}
	
	/**
	 * Constructs and formats an Altitude String.
	 * @param alt the altitude
	 * @return a formatted altitude string
	 */
	private String constructAltitudeString(double alt)
	{
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(6);
		nf.setGroupingUsed(true);
		
		return (nf.format(alt) + " m");
	}
	
	/**
	 * Constructs and formats a Speed String.
	 * @param speed the speed
	 * @return a formatted speed string.
	 */
	private String constructSpeedString(double speed)
	{
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(6);
		nf.setGroupingUsed(true);
		
		return (nf.format(speed) + " km/h | " +
				nf.format(speed * 0.539956803) + " knots");
	}
	
	/**
	 * Constructs and formats a Heading String.
	 * @param heading the heading
	 * @return a formatted heading string.
	 */
	private String constructHeadingString(double heading)
	{
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMaximumIntegerDigits(3);
		nf.setGroupingUsed(false);
		
		return (nf.format(heading) + "�");
	}
	
	public void writeUserParameters(UserParameters params)
	{
		appendTextLn("User Parameters", ConsoleGUI.TitleAttribute);
			
		appendText("Lateral Threshold:\t", TextAttribute);
		appendTextLn(Double.toString(params.cmLateralThreshold), ((params.cmLateralWeightOn) ? TextAttribute : HighlightTextAttribute));
		appendText("Vertical Threshold:\t", TextAttribute);
		appendTextLn(Double.toString(params.cmVerticalThreshold), ((params.cmVerticalWeightOn) ? TextAttribute : HighlightTextAttribute));
		appendText("Angular Threshold:\t", TextAttribute);
		appendTextLn(Double.toString(params.cmAngularThreshold), ((params.cmAngularWeightOn) ? TextAttribute : HighlightTextAttribute));
		appendText("Speed Threshold:\t", TextAttribute);
		appendTextLn(Double.toString(params.cmSpeedThreshold), ((params.cmSpeedWeightOn) ? TextAttribute : HighlightTextAttribute));
		appendText("Residual Threshold:\t", TextAttribute);
		appendTextLn(Double.toString(params.cmResidualThreshold), TextAttribute);
		appendText("Time Horizon:\t", TextAttribute);
		appendTextLn(Double.toString(params.tsTimeHorizon), TextAttribute);
	}
	
	public void writeConsoleParameters(ConsoleParameters consoleParams)
	{
		appendTextLn("Console Parameters", ConsoleGUI.TitleAttribute);
		
		appendText("Selection:\t", TextAttribute);
		if(consoleParams.getFlightSelection() == ConsoleParameters.SELECT_ALL)
		{
			appendTextLn("ALL", HighlightTextAttribute);			
		}
		else
		{
			StringBuffer selectionBuilder = new StringBuffer();
			Iterator it = ((java.util.List)consoleParams.getFlightSelection()).iterator();
			
			while(it.hasNext())
			{
				selectionBuilder.append((String)it.next());
				if(it.hasNext())
				{
					selectionBuilder.append(", ");
				}
			}
			
			appendTextLn(selectionBuilder.toString(), TextAttribute);
		}
		
		appendText("Show Fixes:\t", TextAttribute);
		appendTextLn(ConsoleParameters.tagToString(consoleParams.getFixes()), TextAttribute);
		appendText("Show Flights:\t", TextAttribute);
		appendTextLn(ConsoleParameters.tagToString(consoleParams.getFlights()), TextAttribute);
		appendText("Show Routes:\t", TextAttribute);
		appendTextLn(ConsoleParameters.tagToString(consoleParams.getRoutes()), TextAttribute);
		appendText("Show Trajectories:\t", TextAttribute);
		appendTextLn(ConsoleParameters.tagToString(consoleParams.getTrajectories()), TextAttribute);
	}
	
	public void writeSection(String title)
	{
		appendTextLn("\n" + title + "\n", TitleAttribute);
	}
}
