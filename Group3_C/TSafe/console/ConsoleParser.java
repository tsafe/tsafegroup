/*
 * tsafe2c/tsafe.console
 * ConsoleParser
 * ConsoleParser.java
 * Created on Dec 2, 2004
 */
package tsafe.console;

import java.util.*;
import java.text.ParseException;

/**
 * @author JG
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConsoleParser implements InputParser {
    private ConsoleEngine	engine;
    
	public ConsoleParser(ConsoleEngine engine){
		this.engine = engine;
	}
	
	/* (non-Javadoc)
	 * @see tsafe.console.InputParser#parse(java.lang.String)
	 */
	public String parse(String input) throws ParseException {
		String []inputPiece;
		inputPiece = input.split("\\s+");
		
		if(inputPiece[0].equalsIgnoreCase("select"))
			return parseSelect(inputPiece);
		else if(inputPiece[0].equalsIgnoreCase("show"))
			return parseShow(inputPiece);
		else if(inputPiece[0].equalsIgnoreCase("set") || 
				inputPiece[0].equalsIgnoreCase("enable") || 
				inputPiece[0].equalsIgnoreCase("disable"))
			return parseChange(inputPiece);
		else
			throw new ParseException("Command is not 'select', 'show', 'set', 'enable', or 'disable'", 0);
				
	}
	
	public String parseSelect(String []inputPiece) throws ParseException {
		List flightList = new ArrayList();
		
		StringBuffer sb = new StringBuffer();
		sb.append("Selection changed to: ");
		
		if(inputPiece.length > 1 && !inputPiece[1].equals("*")){
			for(int x = 0; x < inputPiece.length - 1; ++x)
			{
				flightList.add(inputPiece[x+1]);
				sb.append(inputPiece[x+1]);
				
				if((x + 2) < inputPiece.length)
				{
					sb.append(", ");
				}
			}
		
			this.engine.getConsoleParameters().setFlightSelection(flightList);
			
			return sb.toString();
		}
		
		else if(inputPiece.length > 1)
		{
			this.engine.getConsoleParameters().setFlightSelection(ConsoleParameters.SELECT_ALL);
			
			return "Selection changed to ALL";
		}		
		
		throw new ParseException("No flights chosen to select", 1);
		
	}
	
	public String parseShow(String []inputPiece) throws ParseException {
		StringBuffer sb = new StringBuffer();
		Object key = null;
		sb.append("Show ");
		
		if(inputPiece.length != 3)
			throw new ParseException("Must have 3 arguments", -1);
		
		
		else if( !(inputPiece[1].equalsIgnoreCase("fixes") || 
				inputPiece[1].equalsIgnoreCase("flights") || 
				inputPiece[1].equalsIgnoreCase("routes") || 
				inputPiece[1].equalsIgnoreCase("trajectories")) )
			throw new ParseException("Can only show 'fixes', 'flights', 'routes', or 'trajectories'", 1);
		
		
		else if( inputPiece[1].equalsIgnoreCase("fixes") ){
			sb.append("Fixes");
			key = ConsoleParameters.SHOW_FIXES;
			
			if(inputPiece[2].equalsIgnoreCase("all"))
				this.engine.getConsoleParameters().setFixes(ConsoleParameters.ALL);
			else if(inputPiece[2].equalsIgnoreCase("none"))
				this.engine.getConsoleParameters().setFixes(ConsoleParameters.NONE);
			else
				throw new ParseException("Can only show 'all' or 'none' fixes", 2);
		}
		
		else if( inputPiece[1].equals("flights")){ 
			sb.append("Flights");
			key = ConsoleParameters.SHOW_FLIGHTS;
			
			if(inputPiece[2].equalsIgnoreCase("all"))
				this.engine.getConsoleParameters().setFlights(ConsoleParameters.ALL);
			else if(inputPiece[2].equalsIgnoreCase("selected"))
				this.engine.getConsoleParameters().setFlights(ConsoleParameters.SELECTED);
			else if(inputPiece[2].equalsIgnoreCase("withplans"))
				this.engine.getConsoleParameters().setFlights(ConsoleParameters.WITHPLANS);			
			else if(inputPiece[2].equalsIgnoreCase("conforming"))
				this.engine.getConsoleParameters().setFlights(ConsoleParameters.CONFORMING);
			else if(inputPiece[2].equalsIgnoreCase("blundering"))
				this.engine.getConsoleParameters().setFlights(ConsoleParameters.BLUNDERING);
			else if(inputPiece[2].equalsIgnoreCase("none"))
				this.engine.getConsoleParameters().setFlights(ConsoleParameters.NONE);
			else
				throw new ParseException("Wrong keyword entered after show flights", 2);
			
		}
			
		else if( inputPiece[1].equals("routes")){ 
			sb.append("Routes");
			key = ConsoleParameters.SHOW_ROUTES;
			
			if(inputPiece[2].equalsIgnoreCase("all"))
				this.engine.getConsoleParameters().setRoutes(ConsoleParameters.ALL);
			else if(inputPiece[2].equalsIgnoreCase("selected"))
				this.engine.getConsoleParameters().setRoutes(ConsoleParameters.SELECTED);
			else if(inputPiece[2].equalsIgnoreCase("conforming"))
				this.engine.getConsoleParameters().setRoutes(ConsoleParameters.CONFORMING);
			else if(inputPiece[2].equalsIgnoreCase("blundering"))
				this.engine.getConsoleParameters().setRoutes(ConsoleParameters.BLUNDERING);
			else if(inputPiece[2].equalsIgnoreCase("none"))
				this.engine.getConsoleParameters().setRoutes(ConsoleParameters.NONE);
			else
				throw new ParseException("Wrong keyword entered after show routes", 2);
		}
		
		else if( inputPiece[1].equals("trajectories")){ 
			sb.append("Trajectories");
			key = ConsoleParameters.SHOW_TRAJECTORIES;
			
			if(inputPiece[2].equalsIgnoreCase("all"))
				this.engine.getConsoleParameters().setTrajectories(ConsoleParameters.ALL);
			else if(inputPiece[2].equalsIgnoreCase("selected"))
				this.engine.getConsoleParameters().setTrajectories(ConsoleParameters.SELECTED);
			else if(inputPiece[2].equalsIgnoreCase("withplans"))
				this.engine.getConsoleParameters().setTrajectories(ConsoleParameters.WITHPLANS);			
			else if(inputPiece[2].equalsIgnoreCase("conforming"))
				this.engine.getConsoleParameters().setTrajectories(ConsoleParameters.CONFORMING);
			else if(inputPiece[2].equalsIgnoreCase("blundering"))
				this.engine.getConsoleParameters().setTrajectories(ConsoleParameters.BLUNDERING);
			else if(inputPiece[2].equalsIgnoreCase("none"))
				this.engine.getConsoleParameters().setTrajectories(ConsoleParameters.NONE);
			else
				throw new ParseException("Unknown keyword entered after show trajectories", 2);
		}
		
		sb.append(" set to: " );
		sb.append(ConsoleParameters.tagToString(this.engine.getConsoleParameters().getParametersTable().get(key)));
		return sb.toString();
	}
	
	public String parseChange(String []inputPiece) throws ParseException {
		StringBuffer sb = new StringBuffer();
		
		if(inputPiece[0].equals("set")){
			if(inputPiece.length != 4)
				throw new ParseException("Must have 3 arguments", -1);
			if(!inputPiece[1].equalsIgnoreCase("parameter"))
				throw new ParseException("Second parameter must be \"parameter\"", 1);
			
			sb.append("Set parameter: ");
			sb.append(inputPiece[2]);
			sb.append(" = ");
			double val = Double.valueOf(inputPiece[3]).doubleValue();
			sb.append(val);
			
			if(val < 0)
				throw new ParseException("Cannot set parameter to a negative value", 3);
			
			else if(inputPiece[2].equalsIgnoreCase("threslat"))
				this.engine.getUserParameters().cmLateralThreshold = val;
			else if(inputPiece[2].equalsIgnoreCase("thresver"))
			    this.engine.getUserParameters().cmVerticalThreshold = val;
			else if(inputPiece[2].equalsIgnoreCase("thresang")){
				if(val > 2 * 3.1415926535)
					throw new ParseException("Cannot set angular value greater than 2 pi", 3);
				else
					this.engine.getUserParameters().cmAngularThreshold = val;
			}
			else if(inputPiece[2].equalsIgnoreCase("thresspe"))
				this.engine.getUserParameters().cmSpeedThreshold = val;
			else if(inputPiece[2].equalsIgnoreCase("thresres"))
				this.engine.getUserParameters().cmResidualThreshold = val;
			else if(inputPiece[2].equalsIgnoreCase("horiztim"))
				this.engine.getUserParameters().tsTimeHorizon = Long.valueOf(inputPiece[3]).longValue();
		
			else
				throw new ParseException("Trying to set an illegal parameter", 2);
		}
			
		else{
			boolean enable = true;
			
			if(inputPiece.length != 3)
				throw new ParseException("Must have 2 arguments", -1);
			if(!inputPiece[1].equalsIgnoreCase("parameter"))
				throw new ParseException("Second parameter must be \"parameter\"", 1);	
			
			if(inputPiece[0].equalsIgnoreCase("disable"))
			{
				enable = false;
				sb.append("Disable parameter: ");
			}
			else
			{
				sb.append("Enable parameter: ");
			}
			
			sb.append(inputPiece[2]);
			
			if(inputPiece[2].equalsIgnoreCase("threslat"))
				this.engine.getUserParameters().cmLateralWeightOn = enable;
			
			else if(inputPiece[2].equalsIgnoreCase("thresver"))
				this.engine.getUserParameters().cmVerticalWeightOn = enable;
			
			else if(inputPiece[2].equalsIgnoreCase("thresang"))
				this.engine.getUserParameters().cmAngularWeightOn = enable;
			
			else if(inputPiece[2].equalsIgnoreCase("thresspe"))
				this.engine.getUserParameters().cmSpeedWeightOn = enable;
			
			else if(inputPiece[2].equalsIgnoreCase("thresres") ||
					inputPiece[2].equalsIgnoreCase("horiztim"))
				throw new ParseException("Cannot enable or disable this parameter", 2);
			
			else
				throw new ParseException("Unknown keyword", 2);
		}
		
		// Assume: no ParseException thrown indicates a parameters
		// change was made.
		this.getEngine().notifySettingsChanged();
		
		return sb.toString();
	}
		
	/**
	 * Accessor for ConsoleEngine
	 * @return the ConsoleEngine
	 */
	public ConsoleEngine getEngine()
	{
		return this.engine;
	}
}
