/*
 * tsafe2c/tsafe.console
 * TestingParser
 * TestingParser.java
 * Created on Dec 2, 2004
 */
package tsafe.console;

import java.text.ParseException;

import tsafe.common_datastructures.*;

/**
 * @author Dan
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TestingParser extends ConsoleParser {
	// Create generic template that shows everything all the time.
	private static ConsoleParameters universalParameters = new ConsoleParameters();	
	static
	{
		universalParameters.selectAll();
	}
	
	private ConsoleGUI 			gui;
	
	public TestingParser(ConsoleGUI gui, ConsoleEngine engine)
	{
		super(engine);
		
		this.gui = gui;
	}
	
	/* (non-Javadoc)
	 * @see tsafe.console.InputParser#parse(java.lang.String)
	 */
	public String parse(String input) throws ParseException {
		String[] parts = input.split("\\s+");
		
		if(parts.length > 0)
		{
			if(parts[0].equalsIgnoreCase("test"))
			{
				Flight f = generateRandomFlight("Test");
				boolean blunder = false;
				Trajectory traj = null;
				
				for(int i = 1; i < parts.length; i++)
				{
					if(!blunder && parts[i].equalsIgnoreCase("blunder"))
					{
						blunder = true;					
					}
					else if(traj == null && parts[i].equalsIgnoreCase("traj"))
					{
						traj = generateRandomTrajectory();
					}
				}
				
				gui.writeFlight(f, blunder, traj, universalParameters);
				gui.writeFlight(f, blunder, traj, super.getEngine().getConsoleParameters());
			}
			else if(parts[0].equalsIgnoreCase("cparams"))
			{
				gui.writeConsoleParameters(super.getEngine().getConsoleParameters());				
			}
			else if(parts[0].equalsIgnoreCase("uparams"))
			{
				gui.writeUserParameters(super.getEngine().getUserParameters());
			}
			else
			{
				return super.parse(input);			
			}
		}
		
		return null;
	}
	
	/**
	 * TODO: Testing method used to construct a random Flight object for output purposes.
	 * @param title the name of the Flight
	 * @return a randomly-generated Flight object.
	 */
	private Flight generateRandomFlight(String title)
	{
		Point4D basePt = generateRandomPoint4D(0);
		
		FlightTrack ft = new FlightTrack(basePt.getLatitude(), 
				basePt.getLongitude(),
				basePt.getAltitude(),
				basePt.getTime(),				
				(Math.random() * 10), 
				(Math.random() * 10));
		
		Route r = new Route();
		
		int iterations = (int)(Math.random() * 15) + 1;
		long time = 0;
		for(int i = 0; i < iterations; i++)
		{
			int len = ((int)(Math.random() * 6)) + 3;
			StringBuffer sb = new StringBuffer(len);
			for(int j = 0; j < len; j++)
			{
				char ch = (char)(((Math.random() > .5) ? 'A' : 'a') + ((int)(Math.random() * 25))); 
				sb.append(ch);				
			}
			
			Point4D fixPt = generateRandomPoint4D(time);
			time = fixPt.getTime();			
			r.addFix(new Fix(sb.toString(), fixPt.getLatitude(), fixPt.getLongitude()));
		}
		
		Point4D fpPoint = generateRandomPoint4D(basePt.getTime());
		FlightPlan fp = new FlightPlan((Math.random() * 10), fpPoint.getAltitude(), r);
		
		return new Flight(title, ft, fp);
	}
	
	/**
	 * TODO: Testing method used to construct a random Trajectory object for output purposes.
	 * @return a randomly-generated Trajectory object.
	 */
	private Trajectory generateRandomTrajectory()
	{
		Trajectory ret = new Trajectory();
		int len = ((int)Math.random() * 6) + 2;
		long time = 0;
		for(int i = 0; i < len; i++)
		{
			Point4D curPt = generateRandomPoint4D(time);
			time = curPt.getTime();
			ret.addPoint(curPt);
		}
		
		return ret;
	}
	
	private Point4D generateRandomPoint4D(long tOffset)
	{
		return new Point4D(
				((Math.random() * 360) - 180),
				((Math.random() * 360) - 180),
				(Math.random() * 5000),
				(tOffset + ((long)(Math.random() * 1000))));
	}
}
