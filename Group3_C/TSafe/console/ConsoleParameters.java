/*
 * tsafe2c/tsafe.console
 * ConsoleParameters
 * ConsoleParameters.java
 * Created on Dec 2, 2004
 */
package tsafe.console;

import java.util.*;
import tsafe.common_datastructures.*;

/**
 * @author Dan
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConsoleParameters {
	// Key for Flight selection settings (acceptable values are ALL_FLIGHTS or a List of Strings).
	public static Object	FLIGHT_SELECTION		= "FLIGHT_SELECTION";	
	// Indicator to output all flights
	public static Object	SELECT_ALL				= new Object();
	// Convinience item ... empty list for no selection.
	public static Object	SELECT_NONE				= new ArrayList();
	
	// Key for Fix settings (acceptable values are ALL or NONE).
	public static Object	SHOW_FIXES				= "SHOW_FIXES";
	
	// Key for Flight output settings (acceptable values are ALL, SELECTED,
	// WITHPLANS, CONFORMING, BLUNDERING, or NONE).
	public static Object	SHOW_FLIGHTS			= "SHOW_FLIGHTS";
	
	// Key for Route output settings (acceptable values are ALL, SELECTED,
	// CONFORMING, BLUNDERING, or NONE).
	public static Object	SHOW_ROUTES				= "SHOW_ROUTES";
	
	// Key for Trajectory output settings (acceptable values are ALL,
	// SELECTED, WITHPLANS, CONFORMING, BLUNDERING, or NONE).
	public static Object	SHOW_TRAJECTORIES		= "SHOW_TRAJECTORIES";
	
	// Output should show all fields
	public final static Object	ALL	 				= new Integer(0);
	// Output should show no fields
	public final static Object	NONE				= new Integer(1);
	// Output should display only for selected Flights
	public final static Object	SELECTED			= new Integer(2);
	// Output should show only for conforming Flights
	public final static Object	CONFORMING			= new Integer(3);
	// Output should show only for blundering Flights
	public final static Object	BLUNDERING			= new Integer(4);
	// Output should show only for Flights with plans
	public final static Object	WITHPLANS			= new Integer(5);
	
	// Internal hashtable of parameters
	private Hashtable	parameters;
		
	public ConsoleParameters()
	{
		parameters = new Hashtable();
		selectAll();
	}
	
	/**
	 * Sets output parameters to select maximum output for every parameters
	 *
	 */
	public void selectAll()
	{
		setFlightSelection(SELECT_ALL);
		setFixes(ALL);
		setFlights(ALL);
		setRoutes(ALL);
		setTrajectories(ALL);
	}
	
	/**
	 * Sets output parameters to select minimal output for every parameter
	 *
	 */
	public void selectNone()
	{
		setFlightSelection(SELECT_NONE);
		setFixes(NONE);
		setFlights(NONE);
		setRoutes(NONE);
		setTrajectories(NONE);
	}
	
	/**
	 * Returns the Hashtable containing the parameters.
	 * @return the Hashtable containing the parameters
	 */
	public Hashtable getParametersTable()
	{
		return this.parameters;
	}

	/**
	 * Convinience method to set FLIGHT_SELECTION property 
	 * @param selection the new FLIGHT_SELECTION value
	 * @throws IllegalArgumentException if <code>selection</code> is not a valid value.
	 */
	public void setFlightSelection(Object selection)
	{
		if(!(selection == SELECT_ALL || (selection instanceof List)))
		{
			throw new IllegalArgumentException("'FLIGHT_SELECTION' property must either be 'SELECT_ALL' or " +
					"a List.");
		}
		
		parameters.put(ConsoleParameters.FLIGHT_SELECTION, selection);
	}
	
	/**
	 * Convinience method to get FLIGHT_SELECTION value  
	 * @return the FLIGHT_SELECTION value, which is either a constant SELECT_ALL or a List object.
	 */
	public Object getFlightSelection()
	{
		return parameters.get(FLIGHT_SELECTION);		
	}
	
	/**
	 * Convinience method to set SHOW_FIXES with error checking
	 * @param value
	 * @throws IllegalArgumentException if <code>value</code> is not a valid value. 
	 */
	public void setFixes(Object value)
	{
		if(!(value == ALL || value == NONE))
		{
			throw new IllegalArgumentException("'SHOW_FIXES' property must be either ALL or NONE.");
		}
		
		parameters.put(ConsoleParameters.SHOW_FIXES, value);
	}
	
	/**
	 * Convinience method to get SHOW_FIXES
	 * @return the value for SHOW_FIXES parameter
	 */
	public Object getFixes()
	{
		return parameters.get(ConsoleParameters.SHOW_FIXES);
	}
		
	/**
	 * Convinience method to set SHOW_FLIGHTS with error checking
	 * @param value
	 * @throws IllegalArgumentException if <code>value</code> is not a valid value. 
	 */
	public void setFlights(Object value)
	{
		if(!(value == ALL || value == SELECTED || value == WITHPLANS ||
				value == CONFORMING || value == BLUNDERING || value == NONE))
		{
			throw new IllegalArgumentException("'SHOW_FLIGHTS' property must be either ALL, " + 
					"SELECTED, WITHPLANS, CONFORMING, BLUNDERING, " +
					"or NONE.");
		}
		
		parameters.put(ConsoleParameters.SHOW_FLIGHTS, value);
	}
	
	/**
	 * Convinience method to get SHOW_FLIGHTS
	 * @return the value for SHOW_FLIGHTS parameter
	 */
	public Object getFlights()
	{
		return parameters.get(ConsoleParameters.SHOW_FLIGHTS);
	}
		
	/**
	 * Convinience method to set SHOW_ROUTES with error checking
	 * @param value
	 * @throws IllegalArgumentException if <code>value</code> is not a valid value. 
	 */
	public void setRoutes(Object value)
	{
		if(!(value == ALL || value == SELECTED || value == CONFORMING || 
				value == BLUNDERING || value == NONE))
		{
			throw new IllegalArgumentException("'SHOW_ROUTES' property must be either ALL, " + 
					"SELECTED, CONFORMING, BLUNDERING, or NONE.");
		}
		
		parameters.put(ConsoleParameters.SHOW_ROUTES, value);
	}
	
	/**
	 * Convinience method to get SHOW_ROUTES
	 * @return the value for SHOW_ROUTES parameter
	 */
	public Object getRoutes()
	{
		return parameters.get(ConsoleParameters.SHOW_ROUTES);
	}
	
	/**
	 * Convinience method to set SHOW_TRAJECTORIES with error checking
	 * @param value
	 * @throws IllegalArgumentException if <code>value</code> is not a valid value. 
	 */
	public void setTrajectories(Object value)
	{
		if(!(value == ALL || value == SELECTED || value == WITHPLANS || 
				value == CONFORMING || value == BLUNDERING || value == NONE))
		{
			throw new IllegalArgumentException("'SHOW_TRAJECTORIES' property must be either ALL, " + 
					"SELECTED, WITHPLANS, CONFORMING, BLUNDERING, or NONE.");
		}
		
		parameters.put(ConsoleParameters.SHOW_TRAJECTORIES, value);
	}
	
	/**
	 * Convinience method to get SHOW_TRAJECTORIES
	 * @return the value for SHOW_TRAJECTORIES parameter
	 */
	public Object getTrajectories()
	{
		return parameters.get(ConsoleParameters.SHOW_TRAJECTORIES);
	}
	
	public boolean displayFlight(Object field, Flight f, boolean blundering)
	{
		Object val = this.parameters.get(field);
		
		if(val == null)
		{
			throw new IllegalArgumentException("'field' parameter must be either SHOW_FIXES, " +
				"SHOW_FLIGHTS, SHOW_ROUTES, or SHOW_TRAJECTORIES.");
		}
		else if(val == ALL)
		{
			return true;
		}
		else if(val == NONE)
		{
			return false;
		}
		else if(val == SELECTED)
		{
			return flightIncluded(f);
		}
		else if(val == WITHPLANS)
		{
			return (f.getFlightPlan() != null);
		}
		else if(val == CONFORMING)
		{
			return (!blundering);
		}
		else if(val == BLUNDERING)
		{
			return blundering;
		}
		
		throw new IllegalStateException("Invalid parameter value for " + field + ": " + val);
	}
	
	/**
	 * Determines whether or not the given Flight is in the current Flight selection scope.
	 * @param f the Flight to test
	 * @return 'true' if the given Flight is in the selection scope.
	 */
	private boolean flightIncluded(Flight f)
	{
		if(getFlightSelection() == SELECT_ALL)
		{
			return true;
		}
		
		Iterator it = ((List)getFlightSelection()).iterator();
		while(it.hasNext())
		{
			String curFlight = ((String)it.next());
			if(f.getAircraftId().equals(curFlight))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public static String tagToString(Object tag)
	{
		if(tag == ConsoleParameters.ALL)
		{
			return "All";
		}
		else if(tag == ConsoleParameters.NONE)
		{
			return "None";
		}
		else if(tag == ConsoleParameters.SELECTED)
		{
			return "Selected";
		}
		else if(tag == ConsoleParameters.CONFORMING)
		{
			return "Conforming";
		}
		else if(tag == ConsoleParameters.BLUNDERING)
		{
			return "Blundering";
		}
		else if(tag == ConsoleParameters.WITHPLANS)
		{
			return "Withplans";
		}
		
		return "UNKNOWN";
	}
}
