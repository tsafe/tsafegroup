/*
 * tsafe2c/console
 * ConsoleEngine
 * ConsoleEngine.java
 * Created on Nov 27, 2004
 */
package tsafe.console;

import java.awt.event.*;
import java.util.*;

import tsafe.server.*;
import tsafe.common_datastructures.*;
import tsafe.common_datastructures.communication.*;

/**
 * @author Dan
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConsoleEngine implements Runnable, ClientInterface {
	private ConsoleGUI			gui;
	private LatLonBounds		bounds;
	private ServerEngine		engine;
	private UserParameters		parameters;
	private ConsoleParameters	consoleParameters;
	private InputParser			parser;
	
	/**
	 * Creates a new ConsoleEngine
	 *
	 */
	public ConsoleEngine()
	{
		this.gui = new ConsoleGUI();
		this.gui.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				// Remove this client from the server's observer list
				ConsoleEngine.this.engine.detachObserver(ConsoleEngine.this);
			}
		});
		
		
		this.parameters = new UserParameters();
		this.consoleParameters = new ConsoleParameters();
		this.bounds = TSAFEProperties.getLatLonBounds();
		//this.parser = new ConsoleParser(this);
		this.parser = new TestingParser(gui, this);
		
		this.gui.setParser(this.parser);
		gui.setSize(600, 500);
		
		// Center on screen
		gui.setLocationRelativeTo(null);
		//gui.setVisible(true);
		
		gui.writeTextMessage("-TSafe Console Initialized-");
	}
		
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		// Required to be runnable from ClientInterface interface; not implemented.
		gui.setVisible(true);
	}
	
	/**
	 * Writes the contents of the ComputationResults into the Console.
	 * @param cr the ComputationResults
	 */
	private void writeComputationResults(ComputationResults cr)
	{
		gui.writeSection("TSafe Update: " + Calendar.getInstance().getTime().toString());
		gui.writeConsoleParameters(this.consoleParameters);
		gui.writeUserParameters(this.parameters);
		
		// Loop through flights
		Iterator it = cr.getFlights().iterator();
		
		while(it.hasNext())
		{
			Flight curFlight = ((Flight)it.next());
			
			gui.writeFlight(curFlight, 
					cr.getBlunders().contains(curFlight),
					((Trajectory)cr.getFlight2TrajectoryMap().get(curFlight)),
					consoleParameters);
		}
	}
	
	/**
	 * Returns this client's UserParameters.
	 * @return this client's UserParameters
	 */
	public UserParameters getParameters()
	{
		return this.parameters;
	}

	/**
	 * Called by server to initiate a Client update 
	 * @see tsafe.server.ClientInterface#notifyClient()
	 */
	public void notifyClient() {
		ComputationResults results = this.engine.computeFlights(this.parameters);
		writeComputationResults(results);
	}

	/**
	 * Called by server to set Client bounds
	 * @see tsafe.server.ClientInterface#setBounds(tsafe.common_datastructures.LatLonBounds)
	 */
	public void setBounds(LatLonBounds bounds) {
		this.bounds = bounds;
	}

	/**
	 * Sets the server
	 * @see tsafe.server.ClientInterface#setServer(tsafe.server.ServerEngine)
	 */
	public void setServer(ServerEngine engine) {
		this.engine = engine;
	}
	
	/**
	 * Propogates the UserParameters to the server
	 */
	public void propogateSettings(UserParameters params)
	{
	    this.parameters = params;
	}
	
	/**
	 * Called to notify ConsoleEngine that some part of the UserParameters
	 * has changed, and propogation is needed.
	 *
	 */
	public void notifySettingsChanged()
	{
	    this.engine.settingsChanged(this.parameters);
	}
	
	/**
	 * Accessor for UserParameters
	 * @return the UserParameters for this Engine
	 */
	public UserParameters getUserParameters()
	{
	    return this.parameters;
	}
	
	/**
	 * Accessor for ConsoleParameters
	 * @return the ConsoleParameters for this Engine
	 */
	public ConsoleParameters getConsoleParameters()
	{
	    return this.consoleParameters;
	}
}