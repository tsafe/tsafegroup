/*
 * tsafe2c/tsafe.console
 * InputParser
 * InputParser.java
 * Created on Dec 2, 2004
 */
package tsafe.console;

import java.text.ParseException;

/**
 * @author Dan
 */
public interface InputParser {
	/**
	 * Instructs the InputParser to parse the given input into the given UserParameters object.
	 * @param input the input string to parse
	 * @throws ParseException if there is an error in the Parse input.
	 */
	String parse(String input) throws ParseException;
}
