/*
 * Created on Nov 18, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package tsafe.client.command_line;

import java.util.Collection;
import java.util.List;

import tsafe.client.Client_Gui;
import tsafe.client.FlightMap;
import tsafe.client.ParametersDialog;
import tsafe.client.TsafeMenu;
import tsafe.common_datastructures.communication.UserParameters;

/**
 * @author ashish 10:55:33 AM
 */
public class CommandRunner
{
    UserParameters params;

    ParametersDialog pDialog;

    FlightMap fMap;
    
    TsafeMenu tMenu;
    
    Client_Gui cGui;

    public static final byte OUTPUT_ALL = 1;

    public static final byte OUTPUT_NONE = 2;

    public static final byte OUTPUT_SELECTED = 4;

    public static final byte OUTPUT_WITHPLANS = 8;

    public static final byte OUTPUT_CONFORMING = 16;

    public static final byte OUTPUT_BLUNDERING = 32;

    public static final byte PARAM_THRESLAT = 1;

    public static final byte PARAM_THRESVER = 2;

    public static final byte PARAM_THRESANG = 4;

    public static final byte PARAM_THRESSPE = 8;

    public static final byte PARAM_THRESRES = 16;

    public static final byte PARAM_HORIZTIM = 32;

    // valid parameter input masks for each show command
    public static final byte VALID_FIXES = OUTPUT_ALL | OUTPUT_NONE;

    public static final byte VALID_ROUTES = OUTPUT_ALL | OUTPUT_SELECTED | OUTPUT_CONFORMING | OUTPUT_BLUNDERING | OUTPUT_NONE;

    public static final byte VALID_FLIGHTS = VALID_ROUTES | OUTPUT_WITHPLANS;

    public static final byte VALID_TRAJECTORIES = VALID_FLIGHTS;

    public static final byte VALID_PARAMETERS = PARAM_HORIZTIM | PARAM_THRESANG | PARAM_THRESLAT | PARAM_THRESRES | PARAM_THRESSPE | PARAM_THRESVER;

    public CommandRunner(UserParameters up, Client_Gui cg)
    {
        params = up;
        pDialog = cg.paramsDialog;
        fMap = cg.flightMap;
        tMenu = cg.tsafeMenu;
        cGui = cg;
    }

    /**
     * Selects all the flights in the list
     * 
     * @param flights -
     *            List of flights (one or more) to select
     */
    public void selectFlight(List flights)
    {
		Collection selectedFlights = flights;
		fMap.setSelectedFlights(selectedFlights);
		fMap.updateNeeded();
    }

    /**
     * Selects all flights
     *  
     */
    public void selectAllFlights()
    {
    	// put all flights in the collection
    	Collection selectedFlights = cGui.flightList.getSelectedFlights();
    	fMap.setSelectedFlights(selectedFlights);
		fMap.updateNeeded();
    }

    /**
     * Show fixes
     * 
     * @param displayOption -
     *            OUTPUT_ALL | OUTPUT_NONE
     */
    public void showFixes(byte displayOption)
    {
        switch (displayOption)
        {
        case OUTPUT_ALL:
            fMap.setShowFixes(FlightMap.SHOW_ALL);
            tMenu.itemStateChanged("fixesAll","all");
            break;
        case OUTPUT_NONE:
            fMap.setShowFixes(FlightMap.SHOW_NONE);
            tMenu.itemStateChanged("fixesNone", "none");
        }
        fMap.updateNeeded();
    }

    /**
     * Show Flights
     * 
     * @param displayOption
     *            OUTPUT_ALL | OUTPUT_SELECTED | OUTPUT_WITHPLANS |
     *            OUTPUT_CONFORMING | OUTPUT_BLUNDERING | OUTPUT_NONE
     */
    public void showFlights(byte displayOption)
    {
        switch (displayOption)
        {
        case OUTPUT_ALL:
            fMap.setShowFlights(FlightMap.SHOW_ALL);
            tMenu.itemStateChanged("flightsAll","all");
            break;
        case OUTPUT_SELECTED:
            fMap.setShowFlights(FlightMap.SHOW_SELECTED);
            tMenu.itemStateChanged("flightsSelected","selected");
            break;
        case OUTPUT_WITHPLANS:
            fMap.setShowFlights(FlightMap.SHOW_WITH_PLAN);
            tMenu.itemStateChanged("flightsWithPlan","with flight plans");
            break;
        case OUTPUT_CONFORMING:
            fMap.setShowFlights(FlightMap.SHOW_CONFORMING);
            tMenu.itemStateChanged("flightsConforming","conforming");
            break;
        case OUTPUT_BLUNDERING:
            fMap.setShowFlights(FlightMap.SHOW_BLUNDERING);
            tMenu.itemStateChanged("flightsBlundering","blundering");
            break;
        case OUTPUT_NONE:
            fMap.setShowFlights(FlightMap.SHOW_NONE);
            tMenu.itemStateChanged("flightsNone","none");
            
        }
        fMap.updateNeeded();
    }

    /**
     * Show Routes
     * 
     * @param displayOption
     *            OUTPUT_ALL | OUTPUT_SELECTED | OUTPUT_CONFORMING |
     *            OUTPUT_BLUNDERING | OUTPUT_NONE
     */
    public void showRoutes(byte displayOption)
    {
        switch (displayOption)
        {
        case OUTPUT_ALL:
            fMap.setShowRoutes(FlightMap.SHOW_ALL);
            tMenu.itemStateChanged("routesAll","all");
            break;
        case OUTPUT_SELECTED:
            fMap.setShowRoutes(FlightMap.SHOW_SELECTED);
            tMenu.itemStateChanged("routesSelected","selected");
            break;
        case OUTPUT_CONFORMING:
            fMap.setShowRoutes(FlightMap.SHOW_CONFORMING);
            tMenu.itemStateChanged("routesConforming","conforming");
            break;
        case OUTPUT_BLUNDERING:
            fMap.setShowRoutes(FlightMap.SHOW_BLUNDERING);
            tMenu.itemStateChanged("routesBlundering","blundering");
            break;
        case OUTPUT_NONE:
            fMap.setShowRoutes(FlightMap.SHOW_NONE);
            tMenu.itemStateChanged("routesNone","none");
        }
        fMap.updateNeeded();
    }

    /**
     * Show Trajectories
     * 
     * @param displayOption
     *            OUTPUT_ALL | OUTPUT_SELECTED | OUTPUT_CONFORMING |
     *            OUTPUT_WITHPLANS | OUTPUT_BLUNDERING | OUTPUT_NONE
     */
    public void showTrajectories(byte displayOption)
    {
        switch (displayOption)
        {
        case OUTPUT_ALL:
            fMap.setShowTrajectories(FlightMap.SHOW_ALL);
            tMenu.itemStateChanged("trajectoriessAll","all");
            break;
        case OUTPUT_SELECTED:
            fMap.setShowTrajectories(FlightMap.SHOW_SELECTED);
            tMenu.itemStateChanged("trajectoriesSelected","selected");
            break;
        case OUTPUT_WITHPLANS:
            fMap.setShowTrajectories(FlightMap.SHOW_WITH_PLAN);
            tMenu.itemStateChanged("trajectoriesWithPlan","with flight plans");
            break;
        case OUTPUT_CONFORMING:
            fMap.setShowTrajectories(FlightMap.SHOW_CONFORMING);
            tMenu.itemStateChanged("trajectoriesConforming","conforming");
            break;
        case OUTPUT_BLUNDERING:
            fMap.setShowTrajectories(FlightMap.SHOW_BLUNDERING);
            tMenu.itemStateChanged("trajectoriesBlundering","blundering");
            break;
        case OUTPUT_NONE:
            fMap.setShowTrajectories(FlightMap.SHOW_NONE);
            tMenu.itemStateChanged("trajectoriesNone","none");
        }
        fMap.updateNeeded();
    }

    /**
     * 
     * @param parameter
     * @param parameterValue
     */
    public void setParameter(byte parameter, double parameterValue)
    {
        switch (parameter)
        {
        case CommandRunner.PARAM_THRESANG:
            params.cmAngularThreshold = parameterValue;
            break;
        case CommandRunner.PARAM_THRESLAT:
            params.cmLateralThreshold = parameterValue;
            break;
        case CommandRunner.PARAM_HORIZTIM:
            params.tsTimeHorizon = (long) parameterValue;
            break;
        case CommandRunner.PARAM_THRESRES:
            params.cmResidualThreshold = parameterValue;
            break;
        case CommandRunner.PARAM_THRESSPE:
            params.cmSpeedThreshold = parameterValue;
            break;
        case CommandRunner.PARAM_THRESVER:
            params.cmVerticalThreshold = parameterValue;
        }
        pDialog.setTextFields();
        pDialog.setEngineParameters();
    }

    public void enableParameter(byte parameter)
    {
        switch (parameter)
        {
        case CommandRunner.PARAM_THRESANG:
            params.cmAngularWeightOn = true;
            break;
        case CommandRunner.PARAM_THRESLAT:
            params.cmLateralWeightOn = true;
            break;
        case CommandRunner.PARAM_THRESSPE:
            params.cmSpeedWeightOn = true;
            break;
        case CommandRunner.PARAM_THRESVER:
            params.cmVerticalWeightOn = true;
        }
        pDialog.setTextFields();
    }

    public void disableParameter(byte parameter)
    {
        switch (parameter)
        {
        case CommandRunner.PARAM_THRESANG:
            params.cmAngularWeightOn = false;
            break;
        case CommandRunner.PARAM_THRESLAT:
            params.cmLateralWeightOn = false;
            break;
        case CommandRunner.PARAM_THRESSPE:
            params.cmSpeedWeightOn = false;
            break;
        case CommandRunner.PARAM_THRESVER:
            params.cmVerticalWeightOn = false;
        }
        pDialog.setTextFields();
    }
}