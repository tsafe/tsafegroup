/*
 * Created on Nov 21, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package tsafe.client.command_line;

/**
 * @author ashish
 *9:27:41 PM
 */
public class InvalidCommandException extends Exception
{
    public static final InvalidCommandException singleton = new InvalidCommandException();
    private InvalidCommandException()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public InvalidCommandException(String message)
    {
        super(message);
    }
}
