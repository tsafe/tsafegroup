/*
 * Created on Nov 21, 2004
 *
 *
 * Ashish Tonse
 */
package tsafe.client.command_line;

import java.util.LinkedList;

import tsafe.client.Client_Gui;
import tsafe.common_datastructures.communication.UserParameters;


/**
 * @author ashish 9:12:24 PM
 */
public class CommandParser
{
	
    public CommandParser(UserParameters up, Client_Gui cg)
    {
        cRunner = new CommandRunner(up,cg);
    }

    public void parseAndExecute(String line) throws InvalidCommandException
    {
        try
        {
            tokens = line.split("\\s+");
            tokens[0] = tokens[0].toLowerCase();

            if (tokens[0].equals("select"))
                parseSelect();
            else if (tokens[0].equals("show"))
                parseShow();
            else if (tokens[0].equals("set"))
                parseSet();
            else if (tokens[0].equals("enable"))
                parseEnable();
            else if (tokens[0].equals("disable"))
                parseDisable();
            else
                throw ICException;
        }
        catch (ArrayIndexOutOfBoundsException ae)
        {
            throw ICException;
        }

    }

    private void parseSelect() throws InvalidCommandException
    {
        if (tokens.length < 2) throw ICException;

        if (tokens[1].equals("*"))
            if (tokens.length != 2) throw ICException;
            else cRunner.selectAllFlights();
        else if (tokens.length >= 2)
        {
            java.util.List fList = new LinkedList();
            for (int x = 1; x < tokens.length; x++)
                fList.add(tokens[x]);
            cRunner.selectFlight(fList);
        }
    }

    private byte getShowCode(String str)
    {
        str = str.toLowerCase();
        if (str.equals("all"))
            return CommandRunner.OUTPUT_ALL;
        else if (str.equals("selected"))
            return CommandRunner.OUTPUT_SELECTED;
        else if (str.equals("withplans"))
            return CommandRunner.OUTPUT_WITHPLANS;
        else if (str.equals("conforming"))
            return CommandRunner.OUTPUT_CONFORMING;
        else if (str.equals("blundering"))
            return CommandRunner.OUTPUT_BLUNDERING;
        else if (str.equals("none")) 
            return CommandRunner.OUTPUT_NONE;
        return 0;
    }

    private void parseShow() throws InvalidCommandException
    {
        if (tokens.length != 3) throw ICException;

        tokens[1] = tokens[1].toLowerCase();
        byte showCode = getShowCode(tokens[2]);

        if (tokens[1].equals("fixes") && (CommandRunner.VALID_FIXES & showCode) > 0)
            cRunner.showFixes(showCode);
        else if (tokens[1].equals("flights") && (CommandRunner.VALID_FLIGHTS & showCode) > 0)
            cRunner.showFlights(showCode);
        else if (tokens[1].equals("routes") && (CommandRunner.VALID_ROUTES & showCode) > 0)
            cRunner.showRoutes(showCode);
        else if (tokens[1].equals("trajectories") && (CommandRunner.VALID_TRAJECTORIES & showCode) > 0)
            cRunner.showTrajectories(showCode);
        else
            throw ICException;
    }

    private byte getParamCode(String str)
    {
        str = str.toLowerCase();
        if (str.equals("threslat"))
            return CommandRunner.PARAM_THRESLAT;
        else if (str.equals("thresver"))
            return CommandRunner.PARAM_THRESVER;
        else if (str.equals("thresang"))
            return CommandRunner.PARAM_THRESANG;
        else if (str.equals("thresspe"))
            return CommandRunner.PARAM_THRESSPE;
        else if (str.equals("thresres"))
            return CommandRunner.PARAM_THRESRES;
        else if (str.equals("horiztim")) 
            return CommandRunner.PARAM_HORIZTIM;

        return 0;
    }

    private void parseSet() throws InvalidCommandException
    {
        if (tokens.length != 4) throw ICException;

        if (!tokens[1].toLowerCase().equals("parameter")) throw ICException;

        byte paramCode = getParamCode(tokens[2]);

        try
        {
            if ((paramCode & CommandRunner.VALID_PARAMETERS) > 0) 
                cRunner.setParameter(paramCode, Double.parseDouble(tokens[3]));
            else throw ICException;
        }
        catch (NumberFormatException nf)
        {
            throw ICException;
        }
    }

    private void parseEnable() throws InvalidCommandException
    {
        if (tokens.length != 3) throw ICException;

        if (!tokens[1].toLowerCase().equals("parameter")) throw ICException;
        
        byte paramCode = getParamCode(tokens[2]);

        if ((paramCode & CommandRunner.VALID_PARAMETERS) > 0) 
            cRunner.enableParameter(paramCode);
        else throw ICException;
    }

    private void parseDisable() throws InvalidCommandException
    {
        if (tokens.length != 3) throw ICException;

        if (!tokens[1].toLowerCase().equals("parameter")) throw ICException;
        
        byte paramCode = getParamCode(tokens[2]);

        if ((paramCode & CommandRunner.VALID_PARAMETERS) > 0) 
            cRunner.disableParameter(paramCode);
        else throw ICException;
    }

    String[] tokens;
    
    InvalidCommandException ICException = InvalidCommandException.singleton;

    public CommandRunner cRunner;
}