/*
 * Created on Dec 6, 2004
 */
package tsafe.client;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import tsafe.client.command_line.CommandParser;
import tsafe.client.command_line.InvalidCommandException;
import tsafe.common_datastructures.Fix;
import tsafe.common_datastructures.Flight;
import tsafe.common_datastructures.FlightPlan;
import tsafe.common_datastructures.FlightTrack;
import tsafe.common_datastructures.Point4D;
import tsafe.common_datastructures.Route;
import tsafe.common_datastructures.Trajectory;
import tsafe.common_datastructures.communication.ComputationResults;

/**
 * @author Nick Tedesco
 */
public class Client_Console extends JFrame implements ActionListener{

	/**
	 * Manages the communication to the server
	 */
	private ClientEngine client;
	
	/**
	 * Parses the user input
	 */
	private CommandParser parser;
	
	
	/**
	 * Text area displaying infomration
	 */
	private JTextPane output;

	/**
	 * Document that has output
	 */
	private StyledDocument outDoc;
	
	/**
	 * Scrollable window for output
	 */
	private JScrollPane outPane;
	
	/**
	 * Gets user input
	 */
	private JTextField input;
	
	private JButton enter;

// flight parameters	
	/**
     * Show options
     */
    public static final int SHOW_ALL        = 0;
    public static final int SHOW_SELECTED   = 1;
    public static final int SHOW_WITH_PLAN  = 2;
    public static final int SHOW_CONFORMING = 3;
    public static final int SHOW_BLUNDERING = 4;
    public static final int SHOW_NONE       = 5;

    /**
     * Data to print to the console
     */
    private Collection flights  = new LinkedList();
    private Collection blunders = new LinkedList();
    private Map flight2TrajMap = new HashMap(); 

    /**
     * Flags triggering the display of certain flight pane items
     */
    private int showFixes = SHOW_ALL, showFlights = SHOW_ALL,
	showRoutes = SHOW_ALL, showTrajectories = SHOW_ALL;
    
    /**
     * The selected flights
     */
    private Collection selectedFlights = new Vector();
// end flight parameters
    
	public Client_Console(ClientEngine client){
		this.client = client;
		
		this.parser = new CommandParser(client.parameters,client.window);

		// build console window
		buildWindow();
		
		//Window closing commands
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (checkExit())
					System.exit(0);
			}
		});

	}
	
	private boolean checkExit(){
		if (client.getExit()){
			return true;
		} else {
			client.setExit();
			return false;
		}
	}
	/**
	 * Starts parsing the feed and displays the window
	 * 
	 * @overrides Window.show()
	 */
	public void show() {
		this.setBounds(0,400, 700, 370);
		super.show();
	}
	
	private void buildWindow(){
		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel consoleWindow = new JPanel(gridbag);
		
		output = new JTextPane();
		
		outDoc = output.getStyledDocument();
	    Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
	    Style regular = outDoc.addStyle("regular", def);
	    Style s = outDoc.addStyle("red", regular);
	    StyleConstants.setForeground(s, Color.RED);
	    s = outDoc.addStyle("bold", regular);
	    StyleConstants.setBold(s, true);
	    StyleConstants.setFontSize(s, 16);
	    s = outDoc.addStyle("boldred", regular);
	    StyleConstants.setBold(s, true);
	    StyleConstants.setFontSize(s, 16);
	    StyleConstants.setForeground(s, Color.RED);
		output.setEditable(false);
		output.setBackground(Color.BLACK);
		output.setForeground(Color.WHITE);
		outPane = new JScrollPane(output);
		// grid setup for TextPane
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		gridbag.setConstraints(outPane, c);
		consoleWindow.add(outPane);

		// setup InputBox
		input = new JTextField();
		input.setActionCommand("Box");
		input.addActionListener(this);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weighty = 0;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 1;
		gridbag.setConstraints(input, c);
		consoleWindow.add(input);
		input.setText("Enter Command Here");
		
		// setup Enter Button
		enter = new JButton("Enter");
		enter.setActionCommand("Enter");
		enter.addActionListener(this);
		enter.setToolTipText("Enters your input as a command");
		c.gridx = 1;
		c.weightx = .1;
		c.gridwidth = 1;
		gridbag.setConstraints(enter, c);
		consoleWindow.add(enter);
		
		super.setContentPane(consoleWindow);
	}
	
	
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Enter") || e.getActionCommand().equals("Box")){
			String command = input.getText();
			try {
				parser.parseAndExecute(input.getText());
				// if command accepted, clears input box
					// if settings changes, confirm changes
				outDoc.insertString(outDoc.getLength(), ("\nCommand: \""+input.getText()+"\" accepted\n"), outDoc.getStyle("regular"));
				input.setText("");
			} catch (InvalidCommandException e1) {
				output.setText(output.getText()+"\nError: Invalid Command\n\n");
			} catch (BadLocationException e2) {	}
		}
	}
	
	public void updateWindow(ComputationResults results) {

		// Update the console window
			// update settings
	    /* code to update settings goes here */
		showFlights = client.window.flightMap.getShowFlights();
		showRoutes = client.window.flightMap.getShowRoutes();
		showTrajectories = client.window.flightMap.getShowTrajectories();
		showFixes = client.window.flightMap.getShowFixes();
		selectedFlights = client.window.flightMap.selectedFlights;
			
			// update flights
		flights = results.getFlights();
		blunders = results.getBlunders();
		flight2TrajMap = results.getFlight2TrajectoryMap();
		try {
			// print flight data
			printConsole();
		} catch (BadLocationException e) {	}
	}

	private String printTrack(FlightTrack track){
		String ft = new String();
		String dir = new String();
		double lat , lon, alt, speed, heading;
		lat = track.getLatitude();
		lon = track.getLongitude();
		alt = track.getAltitude();
		speed = track.getSpeed();
		DecimalFormat twoPlaces = new DecimalFormat("0.00");
		
		// lattitude
		if (lat > 0){
			dir = "N";
		} else {
			dir = "S";
			lat = lat * -1;
		}
		ft += ((int)lat+"� "+(int)((lat*100)%100)+"' "+dir);
		ft += "\t";
		
		// longitude
		if (lon > 0){
			dir = "E";
		} else {
			dir = "W";
			lon = lon * -1;
		}
		ft += ((int)lon+"� "+(int)((lon*100)%100)+"' "+dir);
		ft += "\t";
		
		// altitude
		ft += (twoPlaces.format(alt)+" m");
		ft += "\t";
		
		
		// speed is in knots / ms
		speed *= 1000;
		double kmh = speed*1.852;
    	ft += (twoPlaces.format(kmh)+" km/h | "+twoPlaces.format(speed)+" knots");
    	ft += "\t";
    	
    	// heading
    	heading = (int)(track.getHeading()*100);
    	ft += (twoPlaces.format(heading)+"�\t");
		return ft;
	}
	
	private String printPlan(FlightPlan plan){
		String fp = new String();
		double alt, speed;
		Route route = plan.getRoute();
		alt = plan.getAssignedAltitude();
		speed = plan.getAssignedSpeed();
		DecimalFormat twoPlaces = new DecimalFormat("0.00");

		// alt
		fp += (twoPlaces.format(alt)+ " m");
		fp += "\t";
		
		// speed is in knots / ms
		speed *= 1000;
		double kmh = speed*1.852;
    	fp += (twoPlaces.format(kmh)+" km/h | "+twoPlaces.format(speed)+" knots");
    	fp += "\n";
		
		// route
    	fp += "\t\t";
    	Iterator i = route.fixIterator();
    	Fix fix;
    	while(i.hasNext()){
    		fix = (Fix) i.next();
    		fp += (fix.getId()+" ");
    	}
    	
		return fp;
	}
	
	private String printTraj(Trajectory traj){
		String tj = new String();
		Point4D point;
		Iterator i = traj.pointIterator();
		
		while(i.hasNext()){
			point = (Point4D) i.next();
			double lat = point.getLatitude();
			double lon = point.getLongitude();
			String dir = new String();
			DecimalFormat twoPlaces = new DecimalFormat("0.00");
			// lattitude
			if (lat > 0){
				dir = "N";
			} else {
				dir = "S";
				lat = lat * -1;
			}
			tj += ((int)lat+"� "+(int)((lat*100)%100)+"' "+dir);
			tj += "  ";
			
			// longitude
			if (lon > 0){
				dir = "E";
			} else {
				dir = "W";
				lon = lon * -1;
			}
			tj += ((int)lon+"� "+(int)((lon*100)%100)+"' "+dir);
			tj += " ";
			tj += "\t";

		}
		return tj;
	}
	
	private void printConsole() throws BadLocationException{
		// stole this from FlightMaps.java - must convert this so it prints to textPane
		
		/**
		 * Print Settings
		 */
		String settings = new String();
		String l2 = new String();
		settings += "\nConsole Output Settings:\nFixes:\t";
		if (showFixes == SHOW_ALL)
			l2 += "SHOW_ALL";
		else
			l2 += "SHOW_NONE";
		settings += "\tFlights:\t";
		l2 += "\t\t";
		if (showFlights == SHOW_ALL)
			l2 += "SHOW_ALL";
		else if (showFlights == SHOW_SELECTED)
			l2 += "SHOW_SELECTED";
		else if (showFlights == SHOW_WITH_PLAN)
			l2 += "SHOW_WITH_PLAN";
		else if (showFlights == SHOW_CONFORMING)
			l2 += "SHOW_CONFORMING";
		else if (showFlights == SHOW_BLUNDERING)
			l2 += "SHOW_BLUNDERING";
		else
			l2 += "SHOW_NONE";
		settings += "\tRoutes:\t";
		l2 += "\t\t";
		if (showRoutes == SHOW_ALL)
			l2 += "SHOW_ALL";
		else if (showRoutes == SHOW_SELECTED)
			l2 += "SHOW_SELECTED";
		else if (showRoutes == SHOW_WITH_PLAN)
			l2 += "SHOW_WITH_PLAN";
		else if (showRoutes == SHOW_CONFORMING)
			l2 += "SHOW_CONFORMING";
		else if (showRoutes == SHOW_BLUNDERING)
			l2 += "SHOW_BLUNDERING";
		else
			l2 += "SHOW_NONE";
		settings += "\tTrajectories:\t";
		l2 += "\t\t";
		if (showTrajectories == SHOW_ALL)
			l2 += "SHOW_ALL";
		else if (showTrajectories == SHOW_SELECTED)
			l2 += "SHOW_SELECTED";
		else if (showTrajectories == SHOW_WITH_PLAN)
			l2 += "SHOW_WITH_PLAN";
		else if (showTrajectories == SHOW_CONFORMING)
			l2 += "SHOW_CONFORMING";
		else if (showTrajectories == SHOW_BLUNDERING)
			l2 += "SHOW_BLUNDERING";
		else
			l2 += "SHOW_NONE";
		settings += "\n";
		l2 += "\n\n";
		outDoc.insertString(outDoc.getLength(), (settings+l2), outDoc.getStyle("regular"));
		
		/**
         * Draw Flights
         */
        Iterator flightIter = flights.iterator();
        while(flightIter.hasNext()) {
            
            Flight flight = (Flight)flightIter.next();
            boolean hasFlightPlan = flight.getFlightPlan() != null;
            boolean isBlundering = hasFlightPlan && blunders.contains(flight);
            boolean isConforming = hasFlightPlan && !isBlundering;
            boolean isSelected = selectedFlights.contains(flight);

            // Draw the flight if . . .
            if ((showFlights == SHOW_ALL) ||
                (showFlights == SHOW_WITH_PLAN  && hasFlightPlan) ||
                (showFlights == SHOW_CONFORMING && isConforming) ||
                (showFlights == SHOW_BLUNDERING && isBlundering) ||
                (showFlights == SHOW_SELECTED   && isSelected)) {
                
                /** Set the color of the flight and draw it */
            	String flightInfo = new String();
    	    	outDoc.insertString(outDoc.getLength(), "Flight: ", outDoc.getStyle("bold"));

    	    	if (isBlundering){
    	    		outDoc.insertString(outDoc.getLength(), (flight.getAircraftId()+"\t"), outDoc.getStyle("boldred"));
    	    	} else{
    	    		outDoc.insertString(outDoc.getLength(), (flight.getAircraftId()+"\t"), outDoc.getStyle("bold"));
    	    	}
    	    	outDoc.insertString(outDoc.getLength(), (printTrack(flight.getFlightTrack())+"\n"), outDoc.getStyle("regular"));
    
                // Draw the route if . . .
                if (hasFlightPlan &&
                    ((showRoutes == SHOW_ALL) ||
                     (showRoutes == SHOW_CONFORMING && isConforming) ||
                     (showRoutes == SHOW_BLUNDERING && isBlundering) ||
                     (showRoutes == SHOW_SELECTED   && isSelected))) {
                	
                    /** Set the color of the route and draw it */
                	String flightPlan = new String();
                	flightPlan += "Flight plan:\t\t";
                	flightPlan += printPlan(flight.getFlightPlan());
                	flightPlan += "\n";
                	
                	outDoc.insertString(outDoc.getLength(), flightPlan, outDoc.getStyle("regular"));	
                }

                // Draw the trajectory if . . .
                if ((showTrajectories == SHOW_ALL) ||
                    (showTrajectories == SHOW_WITH_PLAN  && hasFlightPlan) ||
                    (showTrajectories == SHOW_CONFORMING && isConforming)  ||
                    (showTrajectories == SHOW_BLUNDERING && isBlundering)  ||
                    (showTrajectories == SHOW_SELECTED   && isSelected)) {
                    
                	/** Set the color of the trajectory and draw it */
                	String traj = new String();
                	traj += "Trajectories:\t\t";
                	traj += printTraj((Trajectory)flight2TrajMap.get(flight));
                	traj += "\n";
                	
                	outDoc.insertString(outDoc.getLength(), traj, outDoc.getStyle("regular"));	
                }
                if (isBlundering){
                	outDoc.insertString(outDoc.getLength(), "Conformance Status\t", outDoc.getStyle("regular"));	

                	outDoc.insertString(outDoc.getLength(), "blundering\n", outDoc.getStyle("red"));
                }
                outDoc.insertString(outDoc.getLength(), "\n", outDoc.getStyle("regular"));
            }	// if draw flights
        }	// flight iterator
	}	// print console
	
	public void startWindow() {
		this.pack();
		this.show();
	}
}
