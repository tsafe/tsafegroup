/*
 TSAFE Prototype: A decision support tool for air traffic controllers
 Copyright (C) 2003  Gregory D. Dennis

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package tsafe.client.graphical_client;

import java.util.Collection;

import tsafe.client.ClientInterface;
import tsafe.common_datastructures.LatLonBounds;
import tsafe.common_datastructures.TSAFEProperties;
import tsafe.common_datastructures.client_server_communication.ComputationResults;
import tsafe.common_datastructures.client_server_communication.UserParameters;
import tsafe.server.ServerInterface;

/**
 * Main executable class
 */
public class GraphicalClient extends ClientInterface {

	/**
	 * The graphical main user interface
	 */
	private GraphicalWindow window;
	private CommandWindow cmdWindow;	//added 12/07 - Jane Lin

//	private static int closedWindowCounter = 0;	//added 12/08 - Jane Lin
	
	static private boolean ran = false;

	/**
	 * Construct a GraphicalClient
	 */
	public GraphicalClient(ServerInterface server) {
		super(server);
		bounds = TSAFEProperties.getLatLonBounds();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		this.window = new GraphicalWindow(this);
		this.window.startWindow();
		
		//added 12/07 - Jane Lin
		this.cmdWindow = new CommandWindow(this);
		this.cmdWindow.startWindow();

		ran = true;	//added 12/08 - Jane Lin
System.out.println("GRAPHICALCLIENT::run");
	}

	//Added 12/07 - Jane Lin to get selected flights from flightMap
	public Collection getSelectedFlights() {
		//this.window.getSelectedFlights
		return this.window.getSelectedFlights();
	}

	/**
	 * Queries the server for new flight data.
	 * 
	 * @see tsafe.client.ClientInterface#getFlightData()
	 */
	public void updateClient(ComputationResults results) {
		this.window.updateWindow(results);
		this.cmdWindow.updateWindow(results);	//added 12/07 - Jane Lin
	}
	
	//Added 12/07 - Jane Lin to update show settings 
	public void updateShowFixesG2C(int showOption) {
		if (ran)
			this.cmdWindow.setShowFixes(showOption, true);
	}

	//Added 12/07 - Jane Lin to update show settings 
	public void updateShowFlightsG2C(int showOption) {
		if (ran)
			this.cmdWindow.setShowFlights(showOption, true);
	}

	//Added 12/07 - Jane Lin to update show settings 
	public void updateShowRoutesG2C(int showOption) {
		if (ran)
			this.cmdWindow.setShowRoutes(showOption, true);
	}

	//Added 12/07 - Jane Lin to update show settings 
	public void updateShowTrajectoriesG2C(int showOption) {
		if (ran)
			this.cmdWindow.setShowTrajectories(showOption, true);
	}
	
	//Added 12/07 - Jane Lin to update show settings 
	public void updateShowFixesC2G(int showOption) {
		this.window.setShowFixes(showOption);
	}

	//Added 12/07 - Jane Lin to update show settings 
	public void updateShowFlightsC2G(int showOption) {
		this.window.setShowFlights(showOption);
	}

	//Added 12/07 - Jane Lin to update show settings 
	public void updateShowRoutesC2G(int showOption) {
		this.window.setShowRoutes(showOption);
	}

	//Added 12/07 - Jane Lin to update show settings 
	public void updateShowTrajectoriesC2G(int showOption) {
		this.window.setShowTrajectories(showOption);
	}
	
	/**
	 * @return Returns the parameters.
	 */
	public UserParameters getParameters() {
		return parameters;
	}

	/**
	 * Interface method for the client for reading the fixes.
	 * 
	 * @return All fixes, stored in the database.
	 */
	public Collection getFixes() {
		return this.server.getFixes();
	}

	/**
	 * @return Returns the server.
	 */
	public ServerInterface getServer() {
		return server;
	}

	/**
	 * @param server
	 *            The server to set.
	 */
	public void setServer(ServerInterface server) {
		this.server = server;
	}

	/**
	 * @return Returns the bounds.
	 */
	public LatLonBounds getBounds() {
		return bounds;
	}

	public void testing() {
		ClientInterface client = new GraphicalClient(this.server);
		client.setBounds(bounds);
		client.run();
	}

	//Added 12/08 - Jane Lin
	public FlightList getRealFlightList() {
		this.window.updateNeeded();
		return window.getFlightList();
	}
	
	//ADDED 12/08 - Jane Lin
	public Collection getSelectedFlightsFromCMDWin() {
		return cmdWindow.getSelectedFlightsChanged();
	}
}