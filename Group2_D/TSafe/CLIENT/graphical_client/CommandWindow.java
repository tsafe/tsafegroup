/*
 * Created on Nov 29, 2004
 */
package tsafe.client.graphical_client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;	//added 12/08 - Jane Lin
import java.awt.event.WindowEvent;	//added 12/08 - Jane Lin
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import java.util.Collection;		//added 12/07 - Jane Lin
import java.util.Iterator;			//added 12/07 - Jane Lin

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.JScrollBar;		//added 12/08 - Jane Lin
import javax.swing.JScrollPane;		//added 12/08 - Jane Lin

import tsafe.common_datastructures.Flight;
import tsafe.common_datastructures.FlightPlan;
import tsafe.common_datastructures.FlightTrack;

import tsafe.common_datastructures.client_server_communication.ComputationResults;
import tsafe.common_datastructures.client_server_communication.UserParameters;
import tsafe.common_datastructures.TSAFEProperties;

/**
 * @author Anthony C. Wong
 *
 * CommandWindow class
 */
public class CommandWindow extends JFrame implements ActionListener, KeyListener {
	
	private InputParser parser;
	
	private GraphicalClient client;
	private JTextPane console;
	private JScrollBar bar;		//Added 12/08 - Jane Lin
	private JLabel lbl;
	private JTextField cmd;
	private JButton enter;
	private StyledDocument doc;
	
	private Style styleDefault;
	private Style styleFlightID;
	private Style styleFlightIDRed;
	private Style styleConfStatusRed;
	private Style styleYellow;
	private Style styleCyan;
	
	private DecimalFormat dfMeters;
	private DecimalFormat dfLatLon;
	private DecimalFormatSymbols dfLatLonSyms;

	private static final String TXT_BUTTON_ENTER = "Enter";
	
	private static final double KNOTS_COEFF = 0.539956803;
	private static final double SPD_TO_KMH = 6371000.0*Math.PI/180/60;

	//Added 12/07 - Jane Lin
	/**
	 * List of flights to select from
	 */
	private Collection flightList;
	private Collection flightBlunders;
	private Collection flightSelected;
	private FlightList allFlights;

	//Added 12/07 - Jane Lin
	/**
     * Show options
     */
    public static final int SHOW_ALL        = 0;
    public static final int SHOW_SELECTED   = 1;
    public static final int SHOW_WITH_PLAN  = 2;
    public static final int SHOW_CONFORMING = 3;
    public static final int SHOW_BLUNDERING = 4;
    public static final int SHOW_NONE       = 5;
	private int showFixes  = SHOW_ALL, showFlights      = SHOW_ALL,
                showRoutes = SHOW_ALL, showTrajectories = SHOW_ALL;
	
	public CommandWindow(GraphicalClient client) {
		this.client = client;
		
		this.parser = new InputParser(this, client.getParameters());
		
		//this.bar = new JScrollBar();
		this.console = new JTextPane();
		//this.console.setAutoscrolls(true);	//Added 12/08 - Jane Lin
		this.console.setBackground(Color.BLACK);
		this.console.setSize(new Dimension(600,300));
		this.console.setEditable(false);
		this.doc = this.console.getStyledDocument();
		initStyles();
		
		/* Initialize DecimalFormat instances */
		this.dfMeters = new DecimalFormat("###,##0.##");
		this.dfLatLonSyms = new DecimalFormatSymbols();
		this.dfLatLonSyms.setDecimalSeparator('�');
		this.dfLatLon = new DecimalFormat("##0.##");
		this.dfLatLon.setDecimalFormatSymbols(dfLatLonSyms);		
		
		this.lbl = new JLabel(">>");
		this.cmd = new JTextField();
		this.cmd.addKeyListener(this);
		this.enter = new JButton(TXT_BUTTON_ENTER);
		this.enter.addActionListener(this);
		
		JPanel bottom = new JPanel();
		bottom.setLayout(new BoxLayout(bottom, BoxLayout.LINE_AXIS));
		bottom.add(lbl);
		bottom.add(cmd);
		bottom.add(enter);
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
										new JScrollPane(console),bottom);
		splitPane.setDividerLocation(370);
		splitPane.setPreferredSize(new Dimension(800,400));
		splitPane.setEnabled(false);
		splitPane.setDividerSize(1);
		
		super.setContentPane(splitPane);
		super.setLocation(0,300);
		//this.add(bottom);
		
		//Quit everything when the window is closed

		//added 12/08 - Jane Lin
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				TSAFEProperties.closedWinCounter++;
				if (TSAFEProperties.closedWinCounter == 2)
					System.exit(0);
			}
		});
	}
	
	public void initStyles() {
		// Default style
		this.styleDefault = doc.addStyle("default", null);
		this.styleDefault =  StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
		StyleConstants.setForeground(styleDefault, Color.WHITE);
		
		this.styleFlightID = doc.addStyle("flightid", styleDefault);
		StyleConstants.setFontSize(styleFlightID, 16);
		StyleConstants.setBold(styleFlightID, true);
		
		this.styleFlightIDRed = doc.addStyle("flightidred", styleFlightID);
		StyleConstants.setForeground(styleFlightIDRed, Color.RED);
		
		this.styleConfStatusRed = doc.addStyle("confstatusred", styleDefault);
		StyleConstants.setForeground(styleConfStatusRed, Color.RED);
		
		this.styleYellow = doc.addStyle("yellow", styleDefault);
		StyleConstants.setForeground(styleYellow, Color.ORANGE);
		StyleConstants.setBold(styleYellow, true);
		
		this.styleCyan = doc.addStyle("cyan", styleDefault);
		StyleConstants.setForeground(styleCyan, Color.CYAN);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(TXT_BUTTON_ENTER)) {
		//	this.updateWindow(this.cmd.getText());
			System.out.println("Command: " + this.cmd.getText());
			try {
System.out.println("CMDWIN::actionPerformed");
				this.parser.parseCommand(this.cmd.getText());
			} catch(Exception ex) {
				printMessage(ex.getMessage());
			}
			this.cmd.setText("");
		}
	}
	
	public void keyPressed(KeyEvent e) {
		
	}
	
	public void keyTyped(KeyEvent e) {
		
	}
	
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			System.out.println("CMDWINDOW: Enter pressed");
			try {
System.out.println("CMDWIN::keyReleased");
				this.parser.parseCommand(this.cmd.getText());
			} catch(Exception ex) {
				printMessage(ex.getMessage());
			}
			this.cmd.setText("");
		}
	}

	//Jane Lin
	public void updateWindow(ComputationResults results) {
		/*
		((FlightList) flightList).setFlights(results.getFlights());
		((FlightList) flightBlunders).setFlights(results.getBlunders());
		((FlightList) flightSelected).(((FlightList) flightList).getSelectedFlights());
		*/
		flightList = results.getFlights();
		flightBlunders = results.getBlunders();
		flightSelected = client.getSelectedFlights();
				
		Iterator flightIter = flightList.iterator();

		printSettings();

		while (flightIter.hasNext()) {
			Flight flight = (Flight) flightIter.next();
	
			boolean hasFlightPlan = flight.getFlightPlan() != null;
            boolean isBlundering = hasFlightPlan && flightBlunders.contains(flight);
            boolean isConforming = hasFlightPlan && !isBlundering;
            boolean isSelected = flightSelected.contains(flight);

			// Print the flight if . . .
            if ((showFlights == SHOW_ALL) ||
                (showFlights == SHOW_WITH_PLAN  && hasFlightPlan) ||
                (showFlights == SHOW_CONFORMING && isConforming) ||
                (showFlights == SHOW_BLUNDERING && isBlundering) ||
                (showFlights == SHOW_SELECTED   && isSelected)) {
                
                printFlight(flight, isBlundering);
    
                // Print the route if . . .
                if (hasFlightPlan &&
                    ((showRoutes == SHOW_ALL) ||
                     (showRoutes == SHOW_CONFORMING && isConforming) ||
                     (showRoutes == SHOW_BLUNDERING && isBlundering) ||
                     (showRoutes == SHOW_SELECTED   && isSelected))) {
                	//printRoute(flight);
                	printFlightPlan (flight);
                    //drawRoute(g, flight.getFlightPlan().getRoute());
                }

                // Draw the trajectory if . . .
                if ((showTrajectories == SHOW_ALL) ||
                    (showTrajectories == SHOW_WITH_PLAN  && hasFlightPlan) ||
                    (showTrajectories == SHOW_CONFORMING && isConforming)  ||
                    (showTrajectories == SHOW_BLUNDERING && isBlundering)  ||
                    (showTrajectories == SHOW_SELECTED   && isSelected)) {
                    //drawTrajectory(g, (Trajectory)flight2TrajMap.get(flight));
                    printTrajectory(flight);
                }
                
               if ((showFlights == SHOW_ALL) ||
                (showFlights == SHOW_WITH_PLAN  && hasFlightPlan) ||
                (showFlights == SHOW_CONFORMING && isConforming) ||
                (showFlights == SHOW_BLUNDERING && isBlundering) ||
                (showFlights == SHOW_SELECTED   && isSelected))
                	if (isBlundering) printStatus(flight);
                	
                printMessage("\n");
				//Added and Commented bottom party 12/08 - Jane Lin
//				this.console.setCaretPosition(this.console.getDocument().getLength());
			}
		}
	}
	
	private void printSettings() {
		try {
			doc.insertString(doc.getLength(), "Selected Flights: ", styleCyan);
			Iterator iter = flightSelected.iterator();
			while(iter.hasNext()) {
				doc.insertString(doc.getLength(), ((Flight) iter.next()).getAircraftId(), styleCyan);
				doc.insertString(doc.getLength(), " ", styleCyan);
			}
			doc.insertString(doc.getLength(), "\n\n", styleCyan);

			doc.insertString(doc.getLength(), "Show Fixes:\t\t", styleCyan);
			//Added 12/08 - Jane Lin
			switch(showFixes) {
				case SHOW_ALL: doc.insertString(doc.getLength(), "all\n", styleCyan); break;
				case SHOW_NONE:  doc.insertString(doc.getLength(), "none\n", styleCyan);  break;
			}

			doc.insertString(doc.getLength(), "Show Flights:\t\t", styleCyan);			
			//Added 12/08 - Jane Lin
			switch (showFlights) {
				case SHOW_ALL: doc.insertString(doc.getLength(), "all\n", styleCyan); break;
				case SHOW_SELECTED:  doc.insertString(doc.getLength(), "selected\n", styleCyan); break;
				case SHOW_WITH_PLAN: doc.insertString(doc.getLength(), "with plan\n", styleCyan);  break;
				case SHOW_CONFORMING:  doc.insertString(doc.getLength(), "conforming\n", styleCyan);  break;
				case SHOW_BLUNDERING:  doc.insertString(doc.getLength(), "blundering\n", styleCyan); break;
				case SHOW_NONE:  doc.insertString(doc.getLength(), "none\n", styleCyan);  break;
			}           	

			doc.insertString(doc.getLength(), "Show Routes:\t\t", styleCyan);			
			//Added 12/08 - Jane Lin
			switch (showRoutes) {
				case SHOW_ALL: doc.insertString(doc.getLength(), "all\n", styleCyan); break;
				case SHOW_SELECTED:  doc.insertString(doc.getLength(), "selected\n", styleCyan); break;
				case SHOW_CONFORMING:  doc.insertString(doc.getLength(), "conforming\n", styleCyan);  break;
				case SHOW_BLUNDERING:  doc.insertString(doc.getLength(), "blundering\n", styleCyan); break;
				case SHOW_NONE:  doc.insertString(doc.getLength(), "none\n", styleCyan);  break;
			}           	

			doc.insertString(doc.getLength(), "Show Trajectories:\t", styleCyan);
			//Added 12/08 - Jane Lin
			switch (showTrajectories) {
				case SHOW_ALL: doc.insertString(doc.getLength(), "all\n", styleCyan); break;
				case SHOW_SELECTED:  doc.insertString(doc.getLength(), "selected\n", styleCyan); break;
				case SHOW_WITH_PLAN: doc.insertString(doc.getLength(), "with plan\n", styleCyan);  break;
				case SHOW_CONFORMING:  doc.insertString(doc.getLength(), "conforming\n", styleCyan);  break;
				case SHOW_BLUNDERING:  doc.insertString(doc.getLength(), "blundering\n", styleCyan); break;
				case SHOW_NONE:  doc.insertString(doc.getLength(), "none\n", styleCyan);  break;
			}
			
			doc.insertString(doc.getLength(), "\n", styleCyan);
			
			doc.insertString(doc.getLength(), "Parameter Lateral Threshold:\t\t", styleCyan);
			doc.insertString(doc.getLength(), Double.toString(client.getParameters().cmLateralThreshold), styleCyan);
			if (client.getParameters().cmLateralWeightOn) {
				doc.insertString(doc.getLength(), " (enabled)\n", styleCyan);
			}
			else {
				doc.insertString(doc.getLength(), " (disabled)\n", styleCyan);
			}
			
			doc.insertString(doc.getLength(), "Parameter Vertical Threshold:\t\t", styleCyan);
			doc.insertString(doc.getLength(), Double.toString(client.getParameters().cmVerticalThreshold), styleCyan);
			if (client.getParameters().cmVerticalWeightOn) {
				doc.insertString(doc.getLength(), " (enabled)\n", styleCyan);
			}
			else {
				doc.insertString(doc.getLength(), " (disabled)\n", styleCyan);
			}
			
			doc.insertString(doc.getLength(), "Parameter Angular Threshold:\t\t", styleCyan);
			doc.insertString(doc.getLength(), Double.toString(client.getParameters().cmAngularThreshold), styleCyan);
			if (client.getParameters().cmAngularWeightOn) {
				doc.insertString(doc.getLength(), " (enabled)\n", styleCyan);
			}
			else {
				doc.insertString(doc.getLength(), " (disabled)\n", styleCyan);
			}

			doc.insertString(doc.getLength(), "Parameter Speed Threshold:\t\t", styleCyan);
			doc.insertString(doc.getLength(), Double.toString(client.getParameters().cmSpeedThreshold), styleCyan);
			if (client.getParameters().cmSpeedWeightOn) {
				doc.insertString(doc.getLength(), " (enabled)\n", styleCyan);
			}
			else {
				doc.insertString(doc.getLength(), " (disabled)\n", styleCyan);
			}
			
						
			doc.insertString(doc.getLength(), "Parameter Residual Threshold:\t", styleCyan);
			doc.insertString(doc.getLength(), Double.toString(client.getParameters().cmResidualThreshold), styleCyan);
			doc.insertString(doc.getLength(), "\n", styleCyan);
			
			doc.insertString(doc.getLength(), "Parameter Time Horizon:\t\t", styleCyan);
			doc.insertString(doc.getLength(), Double.toString(client.getParameters().tsTimeHorizon), styleCyan);
			doc.insertString(doc.getLength(), "\n\n", styleCyan);
			
		} catch(BadLocationException ex) {
			
		}
	}
	
	private void printFlight(Flight f, boolean isBlundering) {
		try {
			/*
			 * LINE 1: Aircraft ID, Lat, Lon, Height, Speed, Heading
			 */
			FlightTrack ft = f.getFlightTrack();
			
			/* Print Aircraft ID */
			doc.insertString(doc.getLength(), "Flight: ", styleFlightID);
			if (isBlundering)
				doc.insertString(doc.getLength(), f.getAircraftId(), styleFlightIDRed);
			else
				doc.insertString(doc.getLength(), f.getAircraftId(), styleFlightID);
			doc.insertString(doc.getLength(), "\t", styleDefault);
			
			/* Print Latitude */
//			doc.insertString(doc.getLength(), "00Â° 00' N", styleDefault);
			doc.insertString(doc.getLength(), dfLatLon.format(ft.getLatitude()), styleDefault);
			doc.insertString(doc.getLength(), "' N\t", styleDefault);
			
			/* Print Longitude */
			//doc.insertString(doc.getLength(), "00Â° 00' W", styleDefault);
			doc.insertString(doc.getLength(), dfLatLon.format(Math.abs(ft.getLongitude())), styleDefault);
			doc.insertString(doc.getLength(), "' W\t", styleDefault);
			
			/* Print Altitude */
			//doc.insertString(doc.getLength(), "00,000.00 m", styleDefault);
			doc.insertString(doc.getLength(), dfMeters.format(ft.getAltitude()), styleDefault);
			doc.insertString(doc.getLength(), " m\t", styleDefault);
			
			/* Print Speed */
			//doc.insertString(doc.getLength(), "000.00 km/h | 000.00 knots", styleDefault);
			doc.insertString(doc.getLength(), dfMeters.format(ft.getSpeed()*SPD_TO_KMH), styleDefault);
			doc.insertString(doc.getLength(), " km/h | ", styleDefault);
			doc.insertString(doc.getLength(), dfMeters.format(ft.getSpeed()*SPD_TO_KMH*KNOTS_COEFF), styleDefault);
			doc.insertString(doc.getLength(), " knots\t", styleDefault);
			
			/* Print Heading */
			//doc.insertString(doc.getLength(), "00.00Â°", styleDefault);
			doc.insertString(doc.getLength(), dfMeters.format(ft.getHeading()*180/Math.PI), styleDefault);
			doc.insertString(doc.getLength(), "�\t\n", styleDefault);
		} catch(BadLocationException ble) {
			System.err.println("Unable to update window with results");
		}
	}
	
	private void printFlightPlan(Flight f) {
		FlightPlan fp = f.getFlightPlan();
		
		try {
			/*
			 * LINE 2: Flight Plan (height, speed)
			 */
			doc.insertString(doc.getLength(), "Flight Plan:\t\t", styleDefault);
			
			/* Print Assigned Altitude */
			//doc.insertString(doc.getLength(), "00,000.00 m", styleDefault);
			doc.insertString(doc.getLength(), dfMeters.format(fp.getAssignedAltitude()), styleDefault);
			doc.insertString(doc.getLength(), " m\t", styleDefault);
			
			/* Print Assigned Speed */
			//doc.insertString(doc.getLength(), "000.00 km/h | 000.00 knots", styleDefault);
			doc.insertString(doc.getLength(), dfMeters.format(fp.getAssignedSpeed()*SPD_TO_KMH), styleDefault);
			doc.insertString(doc.getLength(), " km/h | ", styleDefault);
			doc.insertString(doc.getLength(), dfMeters.format(fp.getAssignedSpeed()*SPD_TO_KMH*KNOTS_COEFF), styleDefault);
			doc.insertString(doc.getLength(), " knots\t", styleDefault);
			
			doc.insertString(doc.getLength(), "\n", styleDefault);
			
			/*
			 * LINE 3: Ummmm... what is this?
			 */
			doc.insertString(doc.getLength(), "\t\t", styleDefault);
			doc.insertString(doc.getLength(), "AAAA BBB CCCC DDD EEE FFFF GGGG 111", styleDefault);
			doc.insertString(doc.getLength(), "\n", styleDefault);

		} catch(BadLocationException ble) {
			System.err.println("Unable to update window with results");
		}
	}
	
	private void printTrajectory(Flight f) {
		try {
			/*
			 * LINE 4: Trajectories...
			 */
			doc.insertString(doc.getLength(), "Trajectories:\t", styleDefault);
			doc.insertString(doc.getLength(), "00�00'N", styleDefault);
			doc.insertString(doc.getLength(), "  ", styleDefault);
			doc.insertString(doc.getLength(), "00�00'W", styleDefault);
			doc.insertString(doc.getLength(), "\t", styleDefault);
			doc.insertString(doc.getLength(), "00�00'N", styleDefault);
			doc.insertString(doc.getLength(), "  ", styleDefault);
			doc.insertString(doc.getLength(), "00�00'W", styleDefault);
			doc.insertString(doc.getLength(), "\n", styleDefault);
		} catch(BadLocationException ble) {
			System.err.println("Unable to update window with results");
		}
	}		
	
	private void printStatus(Flight f) {
		try {
			/*
			 * LINE 5: Conformance Status
			 */
			doc.insertString(doc.getLength(), "Conformance Status: \t", styleDefault);
			doc.insertString(doc.getLength(), "blundering", styleConfStatusRed);
			
			doc.insertString(doc.getLength(), "\n", styleDefault);
		} catch(BadLocationException ble) {
			System.err.println("Unable to update window with results");
		}
	}
	
	public void printMessage(String s) {
		try {
			doc.insertString(doc.getLength(), s, styleYellow);
			doc.insertString(doc.getLength(), "\n\n", styleDefault);
		} catch(BadLocationException ble) {
			System.err.println("Unable to update window with results");
		}
	}		

	//Added 12/08 - Jane Lin
	public Collection getSelectedFlights() {
//		client.getSelectedFlights().add(f);
		return client.getSelectedFlights();
	}
	
	//Added 12/08 - Jane Lin
	public Collection getFlightList() {
		return flightList;
	}

	//Added 12/08 - Jane Lin
	public FlightList getRealFlightList() {
		return client.getRealFlightList();
	}
	
	//Added 12/07 - Jane Lin
	public void setShowFixes(int showOption, boolean g2c) {
		this.showFixes = showOption;
		if (!g2c)
		client.updateShowFixesC2G(showOption);
	}
	
	//Added 12/07 - Jane Lin
	public void setShowFlights(int showOption, boolean g2c) {
		this.showFlights = showOption;
		if (!g2c)
		client.updateShowFlightsC2G(showOption);
	}
	
	//Added 12/07 - Jane Lin
	public void setShowRoutes(int showOption, boolean g2c) {
		this.showRoutes = showOption;
		if (!g2c)
		client.updateShowRoutesC2G(showOption);
	}
	
	//Added 12/07 - Jane Lin
	public void setShowTrajectories(int showOption, boolean g2c) {
		this.showTrajectories = showOption;
		if (!g2c)
		client.updateShowTrajectoriesC2G(showOption);
	}
	
	public void startWindow() {
		this.pack();
		this.show();
	}
	
	//Added 12/08 - Jane
	public Collection getSelectedFlightsChanged() {
		return flightSelected;
	}
}