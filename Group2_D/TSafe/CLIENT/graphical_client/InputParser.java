/*
 * Created on Nov 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author jcordova
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

package tsafe.client.graphical_client ;

import java.util.regex.PatternSyntaxException ;
import tsafe.client.graphical_client.CommandWindow ;
import java.util.Vector ;
import java.lang.NumberFormatException ;
import java.lang.Exception ;
import java.lang.Throwable ;
import tsafe.common_datastructures.Flight ;
import tsafe.client.graphical_client.FlightList;
import tsafe.common_datastructures.client_server_communication.UserParameters ;
import java.util.* ;

public class InputParser {
	
	private static CommandWindow cmdWindow ;
	private static UserParameters parameters ;
	
	//---------------------------------------------------------------------------------
	public InputParser(CommandWindow commandWinIn, UserParameters uParam)
	{
		this.cmdWindow = commandWinIn ;
		this.parameters = uParam ;
	}
	//--------------------------------------------------------------------------
	public static void parseCommand(String commandIn) throws Exception
	{
		
		//get rid of leading and trailing white space
		commandIn = commandIn.trim() ;
		//tokenize command 
		String[] tokens = commandIn.split("(\\s)+") ;	
		
		//determine which set of outputs will be parsed
		//whether it is (based on project description): 
		//SelectOutput, ChangeOutPutSettings, or ChangeParameters
		if(tokens.length >= 1)
		{
			if(tokens[0].matches("select"))
				parseSelect(tokens) ; 
			else if(tokens[0].matches("show"))
				parseOutputSettings(tokens) ;
			else if(tokens[0].matches("set") || tokens[0].matches("enable") || 
					tokens[0].matches("disable"))
				parseChangeParameters(tokens) ;
			else
			{
				String error = printCommand(tokens) ;			
				error += (":Invalid Command: no command with \"" + tokens[0] + "\"") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
		}
		//nothing was sent into the command line
		else
		{
			String error = printCommand(tokens) ;
			error += (":Invalid Command: Nothing entered") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
	}
	
	//---------------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------------
	//prints whats in "commandIn" because the command is invalid
	public static String printCommand(String[] commandIn)
	{
		String st = new String() ;
		for(int i = 0 ; i < commandIn.length ; i++) 
			st += (commandIn[i] += " ") ;
 		
		return st ;
	}
 	
	//---------------------------------------------------------------------------------
	
	//---------------------------------------------------------------------------------
	//if first argument of the command is "select" 
	//go here to parse rest to determine if it is a valid command line or a list of flights
	public static void parseSelect(String[] commandIn) throws Exception
	{
		Iterator iter ; //= (cmdWindow.getFlightList()).iterator() ;
		Vector flightIDs = new Vector() ;
		System.out.println("in select") ;
		//list of flights not in flight list 
		Vector noFlightIDs = new Vector() ; 
		
		//if only "select" is written on the command line
		if(commandIn.length == 1) 
		{
			Exception ex = new Exception("INVALID COMMAND: incompete command") ;
			throw ex ;
		
		}
		
		//if all flights are selected using "select *"
		String star = commandIn[1] ;
		if(    (star.charAt(0) == '*')   &&   (star.length() == 1)   )
		{
			//invalid command: other parameters follow the star in "select *"
			if(commandIn.length > 2) 
			{
				String error = printCommand(commandIn) ;
				error += (":INVALID COMMAND: Nothing should follow [select *]") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			
			}
			//valid commmand: only star follows in "select" so its "select *" ;
			else
			{
				System.out.println("Valid command");
				iter = (cmdWindow.getFlightList()).iterator();
				
				while(iter.hasNext())
					{
						Flight temp = (Flight)iter.next() ;
						try
						{
							flightIDs.add(temp.getAircraftId());		
						}
						catch(PatternSyntaxException ex)
						{
					
						}
					}
					
				(cmdWindow.getRealFlightList()).setSelectedFlights(flightIDs);

			}
		}
		
		//if flights are selected using a list of flights
		else
		{
			for(int a = 1 ; a < commandIn.length ; a++)
			{
				iter = (cmdWindow.getFlightList()).iterator();
				//flightIDs.add(commandIn[a]) ;
				boolean foundFlight = false ; 
				while(iter.hasNext())
					{
						Flight temp = (Flight)iter.next() ;
						try
						{
							if(temp.getAircraftId().matches(commandIn[a])) //check to see if flight exists
							{	
								flightIDs.add(commandIn[a]) ; //if exists add to list of selected flights
								foundFlight =  true ; //set found to true
							}							
						}
						catch(PatternSyntaxException ex)
						{
					
						}
					}
					
					if(!foundFlight)
						noFlightIDs.add(commandIn[a]) ;
					
					foundFlight = false ;
			}			

			(cmdWindow.getRealFlightList()).setSelectedFlights(flightIDs);
			
			//Display List of flights not found, if any
			
			if(cmdWindow.getSelectedFlights().size() == 0)
			{
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: none of the flights are found") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			if(noFlightIDs.size() > 0)
			{
				cmdWindow.printMessage("List of flights not found: ") ;
				for( int i = 0 ; i < noFlightIDs.size() ; i++)
					cmdWindow.printMessage( (String)noFlightIDs.get(i) + " ");
			}
		}//end else
	}
	
	//---------------------------------------------------------------------------------
	
	//---------------------------------------------------------------------------------
	//if first argument of the command is "show" 
	//go here to parse rest to determine if it is a valid command line
	//and then call the appropriate function
	public static void parseOutputSettings(String[] commandIn) throws Exception
	{
		System.out.println("in OutputSettings") ;
		
		//check if command is incomplete
		if(commandIn.length == 1)
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: need parameters") ;
			Exception ex = new Exception(error) ;
		}
		
		//check for second token: fixes, flights, routes, trajectories, or an invalid parameter
		else if(commandIn[1].matches("fixes"))
			parseFixes(commandIn);
		else if(commandIn[1].matches("flights")) 
			parseFlights(commandIn) ;
		else if(commandIn[1].matches("routes"))
			parseRoutes(commandIn);
		else if(commandIn[1].matches("trajectories"))
			parseTrajectories(commandIn);
		else	//invalid parameter
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: invalid parameter") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
	}
	
	//---------------------------------------------------------------------------------
	
	//---------------------------------------------------------------------------------
	//parses routes parameters to make sure they are correct
	public static void parseRoutes(String[] commandIn) throws Exception
	{
		//not enough parameters
		if(commandIn.length == 2)
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: not enough parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		//check for: "all", "selected", "conforming", "blundering", or "none"
		else if(commandIn[2].matches("all"))
		{	
			//check if more parameters than needed
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowRoutes(0, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 
		else if(commandIn[2].matches("selected"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowRoutes(1, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else if(commandIn[2].matches("conforming"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowRoutes(3, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 			
		else if(commandIn[2].matches("blundering"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowRoutes(4, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 			
		else if(commandIn[2].matches("none"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowRoutes(5, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: parameter does not match with \"show routes\"") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}

		

	}
	
	//---------------------------------------------------------------------------------
	
	//---------------------------------------------------------------------------------
	//parses trajectories parameters to make sure they are correct
	public static void parseTrajectories(String[] commandIn) throws Exception
	{
		//not enough parameters
		if(commandIn.length == 2)
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: not enough parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		//check for: "all", "selected", "withplans", "conforming", "blundering", or "none"
		else if(commandIn[2].matches("all"))
		{	
			//check if more parameters than needed
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowTrajectories(0, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 
		else if(commandIn[2].matches("selected"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowTrajectories(1, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else if(commandIn[2].matches("withplans"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowTrajectories(2, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else if(commandIn[2].matches("conforming"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowTrajectories(3, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 			
		else if(commandIn[2].matches("blundering"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowTrajectories(4, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 			
		else if(commandIn[2].matches("none"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				(printCommand(commandIn)).toString() ;
				cmdWindow.setShowTrajectories(5, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: parameter does not match with \"show trajectories\"") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
	
	
}	
	//---------------------------------------------------------------------------------
	//parses flights parameters to make sure they are correct
	public static void parseFlights(String[] commandIn) throws Exception
	{
		//not enough parameters
		if(commandIn.length == 2)
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: not enough parameters") ;
		}
		//check for: "all", "selected", "withplans", "conforming", "blundering", or "none"
		else if(commandIn[2].matches("all"))
		{	
			//check if more parameters than needed
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				printCommand(commandIn) ;
				cmdWindow.setShowFlights(0, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 
		else if(commandIn[2].matches("selected"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				printCommand(commandIn) ;
				cmdWindow.setShowFlights(1, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else if(commandIn[2].matches("withplans"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				printCommand(commandIn) ;
				cmdWindow.setShowFlights(2, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else if(commandIn[2].matches("conforming"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				printCommand(commandIn) ;
				cmdWindow.setShowFlights(3, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 			
		else if(commandIn[2].matches("blundering"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				printCommand(commandIn) ;
				cmdWindow.setShowFlights(4, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 			
		else if(commandIn[2].matches("none"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				printCommand(commandIn) ;
				cmdWindow.setShowFlights(5, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: parameter does not match with \"show flights\"") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
			
	}

	//---------------------------------------------------------------------------------
	
	//---------------------------------------------------------------------------------
	//parses fixes parameters to make sure they are correct
	public static void parseFixes(String[] commandIn) throws Exception
	{
		//not enough parameters
		if(commandIn.length == 2)
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: not enough parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		//check for: "all" or "none"
		else if(commandIn[2].matches("all"))
		{	
			//check if more parameters than needed
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				printCommand(commandIn) ;
				cmdWindow.setShowFixes(0, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 
		else if(commandIn[2].matches("none"))
		{	
			//checks if more parameters than needed 
			if(commandIn.length == 3)
			{
				System.out.print("valid command: ") ;
				printCommand(commandIn) ;
				cmdWindow.setShowFixes(5, false) ;
			}
			else
			{	
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: too many parameters") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
			
		} 	
		else
		{
			String error = printCommand(commandIn) ;
			error += (": Invalid Command: parameter does not match with \"show fixes\"") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		
	}
	
	
	//---------------------------------------------------------------------------------
	
	//---------------------------------------------------------------------------------
	//if first argument of the command is "set, enable or disable" 
	//go here to parse rest to determine if it is a valid command line 
	//and then call the appropriate function
	public static void parseChangeParameters(String[] commandIn) throws Exception
	{
		
		System.out.println("in change parameters") ;
		
		//commmand is incomplete
		if(commandIn.length == 1)
		{
			String error = printCommand(commandIn) ;
			error += (": INVALID COMMAND: not enough parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		//check if command is set, enable or disable
		else if(commandIn[0].matches("set"))
		{
			if(commandIn[1].matches("parameter"))
				parseSet(commandIn);
			else
			{
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: parameter doesn't match with \"set\" ") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
		}
		else if(commandIn[0].matches("enable"))
		{
			if(commandIn[1].matches("parameter"))
				parseEnable(commandIn);
			else
			{
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: parameter doesn't match with \"enable\" ") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
		}
		else if(commandIn[0].matches("disable"))
		{System.out.println("in disable") ;
			if(commandIn[1].matches("parameter"))
				parseDisable(commandIn);
			else
			{
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: parameter doesn't match with \"disable\" ") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
		} 
		else 
			;
		
	}
	
	//---------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------
	//checks the "set" parameters to make sure they are correct
	//assumes length of commandIn is at least 1 token
	public static void parseSet(String[] commandIn) throws Exception
	{

		
		//not enough parameters in command line
		if(commandIn.length < 4)
		{
			String error = printCommand(commandIn) ; 
			error += (": INVALID COMMAND: not enough parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		//check for too many parameters
		else if( commandIn.length > 4)
		{
			String error = printCommand(commandIn) ;
			error += (": INVALID COMMAND: too many parameters") ; 
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		else if( commandIn[2].matches("threslat") && parameters.cmLateralWeightOn)
		{
			try{
				if(Double.valueOf(commandIn[3]).doubleValue() < 0)
				{
					String error = printCommand(commandIn) ;
					error += (": INVALID COMMAND: value is less than 0") ;
					Exception ex = new Exception(error) ;
					throw ex ;
				}
				parameters.cmLateralThreshold = Double.valueOf(commandIn[3]).doubleValue() ;
				printCommand(commandIn) ;
				System.out.print(": valid command: value = " + parameters.cmLateralThreshold) ;
				
			}
			catch(NumberFormatException e){
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: need a float/double for \"value\"") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}
		}
		else if( commandIn[2].matches("thresver") && parameters.cmVerticalWeightOn)
		{
			try{
				if(Double.valueOf(commandIn[3]).doubleValue() < 0)
				{
					String error = printCommand(commandIn) ;
					error += (": INVALID COMMAND: value is less than 0") ;
					Exception ex = new Exception(error) ;
					throw ex ;
				}
				parameters.cmVerticalThreshold = Double.valueOf(commandIn[3]).doubleValue() ;
				printCommand(commandIn) ;
				System.out.print(": valid command: value = " + parameters.cmVerticalThreshold) ;
			}
			catch(NumberFormatException e){
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: need a float/double for \"value\"") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}			
		}
		else if( commandIn[2].matches("thresang") && parameters.cmAngularWeightOn)
		{
			try{
				if(Double.valueOf(commandIn[3]).doubleValue() < 0)
				{
					String error = printCommand(commandIn) ;
					error += (": INVALID COMMAND: value is less than 0") ;
					Exception ex = new Exception(error) ;
					throw ex ;
				}
				parameters.cmAngularThreshold = Double.valueOf(commandIn[3]).doubleValue() ;
				printCommand(commandIn) ;
				System.out.print(": valid command: value = " + parameters.cmAngularThreshold) ;
			}
			catch(NumberFormatException e){
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: need a float/double for \"value\"") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}			
		}
		else if( commandIn[2].matches("thresspe") && parameters.cmSpeedWeightOn)
		{
			try{
				if(Double.valueOf(commandIn[3]).doubleValue() < 0)
				{
					String error = printCommand(commandIn) ;
					error += (": INVALID COMMAND: value is less than 0") ;
					Exception ex = new Exception(error) ;
					throw ex ;
				}				
				parameters.cmSpeedThreshold = Double.valueOf(commandIn[3]).doubleValue() ;
				printCommand(commandIn) ;
				System.out.print(": valid command: value = " + parameters.cmSpeedThreshold) ;
			}
			catch(NumberFormatException e){
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: need a float/double for \"value\"") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}			
		}
		else if( commandIn[2].matches("thresres"))
		{
			try{
				if(Double.valueOf(commandIn[3]).doubleValue() < 0)
				{
					String error = printCommand(commandIn) ;
					error += (": INVALID COMMAND: value is less than 0") ;
					Exception ex = new Exception(error) ;
					throw ex ;
				}
				parameters.cmResidualThreshold = Double.valueOf(commandIn[3]).doubleValue() ;
				printCommand(commandIn) ;
				System.out.print(": valid command: value = " + parameters.cmResidualThreshold) ;
			}
			catch(NumberFormatException e){
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: need a float/double for \"value\"") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}			
		}
		else if( commandIn[2].matches("horiztim"))
		{
			try{
				if(Double.valueOf(commandIn[3]).doubleValue() < 0)
				{
					String error = printCommand(commandIn) ;
					error += (": INVALID COMMAND: value is less than 0") ;
					Exception ex = new Exception(error) ;
					throw ex ;
				}
				parameters.tsTimeHorizon = (long)Double.valueOf(commandIn[3]).doubleValue() ;
				printCommand(commandIn) ;
				System.out.print(": valid command: value = " + parameters.tsTimeHorizon) ;
			}
			catch(NumberFormatException e){
				String error = printCommand(commandIn) ;
				error += (": INVALID COMMAND: need a float/double for \"value\"") ;
				Exception ex = new Exception(error) ;
				throw ex ;
			}			
		}
		else
		{
			String error = printCommand(commandIn) ;
			error += (": INVALID COMMAND: parameters don't match ") ;
			error += ("or cannot set disabled parameter") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		
		
	}
	//---------------------------------------------------------------------------------
	
	public static void parseDisable(String[] commandIn) throws Exception
	{
		if(commandIn.length < 3)
		{
			String error = printCommand(commandIn) ;
			error += (": INVALID COMMAND: not enough parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		else if(commandIn.length > 3)
		{
			String error = printCommand(commandIn) ;
			error += (": INVALID COMMAND: too many parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		else if(commandIn[2].matches("threslat"))
		{
			parameters.cmLateralWeightOn = false ;
			System.out.print("valid command: ") ;
			printCommand(commandIn) ;
		}
		else if(commandIn[2].matches("thresver"))
		{	parameters.cmVerticalWeightOn = false ; 
			System.out.print("valid command: ") ;
			printCommand(commandIn) ;
		} 
		else if(commandIn[2].matches("thresang"))
		{	parameters.cmAngularWeightOn = false ; 
			System.out.print("valid command: ") ;
			printCommand(commandIn) ;
		} 
		else if(commandIn[2].matches("thresspe"))
		{	parameters.cmSpeedWeightOn = false ; 
			System.out.print("valid command: ") ;
			printCommand(commandIn) ;
		} 
		else if(commandIn[2].matches("thresres") || commandIn[2].matches("horiztim"))
		{
			String error = printCommand(commandIn).toString() ;
			error += (": INVALID COMMAND: parameter is enabled by default, cannot disable") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		else
		{
			String error = printCommand(commandIn) ;
			error+=(": INVALID COMMAND: parameter does not match with \"disable\"") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}

	}
	//---------------------------------------------------------------------------------
	public static void parseEnable(String[] commandIn) throws Exception
	{
		if(commandIn.length < 3)
		{
			String error = printCommand(commandIn) ;
			error+=(": INVALID COMMAND: not enough parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		else if(commandIn.length > 3)
		{
			String error = printCommand(commandIn) ;
			error+=(": INVALID COMMAND: too many parameters") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		else if(commandIn[2].matches("threslat"))
		{
			parameters.cmLateralWeightOn = true ;
			System.out.print("valid command: ") ;
			printCommand(commandIn) ;
		}
		else if(commandIn[2].matches("thresver"))
		{	parameters.cmVerticalWeightOn = true ; 
			System.out.print("valid command: ") ;
			printCommand(commandIn) ;
		} 
		else if(commandIn[2].matches("thresang"))
		{	parameters.cmAngularWeightOn = true ; 
			System.out.print("valid command: ") ;
			printCommand(commandIn) ;
		} 
		else if(commandIn[2].matches("thresspe"))
		{	parameters.cmSpeedWeightOn = true ; 
			System.out.print("valid command: ") ;
			printCommand(commandIn) ;
		} 
		else if(commandIn[2].matches("thresres") || commandIn[2].matches("horiztim"))
		{
			String error = printCommand(commandIn) ;
			error+=(": INVALID COMMAND: parameter is enabled by default, cannot enable") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		else
		{
			String error = printCommand(commandIn) ;
			error+=(": INVALID COMMAND: parameter does not match with \"enable\"") ;
			Exception ex = new Exception(error) ;
			throw ex ;
		}
		
	}
	
}
