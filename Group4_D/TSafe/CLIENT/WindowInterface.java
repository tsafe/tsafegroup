/*
 * Created on Aug 26, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package tsafe.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionListener;

import tsafe.client.graphical_client.FlightList;
import tsafe.client.graphical_client.FlightMap;
import tsafe.client.graphical_client.GraphicalClient;
import tsafe.client.graphical_client.GraphicalWindow;
import tsafe.client.textual_client.TextualClient;
import tsafe.client.textual_client.TextualWindow;
import tsafe.common_datastructures.client_server_communication.ComputationResults;

/**
 * @author cackermann
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
abstract public class WindowInterface extends JFrame implements ListSelectionListener,
		ActionListener {

	/**
	 * List of flights to select from
	 */
	protected FlightList flightList;

	/**
	 * Parameters dialog box
	 */
	protected ParametersDialog paramsDialog;

	/**
	 * Manages the communication to the server
	 */
	protected ClientInterface client;
	
	/**
	 * This window's menubar
	 */
	protected TsafeMenu menu;
	
	/**
	 * Show Menu Initial Selections for graphical client.
	 */
	protected static final int INITIAL_SHOW_FIXES_OPTION = FlightMap.SHOW_NONE;

	protected static final int INITIAL_SHOW_FLIGHTS_OPTION = FlightMap.SHOW_ALL;

	protected static final int INITIAL_SHOW_ROUTES_OPTION = FlightMap.SHOW_ALL;

	protected static final int INITIAL_SHOW_TRAJS_OPTION = FlightMap.SHOW_WITH_PLAN;

	protected static final String INITIAL_SHOW_FIXES_TEXT = showOptionToText(INITIAL_SHOW_FIXES_OPTION);

	protected static final String INITIAL_SHOW_FLIGHTS_TEXT = showOptionToText(INITIAL_SHOW_FLIGHTS_OPTION);

	protected static final String INITIAL_SHOW_ROUTES_TEXT = showOptionToText(INITIAL_SHOW_ROUTES_OPTION);

	protected static final String INITIAL_SHOW_TRAJS_TEXT = showOptionToText(INITIAL_SHOW_TRAJS_OPTION);

	
	public WindowInterface(ClientInterface client, String title) {

		super(title);
		this.client = client;

		//		 Create a parameters dialog box
		this.paramsDialog = ParametersDialog.getParametersDialog(this, this.client
				.getParameters());
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		//Quit everything when the window is closed
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				WindowInterface window = (WindowInterface)e.getWindow();
				if(menu != null) {
					if(window instanceof GraphicalWindow)
						menu.toggleGraphicalWindow();
					else if(window instanceof TextualWindow)
						menu.toggleTextualWindow();
				}
			}
		});

	}
	
	public abstract void showMenuChanged(String submenu, String submenuItem);

	public abstract void updateWindow(ComputationResults results);

	/**
	 * Starts parsing the feed and displays the window
	 * 
	 * @overrides Window.show()
	 */
	public void show() {
		super.show();
	}


	public void actionPerformed(ActionEvent e) {

		// Conformance Monitor parameters was selected
		if (e.getActionCommand().equals(TsafeMenu.CONF_MONITOR_TEXT))
			this.paramsDialog.showConformanceMonitorParameters();

		// Trajectory Synthesizer parameters was selected
		else if (e.getActionCommand().equals(TsafeMenu.TRAJ_SYNTH_TEXT))
			this.paramsDialog.showTrajectorySynthesizerParameters();

		// Toggle the graphical window visible/invisible
		else if(e.getActionCommand().equals(TsafeMenu.DISPLAY_GRAPHIC_CLIENT_TEXT)) {

			// If "this" refers to the GrahpicalWindow
			if(this instanceof GraphicalWindow) {

				// If "this" is the only window visible, confirm with the user before exiting TSAFE
				if(this.client.partner == null || !this.client.partner.window.isShowing()) {

					if(JOptionPane.showConfirmDialog(this, "Are you sure you want to exit TSAFE?", "Exit TSAFE?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
						menu.toggleGraphicalWindowMenuItem();
						return;
					}

					if(this.client.partner != null)
						this.client.partner.window.dispose();

					this.hide();
					this.client.server.detachObserver(this.client);
					this.dispose();

				// If this is not the onlt visible window, just hide it
				} else {

					this.hide();
					this.client.server.detachObserver(this.client);

				}

			// If our GraphicalWindow partner is null, make it and show it
			} else if(this.client.partner == null) {

				this.client.partner = new GraphicalClient(this.client.server);
				this.client.server.attachObserver(this.client.partner);
				this.client.partner.start();

			// If our partner is only hidden, show it
			} else if(!this.client.partner.window.isShowing()) {

				this.client.server.attachObserver(this.client.partner);
				this.client.partner.window.show();

			// If our partner is visible, hide it
			} else {

				this.client.partner.window.hide();
				this.client.server.detachObserver(this.client.partner);

			}

		}

		// Toggle the graphical window visible/invisible
		else if(e.getActionCommand().equals(TsafeMenu.DISPLAY_TEXT_CLIENT_TEXT)) {

			// If "this" refers to the TextualWindow
			if(this instanceof TextualWindow) {

				// If "this" is the only window visible, confirm with the user before exiting TSAFE
				if(this.client.partner == null || !this.client.partner.window.isShowing()) {

					if(JOptionPane.showConfirmDialog(this, "Are you sure you want to exit TSAFE?", "Exit TSAFE?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
						menu.toggleTextualWindowMenuItem();
						return;
					}

					if(this.client.partner != null)
						this.client.partner.window.dispose();

					this.hide();
					this.client.server.detachObserver(this.client);
					this.dispose();

				// If this is not the onlt visible window, just hide it
				} else {

					this.hide();
					this.client.server.detachObserver(this.client);

				}

			// If our TextualWindow partner is null, make it and show it
			} else if(this.client.partner == null) {

				this.client.partner = new TextualClient(this.client.server);
				this.client.server.attachObserver(this.client.partner);
				this.client.partner.start();

			// If our partner is only hidden, show it
			} else if(!this.client.partner.window.isShowing()) {

				this.client.server.attachObserver(this.client.partner);
				this.client.partner.window.show();

			// If our partner is visible, hide it
			} else {

				this.client.partner.window.hide();
				this.client.server.detachObserver(this.client.partner);

			}

		}

	}

	//	 PRIVATE HELPER METHODS
	protected TsafeMenu makeTsafeMenu() {
		menu = new TsafeMenu(this, INITIAL_SHOW_FIXES_TEXT,
				INITIAL_SHOW_FLIGHTS_TEXT, INITIAL_SHOW_ROUTES_TEXT,
				INITIAL_SHOW_TRAJS_TEXT);
		return menu;
	}

	protected static String showOptionToText(int showOption) {
		switch (showOption) {
		case FlightMap.SHOW_ALL:
			return TsafeMenu.ALL_TEXT;
		case FlightMap.SHOW_SELECTED:
			return TsafeMenu.SELECTED_TEXT;
		case FlightMap.SHOW_WITH_PLAN:
			return TsafeMenu.WITH_PLAN_TEXT;
		case FlightMap.SHOW_CONFORMING:
			return TsafeMenu.CONFORMING_TEXT;
		case FlightMap.SHOW_BLUNDERING:
			return TsafeMenu.BLUNDERING_TEXT;
		case FlightMap.SHOW_NONE:
			return TsafeMenu.NONE_TEXT;
		}

		throw new RuntimeException("Invalid Show Option");
	}

	protected static int showTextToOption(String showText) {
		if (showText.equals(TsafeMenu.ALL_TEXT))
			return FlightMap.SHOW_ALL;
		if (showText.equals(TsafeMenu.SELECTED_TEXT))
			return FlightMap.SHOW_SELECTED;
		if (showText.equals(TsafeMenu.WITH_PLAN_TEXT))
			return FlightMap.SHOW_WITH_PLAN;
		if (showText.equals(TsafeMenu.CONFORMING_TEXT))
			return FlightMap.SHOW_CONFORMING;
		if (showText.equals(TsafeMenu.BLUNDERING_TEXT))
			return FlightMap.SHOW_BLUNDERING;
		if (showText.equals(TsafeMenu.NONE_TEXT))
			return FlightMap.SHOW_NONE;

		throw new RuntimeException("Invalid Show Text");
	}

	public void startWindow() {
		this.pack();
		this.show();
	}
	
	public void updateParametersDialog() {
		paramsDialog.setTextFields();
	}
	
}
