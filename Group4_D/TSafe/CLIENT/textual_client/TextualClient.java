/*
 * Created on Dec 3, 2004
 *
 */
package tsafe.client.textual_client;

import java.util.Collection;

import tsafe.client.ClientInterface;
import tsafe.client.graphical_client.GraphicalClient;
import tsafe.common_datastructures.LatLonBounds;
import tsafe.common_datastructures.TSAFEProperties;
import tsafe.common_datastructures.client_server_communication.ComputationResults;
import tsafe.common_datastructures.client_server_communication.UserParameters;
import tsafe.server.ServerInterface;

/**
 * @author sscout
 *
 */
public class TextualClient extends ClientInterface {

	/**
	 * Construct a TextualClient
	 */
	public TextualClient(ServerInterface server) {
		super(server);
		bounds = TSAFEProperties.getLatLonBounds();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		this.window = new TextualWindow(this);
		this.window.startWindow();
	}

	/**
	 * Queries the server for new flight data.
	 * 
	 * @see tsafe.client.ClientInterface#getFlightData()
	 */
	public void updateClient(ComputationResults results) {
		if(this.window != null)
			this.window.updateWindow(results);
	}

	/**
	 * @return Returns the parameters.
	 */
	public UserParameters getParameters() {
		return parameters;
	}

	/**
	 * Interface method for the client for reading the fixes.
	 * 
	 * @return All fixes, stored in the database.
	 */
	public Collection getFixes() {
		return this.server.getFixes();
	}

	/**
	 * @return Returns the server.
	 */
	public ServerInterface getServer() {
		return server;
	}

	/**
	 * @param server
	 *            The server to set.
	 */
	public void setServer(ServerInterface server) {
		this.server = server;
	}

	/**
	 * @return Returns the bounds.
	 */
	public LatLonBounds getBounds() {
		return bounds;
	}

	public void testing() {
		ClientInterface client = new GraphicalClient(this.server);
		client.setBounds(bounds);
		client.run();
	}
}
