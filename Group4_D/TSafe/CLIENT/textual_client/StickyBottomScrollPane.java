/*
 * Created on Dec 3, 2004
 *
 */
package tsafe.client.textual_client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.util.LinkedList;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * @author J
 *
 */
public class StickyBottomScrollPane extends JPanel {

    private JScrollPane scrollPane;
    private JTextPane textPane;

    /**
     * Maximum number of characters in textPane.
     */
    private int MAX_BUFFER_LENGTH = 50000;
    
    /**
     * Wait until we are threshold chars over max buffer length 
     * before truncating.
     */
    private int BUFFER_TRUNCATE_THRESHOLD = 3000;

    /**
     * threshold in pixels used for comparing scroll bar position with 
     * bottom of the scroll space. 
     */
    private int STICKY_THRESHOLD = 30;
    
    private Document doc;
    
    private Object mutex = new Object();

    private LinkedList appendQueue = new LinkedList(); 

    /**
     * Appender thread
     */
    private Appender appender;

    /**
     * Scroll every line.  Choosing false here makes scrolling
     * faster but jumpy.
     */
    private boolean scrollEverLine = true;
    
    private class TextAddition {
    	String text;
    	AttributeSet style;
    	int scrollValue;
    	
    	TextAddition(String text, AttributeSet style) {
    		this.text = new String(text);
			this.style = style.copyAttributes();
    	}

    	TextAddition(int scrollValue) {
    		this.text = null;
			this.style = null;
			this.scrollValue = scrollValue;
    	}
    }
    
    volatile private boolean disableAutoScroll = false;
    volatile private boolean updateRequested = false;
    
    public StickyBottomScrollPane() {
    	textPane = new JTextPane();
        scrollPane = new JScrollPane(textPane);
        
		setLayout(new java.awt.BorderLayout());
        add(scrollPane, BorderLayout.CENTER);

        textPane.setEditable(false);
		doc = textPane.getDocument();

        //textPane.setContentType("text/rtf");
        //textPane.setEditorKit(new RTFEditorKit());
		textPane.setForeground(Color.white);
        textPane.setBackground(Color.black);

        MouseAdapter scrollPauseAdapter = new MouseAdapter() { 
			public void mouseReleased(java.awt.event.MouseEvent e) {
				enableAutoScroll(true);
			}
			public void mousePressed(java.awt.event.MouseEvent e) {
				enableAutoScroll(false);
			}
		};
		
		scrollPane.getVerticalScrollBar().addMouseListener(scrollPauseAdapter);
		scrollPane.getHorizontalScrollBar().addMouseListener(scrollPauseAdapter);

	    appender = new Appender();
    }
    
	/**
	 * Append text to pane.  Note: blank lines are best specified with
	 * a single space rather than no space. " \n" makes for smoother scrolling
	 * than  "\n"
	 * 
	 * @param text
	 */
    void append(String text) {
        Style style = StyleContext.getDefaultStyleContext().
		  getStyle( StyleContext.DEFAULT_STYLE );
    	append(text,Color.white,Color.black,false,StyleConstants.getFontSize(style));
    }

	/**
	 * Append text to pane.  Note: blank lines are best specified with
	 * a single space rather than no space. " \n" makes for smoother scrolling
	 * than  "\n"
	 * 
	 * @param text
	 */
    void append(String text, int size) {
    	append(text,Color.white,Color.black,false,size);
    }

	/**
	 * Append text to pane.  Note: blank lines are best specified with
	 * a single space rather than no space. " \n" makes for smoother scrolling
	 * than  "\n"
	 * 
	 * @param text
	 * @param foreground
	 */
    void append(String text, Color foreground) {
        Style style = StyleContext.getDefaultStyleContext().
		  getStyle( StyleContext.DEFAULT_STYLE );
    	append(text,foreground,Color.black,false,StyleConstants.getFontSize(style));
    }

	/**
	 * Append text to pane.  Note: blank lines are best specified with
	 * a single space rather than no space. " \n" makes for smoother scrolling
	 * than  "\n"
	 * 
	 * @param text
	 * @param foreground
	 * @param size
	 */
    void append(String text, Color foreground, int size) {
    	append(text,foreground,Color.black,false,size);
    }

    /**
	 * Append text to pane.  Note: blank lines are best specified with
	 * a single space rather than no space. " \n" makes for smoother scrolling
	 * than  "\n"
     * 
     * @param txt - text to append
     * @param color - foreground/text color
     * @param bgColor - background color
     * @param isBold - bold font
     * @param size - font size
     */
    public void append(String txt, Color color, Color bgColor, boolean isBold, int size) {
        SimpleAttributeSet style = new SimpleAttributeSet(StyleContext.getDefaultStyleContext().
		  getStyle( StyleContext.DEFAULT_STYLE ).copyAttributes());
	    StyleConstants.setForeground(style, color);
	    StyleConstants.setBackground(style, bgColor);
	    StyleConstants.setFontSize(style, size);
	    StyleConstants.setBold(style, isBold);
	    
	    synchronized (mutex) {
		    appendQueue.add(new TextAddition(txt,style));
			mutex.notify();
	    }
    }   
	
	public JTextPane getTextPane() {
		return textPane;
	}
	
	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void enableAutoScroll(boolean enable) {
		synchronized (mutex) {
			disableAutoScroll = !enable;
			if(enable) {
				mutex.notifyAll();
			}
		}
	}
	
	private class Appender extends Thread {

		volatile boolean done = false;
		
		Appender() {
			start();
		}
	
		public void end() {
			synchronized (mutex) {
				done = true;
			}
		}

		public void finalize() throws Throwable {
			end();
			super.finalize();
		}
		
		public void run() {
	    	boolean atBottom;
	    	int value;

	    	while(true) {
		    	synchronized (mutex) {
		    		while(appendQueue.size() == 0 || updateRequested || disableAutoScroll) {
		    			try { mutex.wait(); } catch(Exception e) { }
		    		}

		    		value = scrollPane.getVerticalScrollBar().getValue();
		    			
			    	//System.out.println("value = "+(scrollPane.getVerticalScrollBar().getValue()+textPane.getVisibleRect().height)+" max = "+scrollPane.getVerticalScrollBar().getMaximum()+" text height "+textPane.getVisibleRect().height);
			    	atBottom = ((value+textPane.getVisibleRect().height) >= scrollPane.getVerticalScrollBar().getMaximum()-STICKY_THRESHOLD);
	
			    	boolean newLineFound = false;
			    	
				    TextAddition adding = null;
				    int len = 0;
				    while(appendQueue.size() > 0 && !newLineFound) {
					    len = doc.getLength();
					    adding = (TextAddition)appendQueue.removeFirst();
						String text = adding.text;
					    AttributeSet style = adding.style;
					    
					    if(scrollEverLine && !newLineFound)
					    	newLineFound = (text.indexOf('\n') != -1);
					    
					    try {
							doc.insertString(len, text, style);
					    } catch (Exception e) {
					        System.out.println("Failed to append msg ["+text+"]\n"+e);
					    }
				    }
	
				    if(atBottom && adding != null) {
					    len = doc.getLength();
					    try {
						    if(newLineFound && len > (MAX_BUFFER_LENGTH+BUFFER_TRUNCATE_THRESHOLD)) {
						    	doc.remove(0,len - MAX_BUFFER_LENGTH);
						    	len = doc.getLength(); 
						    }
					    } catch(Exception e) {  }
	
					    textPane.setCaretPosition(len);
					    SwingUtilities.invokeLater(new Scroller(scrollPane.getVerticalScrollBar()));
			    		appendQueue.addFirst(new TextAddition(-1));
			    		updateRequested = true;
				    } else {
			    		updateRequested = false;
				    }
				    if(done)
				    	break;
		    	}
	    	}
	    }
	}
	
    private class Scroller implements Runnable {
        private JScrollBar m_oScrollbar;
        private int value;
        
        public Scroller(JScrollBar scrollbar) {
            m_oScrollbar = scrollbar;
            this.value = -1;
        }

        public Scroller(JScrollBar scrollbar, int value) {
            m_oScrollbar = scrollbar;
            this.value = value;
        }
        
        public void run() {
        	int index;
        	
            synchronized (mutex) {
            	TextAddition added = (TextAddition)appendQueue.removeFirst();
            	value = added.scrollValue;
            	if(value != -1)
	        		index = value;
	        	else
	        		index = m_oScrollbar.getMaximum()-m_oScrollbar.getHeight();
	        	if(m_oScrollbar.getValue() != index) {
	        		m_oScrollbar.setValue(index);
	        	}
	        	
	            updateRequested = false;
            	mutex.notifyAll();
            }
        }
    }
    
}
