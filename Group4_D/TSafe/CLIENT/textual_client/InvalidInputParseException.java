/*
 * Created on Dec 6, 2004
 */
package tsafe.client.textual_client;

/**
 * @author Toure
 *
 */
public class InvalidInputParseException extends Exception{

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = -5087312267333450948L;

	public InvalidInputParseException(String message) {
        // Constructor.  Create a ParseError object containing
        // the given message as its error message.
     super(message);
  }


}
