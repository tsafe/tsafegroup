package tsafe.client.textual_client;

public abstract class CommandParser {
	
	/* command name */
	String name;
	
	/* Range of args, command name counts a 1 arg */
	int minArgs;
	int maxArgs;
	
	CommandParser(String name, int minArgs, int maxArgs) {
		this.name = new String(name);
		this.minArgs = minArgs;
		this.maxArgs = maxArgs;
	}
	
	void parse( String[] input) 
			//this exception is thrown when the command is invalid 
						throws InvalidInputParseException {
		if(input.length < minArgs || input.length > maxArgs)
			throw new InvalidInputParseException("Wrong number of arguments to command '"+name+"'.\n"+
											 usage()+"\n");
		
	}
	
	String usage() {
		StringBuffer sb = new StringBuffer("Usage: ");
		sb.append(name);
		int i;
		for(i=1;i<minArgs;i++)
			sb.append(" <argument>");
		for(;i<maxArgs;i++)
			sb.append(" [optional argument]");
		return sb.toString();
	}
	
	String getName() {
		return name;
	}
	
	int getMinArgs() {
		return minArgs;
	}
	int getMaxArgs() {
		return maxArgs;
	}
	
}