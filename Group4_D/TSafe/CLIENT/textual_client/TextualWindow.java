/*
 * Created on Dec 3, 2004
 *
 */
package tsafe.client.textual_client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;


import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;

import tsafe.client.TsafeMenu;
import tsafe.client.WindowInterface;
import tsafe.client.graphical_client.FlightList;
import tsafe.client.graphical_client.FlightMap;

import tsafe.common_datastructures.LatLonBounds;
import tsafe.common_datastructures.client_server_communication.ComputationResults;

/**
 * @author sscout
 *
 */
public class TextualWindow extends WindowInterface {

	/**
	 * Map on which flight data, fixes, etc are drawn
	 */
	private FlightFilter flightFilter;

	/**
	 * Main text console window
	 */
	private StickyBottomScrollPane displayPane;
	
	/**
	 * Text console input field
	 */
	private JTextField prompt;
	
	/**
	 * Input parser
	 */
	private InputParser parser;
	
	/**
	 * Textual pane displaying/scrolling tester
	 */
	//private TestAppend testAppend;
	
	/**
	 * Show Menu Initial Selections for graphical client.
	 */
	private static final int INITIAL_SHOW_FIXES_OPTION = FlightMap.SHOW_NONE;

	private static final int INITIAL_SHOW_FLIGHTS_OPTION = FlightMap.SHOW_ALL;

	private static final int INITIAL_SHOW_ROUTES_OPTION = FlightMap.SHOW_ALL;

	private static final int INITIAL_SHOW_TRAJS_OPTION = FlightMap.SHOW_WITH_PLAN;

	private static final String INITIAL_SHOW_FIXES_TEXT = showOptionToText(INITIAL_SHOW_FIXES_OPTION);

	private static final String INITIAL_SHOW_FLIGHTS_TEXT = showOptionToText(INITIAL_SHOW_FLIGHTS_OPTION);

	private static final String INITIAL_SHOW_ROUTES_TEXT = showOptionToText(INITIAL_SHOW_ROUTES_OPTION);

	private static final String INITIAL_SHOW_TRAJS_TEXT = showOptionToText(INITIAL_SHOW_TRAJS_OPTION);

	private static final String ENTER_COMMAND = "ENTER COMMAND";

	public TextualWindow(TextualClient client) {
		super(client, "TSAFE Console");

		// Create a flight map
		this.flightFilter = makeFlightFilter(this.client.getBounds(),
				this.client.getFixes());
	
		// Build the content pane
		setJMenuBar(makeTsafeMenu());		
	
		//Create a list of flights and add the client as a listener
		this.flightList = FlightList.getFlightList();
		flightList.addListSelectionListener(this);

		JPanel consolePanel = new JPanel();
		consolePanel.setLayout(new BorderLayout());
		displayPane = new StickyBottomScrollPane();
		displayPane.setPreferredSize(new Dimension(750,420));
		prompt = new JTextField();
		prompt.setBorder(BorderFactory.createLoweredBevelBorder());
		consolePanel.add(displayPane, BorderLayout.CENTER);
		consolePanel.add(prompt, BorderLayout.SOUTH);

		super.setContentPane(consolePanel);
		displayPane.append("TSafe Command Console:  version 1.0\n");
		
		parser = new InputParser(this.client.getParameters(), this.menu, 
								this.displayPane, this);
		
		//testAppend = new TestAppend(displayPane);
		
		displayPane.getTextPane().addMouseListener(new java.awt.event.MouseAdapter() { 
			public void mouseReleased(java.awt.event.MouseEvent e) {
				if(displayPane.getTextPane().getCaret().getDot() != displayPane.getTextPane().getCaret().getMark())
					displayPane.getTextPane().copy();
				displayPane.getTextPane().getCaret().setDot(displayPane.getTextPane().getCaret().getMark());
				requestFieldFocus();
				displayPane.enableAutoScroll(true);
			}
			public void mousePressed(java.awt.event.MouseEvent e) {
				displayPane.enableAutoScroll(false);
			}
		});

		prompt.setActionCommand(ENTER_COMMAND);
		prompt.addActionListener(new java.awt.event.ActionListener() { 
			public void actionPerformed(java.awt.event.ActionEvent e) {    
				//firePropertyChange("prompt","",prompt.getText());
				//System.out.println("fired PropertyChange(prompt,\"\","+prompt.getText()+")");
				if(ENTER_COMMAND.equals(e.getActionCommand())) {
					try {
						//System.out.println(AWTEvent.ACTION_EVENT_MASK);
						parser.parse(prompt.getText());
					} catch(InvalidInputParseException exception) {
						displayPane.append(exception.getMessage()+"\n",Color.orange);
					}
					prompt.setText("");
				}
			}
		});

	}

	
	private static FlightFilter makeFlightFilter(LatLonBounds bounds,
			Collection fixes) {
		FlightFilter flightFilter = new FlightFilter(bounds, fixes);
		flightFilter.setShowFixes(INITIAL_SHOW_FIXES_OPTION);
		flightFilter.setShowFlights(INITIAL_SHOW_FLIGHTS_OPTION);
		flightFilter.setShowRoutes(INITIAL_SHOW_ROUTES_OPTION);
		flightFilter.setShowTrajectories(INITIAL_SHOW_TRAJS_OPTION);
		return flightFilter;
	}

	/**
	 * Called by TsafeMenu menu when recognizes menu events
	 */
	public void showMenuChanged(String submenu, String submenuItem) {
		int showOption = showTextToOption(submenuItem);

		if (submenu.equals(TsafeMenu.FIXES_TEXT))
			flightFilter.setShowFixes(showOption);

		else if (submenu.equals(TsafeMenu.FLIGHTS_TEXT))
			flightFilter.setShowFlights(showOption);

		else if (submenu.equals(TsafeMenu.ROUTES_TEXT))
			flightFilter.setShowRoutes(showOption);

		else if (submenu.equals(TsafeMenu.TRAJS_TEXT))
			flightFilter.setShowTrajectories(showOption);

		flightFilter.updateNeeded();
		repaint();
	}

	/**
	 * List selection event handler When the selected item has changed,
	 */
	public void valueChanged(ListSelectionEvent e) {
		Collection selectedFlights = flightList.getSelectedFlights();
		flightFilter.setSelectedFlights(selectedFlights);
		flightFilter.updateNeeded();
		repaint();
	}
	
	
	private void refreshWindow() {
		// Parameters changed, update flight map
		//flightFilter.updateNeeded();
		//flightFilter.repaint();
	}

	public void updateWindow(ComputationResults results) {

		flightList.setFlights(results.getFlights());

		// Update the flight map
		synchronized (flightFilter) {
			flightFilter.setFlights(results.getFlights());
			flightFilter.setBlunders(results.getBlunders());
			flightFilter.setFlightTrajectoryMap(results.getFlight2TrajectoryMap());
		}
		
		/* send to console */
	    Date date = new Date();
	    SimpleDateFormat df = new SimpleDateFormat("MMMM d, yyyy HH:mm:ss 'GMT'");
	    df.setTimeZone(TimeZone.getTimeZone("GMT"));
		displayPane.append(" \n");
		displayPane.append(df.format(date));
		displayPane.append("\n");
		try {
			parser.parse("options");
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		displayPane.append(" \n");
		
		flightFilter.printUpdate(displayPane);
		//flightMap.updateNeeded();
		//repaint();
	}

	public void requestFieldFocus() {
		if(!prompt.hasFocus())
			prompt.requestFocusInWindow();
	}
	
	public FlightList getFlightList() {
		return flightList;
	}
	
	
	
}
