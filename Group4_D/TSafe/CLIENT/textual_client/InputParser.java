/*
 * Created on Dec 2, 2004
 *
 */
package tsafe.client.textual_client;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Iterator;

import tsafe.client.TsafeMenu;
import tsafe.client.graphical_client.FlightList;
import tsafe.common_datastructures.Flight;
import tsafe.common_datastructures.client_server_communication.UserParameters;

/**
 * @author Aboubacar Toure
 *
 */
public class InputParser {
	
	private String command ;
	private String[] parameters ;
	
	ShowCommand shower ;
	SelectCommand selector ;
	SetCommand setter ;
	
	private UserParameters uParams;
	private TsafeMenu menu;
	private TextualWindow window;
	private StickyBottomScrollPane displayPane;

	CommandParser commands[] = { new OptionsCommand(), new ExitCommand() };
	
	public InputParser(UserParameters uParams, TsafeMenu menu, 
						StickyBottomScrollPane displayPane,
						TextualWindow window){
		this.uParams = uParams;
		this.menu = menu;
		this.displayPane = displayPane;
		this.window = window;
		
		shower = new ShowCommand();
		selector = new SelectCommand() ;
		setter = new SetCommand()  ;
	}

	/* (non-Javadoc)
	 * @see tsafe.client.graphical_client.CommandParser#parse(java.lang.String)
	 */
	public void parse(String input) throws InvalidInputParseException {
		String[] tokens = tokenize(input.trim()) ;
		
		String cmd = tokens[0] ;
		int len = tokens.length - 1 ; //number of parameters -1 => cmd is first one
		String[] param = new String[len] ; //command parameters
		for( int i=0; i<len; i++){
			param[i] = tokens[i+1] ;
		}

		/* Parse extendable command list */
		for( int j=0;j<commands.length;j++) { 
			if(cmd.equalsIgnoreCase(commands[j].getName())) {
				commands[j].parse(tokens);
				return;
			}
		}
		
		//if(isValidCommand(tokens)){
		command = cmd ;
		parameters = param ;
		
		doCommand() ;
		/*
}
		else{
			throw new InvalidInputParseException("\nInvalid command \""+cmd+"\".") ;
		}
		*/		
	}
	/**
	 * @param cmd
	 * @param params
	 */
	private void doCommand() throws InvalidInputParseException {
			
		if (command.equals("select")){
			selector.select() ;
		}
		
		else if (command.equals("show")){
			shower.show() ;
		}
		
		else if (command.equals("set") || command.equals("enable") || command.equals("disable")){
			setter.set() ;
		}
		
		else {
			throw new InvalidInputParseException("\nInvalid command \""+command+"\".") ;
		}
		
	}

	private String[] tokenize(String input){
		String[] tokens = input.split(" ") ;
		
		tokens[0] = tokens[0].toLowerCase() ; //fault tolerance
		
		return tokens;
	}
	
	private boolean isValidCommand(String[] cmd){
		
		String flightId = new String( "([a-zA-Z]+)([0-9])+" );

		if(cmd.length == 0)
			return false;
		
		if( cmd[0].compareTo( "select") == 0  ) {
			
			if ( cmd.length < 2 ) {
				return false ;
			}
			else if ( cmd.length == 2 ) {
				
				if ( cmd[1].compareTo( "*") == 0 )
					return true ;
				else if ( cmd[1].matches( flightId) ) {
										
					return true ;					
				}
				else 
					return false ;
				
			}
			else {
				for ( int a = 1 ; a < cmd.length ; ++a ) {				
					if ( !(cmd[a].matches( flightId) )  )						
						return false ;
				}
				return true ;
			}
			
		}
		else if ( cmd[0].compareTo( "show") == 0 ) {
			
			if ( cmd.length < 3 || cmd.length > 3 ) {
				return false ;
			}

			cmd[1] = cmd[1].toLowerCase() ;
			
			if ( cmd[1].compareTo( "fixes") == 0 ) {
				
				cmd[2] = cmd[2].toLowerCase() ;
				
				if ( cmd[2].equals( "all") || cmd[2].equals("none") ) 
					return true ;
				else 
					return false ;
			}
			else if ( cmd[1].compareTo( "flights") == 0 ) {
				
				cmd[2] = cmd[2].toLowerCase() ;
				
				if ( cmd[2].equals( "all") ||  cmd[2].equals("selected") || cmd[2].equals("withplans") || cmd[2].equals("conforming") || cmd[2].equals("blundering") || cmd[2].equals("none") ) 
					return true ;
				else 
					return false ;
			}
			else if ( cmd[1].compareTo( "routes") == 0 ) {
				
				cmd[2] = cmd[2].toLowerCase() ;
				
				if ( cmd[2].equals( "all") ||  cmd[2].equals("selected")  || cmd[2].equals("conforming") || cmd[2].equals("blundering") || cmd[2].equals("none") ) 
					return true ;
				else 
					return false ;
			}
			else if ( cmd[1].compareTo( "trajectories") == 0 ) {
				
				cmd[2] = cmd[2].toLowerCase() ;
				
				if ( cmd[2].equals( "all") ||  cmd[2].equals("selected")  || cmd[2].equals("withplans") || cmd[2].equals("conforming") || cmd[2].equals("blundering") || cmd[2].equals("none") ) 
					return true ;
				else 
					return false ;
			}
			
			
		}
		else if ( cmd[0].equals( "set") || cmd[0].equals( "enable") || cmd[0].equals( "disable")) {
			
			if ( cmd.length < 3 )
				return false ;

			if ( cmd[1].equals( "parameter")) {
				
				if ( cmd[2].equals( "threslat") ||  cmd[2].equals("thresver")  || cmd[2].equals("thresang") || cmd[2].equals("thresspe") || cmd[2].equals("thresres") || cmd[2].equals("horiztim") ) {
					
					if ( cmd[0].equals("set") && cmd.length == 4 ) {
						
						if (  cmd[3].matches("[0-9]+") ) 
							return true ;
						else
							return false ;
					}
					else if ( cmd.length == 3 && !cmd[0].equals("set") )
						return true ;
					else
						return false ;
						
				}
				else
					return false ;
				
				
			}
			else
				return false ;
			
		}
		
		return false ;
	}
	
	/**
	 * @return Returns the command.
	 */
	public String getCommand() {
		return command;
	}
	/**
	 * @return Returns the parameters.
	 */
	public String[] getParameters() {
		return parameters;
	}
	
	///////////////////
	class ShowCommand {
		private String arg ;
		private String type ;
		
		ShowCommand(){
		}
		
		void show() throws InvalidInputParseException{
			
			if(parameters .length == 2){
				arg = parameters [0].toLowerCase() ;
				type = parameters [1].toLowerCase() ;
			}
			else{
				StringBuffer sb = new StringBuffer();
				for(int i=0;i<parameters.length;i++) {
					sb.append(parameters[i]);
					sb.append(" ");
				}
				throw new InvalidInputParseException("Invalid show argument: "+sb.toString()) ;
			}
			
			if (arg.equals("fixes")){
				
				if (type.equals("all")){
					menu.setFixesAll();
				}
				else if ( type.equals("none")){
					menu.setFixesNone();
				}
				else {
					throw new InvalidInputParseException("Invalid show argument: "
							+arg+" type [all|none]: found "+type) ;
				}
				displayPane.append("show: fixes option set.\n",Color.CYAN);
			}
			else if (arg.equals("flights")){
				
				if (type.equals("all")){
					menu.setFlightsAll() ;
				}
				else if ( type.equals("selected")){
					menu.setFlightsSelected() ;
				}
				else if ( type.equals("withplans")){
					menu.setFlightsWithPlan() ;
				}
				else if ( type.equals("conforming")){
					menu.setFlightsConforming() ;
				}
				else if ( type.equals("blundering")){
					menu.setFlightsBlundering() ;
				}
				else if ( type.equals("none")){
					menu.setFlightsNone() ;
				}
				else {
					throw new InvalidInputParseException("Invalid show argument: "
							+arg+" type [all|selected|withplans|conforming|blundering|none]: found "+type);
				}
				displayPane.append("show: flights option set.\n",Color.CYAN);
			}
			else if (arg.equals("routes")){	
				
				if (type.equals("all")){
					menu.setRoutesAll() ;
				}
				else if ( type.equals("selected")){
					menu.setRoutesSelected() ;
				}
				else if ( type.equals("conforming")){
					menu.setRoutesConforming() ;
				}
				else if ( type.equals("blundering")){
					menu.setRoutesBlundering() ;
				}
				else if ( type.equals("none")){
					menu.setRoutesNone() ;
				}
				else {
					throw new InvalidInputParseException("Invalid show argument "
							+arg+" type [all|selected|conforming|blundering|none]: found "+type);
				}
				displayPane.append("show: routes option set.\n",Color.CYAN);
			}		
			else if (arg.equals("trajectories")){
				
				if (type.equals("all")){
					menu.setTrajsAll() ;
				}
				else if ( type.equals("selected")){
					menu.setTrajsSelected() ;
				}
				else if ( type.equals("withplans")){
					menu.setTrajsWithPlan() ;
				}
				else if ( type.equals("conforming")){
					menu.setTrajsConforming() ;
				}
				else if ( type.equals("blundering")){
					menu.setTrajsBlundering() ;
				}
				else if ( type.equals("none")){
					menu.setTrajsNone() ;
				}
				else {
					throw new InvalidInputParseException("Invalid show argument: "
							+arg+" type [all|selected|withplans|conforming|blundering|none]: found "+type);
				}
				displayPane.append("show: trajectories option set.\n",Color.CYAN);
			}
			else{
				throw new InvalidInputParseException("Invalid show argument: found "+arg) ;
			}
		}
	}
	/////////////////
	class SetCommand{
		private String arg ;
		private String type ;
		
		SetCommand(){
		}
		
		void set() throws InvalidInputParseException{
			
				if ( parameters.length > 3 && command.equals("set") ) {
					throw new InvalidInputParseException("Invalid Command : too many arguments for "+command) ;
				}
				else if ( parameters.length < 3 && command.equals("set") ) {
					throw new InvalidInputParseException("Invalid Command : too few arguments for "+command) ;
				}
				else if ( parameters.length > 2 && (command.equals("enable") || command.equals("disable"))) {
					throw new InvalidInputParseException("Invalid Command : too many arguments for "+command) ;
				}
				else if ( parameters.length < 2 && (command.equals("enable") || command.equals("disable"))) {
					throw new InvalidInputParseException("Invalid Command : too few arguments for "+command) ;
				}
				else if (command.equals("set")){
					String paramName = parameters[1] ;
					double value ;
					try{
						value = Double.parseDouble(parameters[2]) ;
					}
					catch(Exception e){
						throw new InvalidInputParseException("Invalid Command : value ["
								                           +parameters[2]+"] should be a number") ;
					}
					
					paramName = paramName.toLowerCase() ;
					
					if (paramName.equals("threslat")){
						uParams.cmLateralThreshold  = value ;
					}
					else if (paramName.equals("thresver")){
						uParams.cmVerticalThreshold = value ;
					}
					else if (paramName.equals("thresang")){
						uParams.cmAngularThreshold = value ;
					}
					else if (paramName.equals("thresspe")){
						uParams.cmSpeedThreshold = value ;
					}
					else if (paramName.equals("thresres")){
						uParams.cmResidualThreshold = value ;
					}
					else if (paramName.equals("horiztim")){
						uParams.tsTimeHorizon = (long) value ;
					} else {
						throw new InvalidInputParseException("Invalid Command : invalid parameter "+paramName+" for "+command) ;
					}

					displayPane.append(command+": set "+paramName+" to "+value+"\n",Color.CYAN);
					
					window.updateParametersDialog() ;
				}
				else if (command.equals("enable")){
					
					String tpName = parameters[1].toLowerCase() ;
					
					if (  tpName.compareTo("threslat") == 0 )
						uParams.cmLateralWeightOn = true ;
					else if (  tpName.compareTo("thresver") == 0 ) 
						uParams.cmVerticalWeightOn = true ;
					else if (  tpName.compareTo("thresang") == 0 ) 
						uParams.cmAngularWeightOn = true ;
					else if (  tpName.compareTo("thresspe") == 0 ) 
						uParams.cmSpeedWeightOn = true ;
					else if (  tpName.compareTo("thresres") == 0 ) 
						throw new InvalidInputParseException("Residual Threshold cannot be enabled") ;
					else if (  tpName.compareTo("hoiztim") == 0 ) 
						throw new InvalidInputParseException("Time Horizon cannot be enabled") ;
					else {
						throw new InvalidInputParseException("Invalid Command : invalid parameter "+tpName+" for "+command) ;
					}

					displayPane.append(command+": enabled "+tpName+"\n",Color.CYAN);
					
					window.updateParametersDialog() ;
				}
				else if (command.equals("disable")){
					String tpName = parameters[1].toLowerCase() ;
					
					if (  tpName.compareTo("threslat") == 0 ) 
						uParams.cmLateralWeightOn = false ;
					else if ( tpName.compareTo("thresver") == 0 ) 
						uParams.cmVerticalWeightOn = false ;
					else if (  tpName.compareTo("thresang") == 0 ) 
						uParams.cmAngularWeightOn = false ;
					else if (  tpName.compareTo("thresspe") == 0 ) 
						uParams.cmSpeedWeightOn = false ;
					else if (  tpName.compareTo("thresres") == 0 ) 
						throw new InvalidInputParseException("Residual Threshold cannot be enabled") ;
					else if (  tpName.compareTo("hoiztim") == 0 ) 
						throw new InvalidInputParseException("Time Horizon cannot be enabled") ;
					else {
						throw new InvalidInputParseException("Invalid Command : invalid parameter "+tpName+" for "+command) ;
					}
					
					displayPane.append(command+": disabled "+tpName+"\n",Color.CYAN);
					
					window.updateParametersDialog() ;
					
				}
				
				
		
		}
	}
	///////////////////
	
	
	class SelectCommand {
		
		private String arg ;
		private String type ;
		
		SelectCommand(){
		}
		
		
		void select() throws InvalidInputParseException{
			
				if ( parameters.length > 1 && parameters[0].compareTo("*") == 0 ) {
					throw new InvalidInputParseException("Invalid Command : Only single * or list of flightIDs accepted") ;
				}
				else {
					//valid args
					if(parameters.length == 1 && parameters[0].equalsIgnoreCase("*")) {
						FlightList fl = window.getFlightList();
						fl.selectAllFlights();
						displayPane.append("select: Selected all.\n",Color.CYAN);
					} else {
						FlightList fl = window.getFlightList();
						fl.selectNoFlights();
						for(int i=0;i<parameters.length;i++) {
							if(!fl.selectFlight(parameters[i]))
								displayPane.append("select: "+parameters[i]+" a not valid flight ID.\n",Color.ORANGE);
							else
								displayPane.append("select: selected "+parameters[i]+".\n",Color.CYAN);
						}
					}
				}
				
		
		}
		
	} //end of selectCommand class
	
	/////////////////////////
	
	class OptionsCommand extends CommandParser {

		OptionsCommand() {
			super("options",1,1);
		}
		
		/* (non-Javadoc)
		 * @see tsafe.client.textual_client.CommandParser#parse(java.lang.String[])
		 */
		void parse(String[] input) throws InvalidInputParseException {
			super.parse(input);
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("   Displaying fixes:  ");
			if(menu.getFixesAll())
				sb.append("All    ");
			else
				sb.append("None    ");
			
			sb.append("Displaying flights:  ");
			if(menu.getFlightsAll())
				sb.append("All    ");
			else if(menu.getFlightsNone())
				sb.append("None    ");
			else if(menu.getFlightsWithPlan())
				sb.append("With Flight Plan    ");
			else if(menu.getFlightsConforming())
				sb.append("Conforming    ");
			else if(menu.getFlightsBlundering())
				sb.append("Blundering    ");
			else if(menu.getFlightsSelected())
				sb.append("Selected    ");
			
			sb.append("Displaying routes:  ");
			if(menu.getRoutesAll()) 
				sb.append("All    ");
			else if(menu.getRoutesBlundering())
				sb.append("Blundering    ");
			else if(menu.getRoutesConforming())
				sb.append("Conforming    ");
			else if(menu.getRoutesSelected())
				sb.append("Selected    ");
			else if(menu.getRoutesNone())
				sb.append("None    ");

			sb.append("Displaying trajectories:  ");
			if(menu.getTrajsAll())
				sb.append("All\n");
			else if(menu.getTrajsBlundering())
				sb.append("Blundering\n");
			else if(menu.getTrajsConforming())
				sb.append("Conforming\n");
			else if(menu.getTrajsNone())
				sb.append("None\n");
			else if(menu.getTrajsSelected())
				sb.append("Selected\n");
			else if(menu.getTrajsWithPlan())
				sb.append("With Flight Plan\n");

			sb.append("   Selected flights:  ");
			FlightList fl = window.getFlightList();
			Collection flights = fl.getSelectedFlights();
			if(flights.size() == 0)
				sb.append("None\n");
			else {
				Iterator itr = flights.iterator();
				while(itr.hasNext()) {
					Flight f = (Flight)itr.next();
					sb.append(f.getAircraftId());
					if(itr.hasNext())
						sb.append("  ");
				}
				sb.append("\n");
			}
			
			NumberFormat nf = NumberFormat.getNumberInstance();
			nf.setMinimumFractionDigits(2);
			nf.setMaximumFractionDigits(2);
			
			sb.append("   Lateral Threshold:  ");
			if(uParams.cmLateralWeightOn)
				sb.append(nf.format(uParams.cmLateralThreshold));
			else
				sb.append(nf.format(uParams.cmLateralThreshold)+" (disabled)");
			sb.append("    ");
			sb.append("Vertical Theshold:  ");
			if(uParams.cmVerticalWeightOn)
				sb.append(nf.format(uParams.cmVerticalThreshold));
			else
				sb.append(nf.format(uParams.cmVerticalThreshold)+" (disabled)");
			sb.append("    ");
			sb.append("Angular Threshold:  ");
			if(uParams.cmAngularWeightOn)
				sb.append(nf.format(uParams.cmAngularThreshold));
			else
				sb.append(nf.format(uParams.cmAngularThreshold)+" (disabled)");
			sb.append("\n");
			sb.append("   Speed Threshold:  ");
			if(uParams.cmSpeedWeightOn)
				sb.append(nf.format(uParams.cmSpeedThreshold));
			else
				sb.append(nf.format(uParams.cmSpeedThreshold)+" (disabled)");
			sb.append("    ");
			sb.append("Residual Threshold:  ");
			sb.append(nf.format(uParams.cmResidualThreshold));
			sb.append("    ");
			sb.append("Time Horizon:  ");
			sb.append(new Long(uParams.tsTimeHorizon).toString());
			sb.append("\n");
			displayPane.append(sb.toString());
		}

		String usage() {
			return "Usage: options";
		}
		
	}

	/////////////////////////
	class ExitCommand extends CommandParser {
		ExitCommand() {
			super("exit",1,1);
		}
		
		void parse(String[] args) throws InvalidInputParseException {
			super.parse(args);
			menu.toggleTextualWindow();
		}
		
		/*
		String usage() {
			return "Usage: exit";
		}
		*/
	}
}
