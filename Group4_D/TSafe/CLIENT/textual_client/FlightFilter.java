/*
 * Created on Dec 3, 2004
 *
 */
package tsafe.client.textual_client;

import java.awt.Color;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;

import tsafe.client.graphical_client.FlightMap;
import tsafe.common_datastructures.Flight;
import tsafe.common_datastructures.FlightPlan;
import tsafe.common_datastructures.LatLonBounds;
import tsafe.common_datastructures.Trajectory;

/**
 * @author sscout
 *
 */
class FlightFilter {

    private static final Color BLUNDER_COLOR  = Color.red;
    private static final Color CONFORM_COLOR  = Color.white;
	
	boolean needUpdate;
	
	private Collection blunders = new LinkedList();
	/**
	 * The bounds outlining the visible region
	 */
	private LatLonBounds bounds;
	/**
	 * Data to draw to the screen
	 */
	private Collection fixes;
	private Map flight2TrajMap = new HashMap();
	private Collection flights  = new LinkedList();
	/**
	 * The selected flights
	 */
	private Collection selectedFlights = new Vector();
	/**
     * Flags triggering the display of certain flight pane items
     */
    private int showFixes  = FlightMap.SHOW_ALL;
    /**
     * Flags triggering the display of certain flight pane items
     */
    private int showFlights      = FlightMap.SHOW_ALL;
    /**
     * Flags triggering the display of certain flight pane items
     */
    private int showRoutes = FlightMap.SHOW_ALL;
    /**
     * Flags triggering the display of certain flight pane items
     */
    private int showTrajectories = FlightMap.SHOW_ALL;

	/**
     * Construct a flight map with a given map image, bounds, and fixes
     */
    public FlightFilter(LatLonBounds bounds, Collection fixes) {
        this.bounds   = bounds;
        this.fixes    = fixes;
    }
	
	/** Sets the blunders in the pane */
	public void setBlunders(Collection blunders) {
	    this.blunders = new LinkedList(blunders);
	}

	/** Sets the flights in the pane */
	public void setFlights(Collection flights) {
	    this.flights = new LinkedList(flights);
	}

	/** Sets the flight trajectory map in the pane */
	public void setFlightTrajectoryMap(Map flight2TrajMap) {
	    this.flight2TrajMap = new HashMap(flight2TrajMap);
	}

	/**
	 * Set the selected flight
	 */
	public void setSelectedFlights(Collection selectedFlights) {
	    this.selectedFlights = selectedFlights;
	}

	/** Turns on or off the painting of fixes */
	public void setShowFixes(int showFixes) {
	    this.showFixes = showFixes;
	}

	/** Turns on or off the painting of flights */
	public void setShowFlights(int showFlights) {
	    this.showFlights = showFlights;
	}

	/** Turns on or off the painting of routes */
	public void setShowRoutes(int showRoutes) {
	    this.showRoutes = showRoutes;
	}

	/** Turns on or off the painting of trajectories */
	public void setShowTrajectories(int showTrajectories) {
	    this.showTrajectories = showTrajectories;
	}

    public void printUpdate(StickyBottomScrollPane outputPane) {
		
        /**
         * Draw Flights
         */
        Iterator flightIter = flights.iterator();
        while(flightIter.hasNext()) {
            
            Flight flight = (Flight)flightIter.next();
            boolean hasFlightPlan = flight.getFlightPlan() != null;
            boolean isBlundering = hasFlightPlan && blunders.contains(flight);
            boolean isConforming = hasFlightPlan && !isBlundering;
            boolean isSelected = selectedFlights.contains(flight);

            // Draw the flight if . . .
            if ((showFlights == FlightMap.SHOW_ALL) ||
                (showFlights == FlightMap.SHOW_WITH_PLAN  && hasFlightPlan) ||
                (showFlights == FlightMap.SHOW_CONFORMING && isConforming) ||
                (showFlights == FlightMap.SHOW_BLUNDERING && isBlundering) ||
                (showFlights == FlightMap.SHOW_SELECTED   && isSelected)) {

            	Color color;
            	
                /** Set the color of the flight and draw it */
                if (isBlundering) color = BLUNDER_COLOR;
                else color = CONFORM_COLOR;
                printFlight(outputPane, flight, color);
    
                // Draw the route if . . .
                if (hasFlightPlan &&
                    ((showRoutes == FlightMap.SHOW_ALL) ||
                     (showRoutes == FlightMap.SHOW_CONFORMING && isConforming) ||
                     (showRoutes == FlightMap.SHOW_BLUNDERING && isBlundering) ||
                     (showRoutes == FlightMap.SHOW_SELECTED   && isSelected))) {
                    /** Set the color of the route and draw it */
                    printPlan(outputPane, flight.getFlightPlan());
                }

                // Draw the trajectory if . . .
                if ((showTrajectories == FlightMap.SHOW_ALL) ||
                    (showTrajectories == FlightMap.SHOW_WITH_PLAN  && hasFlightPlan) ||
                    (showTrajectories == FlightMap.SHOW_CONFORMING && isConforming)  ||
                    (showTrajectories == FlightMap.SHOW_BLUNDERING && isBlundering)  ||
                    (showTrajectories == FlightMap.SHOW_SELECTED   && isSelected)) {
                     /** Set the color of the trajectory and draw it */
                    printTrajectory(outputPane, (Trajectory)flight2TrajMap.get(flight));
                }
                
                if(isBlundering) {
                	outputPane.append("Conformance Status:\t");
                	outputPane.append("BLUNDERING\n",Color.red);
                }
                outputPane.append(" \n");
            }
        }
    }
	
    private void printFlight(StickyBottomScrollPane outputPane, Flight f, Color color) {
    	outputPane.append("Flight: ",Color.white,Color.black,true,16);
    	outputPane.append(f.getAircraftId(),color,Color.black,true,16);
    	outputPane.append("\t");
    	outputPane.append(f.getFlightTrack().toString());
    	outputPane.append("\n");
    }

    private void printPlan(StickyBottomScrollPane outputPane, FlightPlan p) {
    	outputPane.append("Flight plan:\t\t");
        outputPane.append(p.toString());
        outputPane.append("\n");
    	outputPane.append("\t\t");
        outputPane.append(p.getRoute().toString());
        outputPane.append("\n");
    }
    
    private void printTrajectory(StickyBottomScrollPane outputPane, Trajectory t) {
    	outputPane.append("Trajectory:\t\t");
        outputPane.append(t.toString());
        outputPane.append("\n");
    }
	
	/**
	 * Tells the flight pane its stored flight data
	 * has changed and must perform an updated repaint
	 */
	public void updateNeeded() {
	    this.needUpdate = true;
	}

	
}
