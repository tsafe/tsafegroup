/*
 * Created on Dec 4, 2004
 *
 */
package tsafe.client;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JCheckBoxMenuItem;

/**
 * @author jnagy
 *
 */
public class ReplicatableJCheckBoxMenuItem extends JCheckBoxMenuItem 
											implements ItemListener {
	
	LinkedList items = null;
	
	ReplicatableJCheckBoxMenuItem(LinkedList items,String text) {
		super(text);
		this.items = items;
		items.add(this);
		addItemListener(this);
	}

	protected void finalize() throws Throwable {
		if(!items.remove(this))
			System.out.println("Could not clean up "+this.getName());
		super.finalize();
	}
	

    public void itemStateChanged(ItemEvent e) {
        JCheckBoxMenuItem mi = (JCheckBoxMenuItem)(e.getSource());

        //System.out.println("caught change in "+mi.getClass());
        //System.out.println("items size = "+items.size());
        
        Iterator itr = items.iterator();
        while(itr.hasNext()) {
        	JCheckBoxMenuItem otherItem = (JCheckBoxMenuItem)itr.next();
        	if(otherItem != mi) {
        		if(otherItem.isSelected() != mi.isSelected()) {
        			otherItem.setSelected(mi.isSelected());
	        		//System.out.println("Toggled "+otherItem);
        		}
        	}
        }
    }
}
