/*
 * Created on Aug 26, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package tsafe.client.graphical_client;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.Collection;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;

import tsafe.client.TsafeMenu;
import tsafe.client.WindowInterface;
import tsafe.common_datastructures.LatLonBounds;
import tsafe.common_datastructures.TSAFEProperties;
import tsafe.common_datastructures.client_server_communication.ComputationResults;

/**
 * @author cackermann
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class GraphicalWindow extends WindowInterface {

	/**
	 * Location of split pane divider
	 */
	private static final int SPLIT_DIVIDER_LOCATION = 100;


	/**
	 * Map on which flight data, fixes, etc are drawn
	 */
	protected FlightMap flightMap;
	
	/**
	 *  
	 */
	public GraphicalWindow(GraphicalClient client) {
		super(client, "TSAFE");
		
		// Create the background image.
		Image bgImage = Toolkit.getDefaultToolkit().getImage(
				TSAFEProperties.getBackgroundImage());

		// Create a flight map
		this.flightMap = makeFlightMap(bgImage, this.client.getBounds(),
				this.client.getFixes());

		// Build the content pane
		setJMenuBar(makeTsafeMenu());		
	
		//Create a list of flights and add the client as a listener
		this.flightList = FlightList.getFlightList();
		flightList.addListSelectionListener(this);

		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				new JScrollPane(flightList), flightMap);
		splitPane.setDividerLocation(SPLIT_DIVIDER_LOCATION);
		Dimension mapDim = flightMap.getPreferredSize();
		splitPane.setPreferredSize(new Dimension(
				(int) (mapDim.getWidth() + SPLIT_DIVIDER_LOCATION),
				(int) mapDim.getHeight()));
		super.setContentPane(splitPane);
		
	}

	/**
	 * Called by TsafeMenu menu when recognizes menu events
	 */
	public void showMenuChanged(String submenu, String submenuItem) {
		int showOption = showTextToOption(submenuItem);

		if (submenu.equals(TsafeMenu.FIXES_TEXT))
			flightMap.setShowFixes(showOption);

		else if (submenu.equals(TsafeMenu.FLIGHTS_TEXT))
			flightMap.setShowFlights(showOption);

		else if (submenu.equals(TsafeMenu.ROUTES_TEXT))
			flightMap.setShowRoutes(showOption);

		else if (submenu.equals(TsafeMenu.TRAJS_TEXT))
			flightMap.setShowTrajectories(showOption);

		flightMap.updateNeeded();
		repaint();
	}

	/**
	 * List selection event handler When the selected item has changed,
	 */
	public void valueChanged(ListSelectionEvent e) {
		Collection selectedFlights = flightList.getSelectedFlights();
		flightMap.setSelectedFlights(selectedFlights);
		flightMap.updateNeeded();
		repaint();
	}
	
	
	private void refreshWindow() {
		// Parameters changed, update flight map
		flightMap.updateNeeded();
		flightMap.repaint();
	}

	private static FlightMap makeFlightMap(Image mapImage, LatLonBounds bounds,
			Collection fixes) {
		FlightMap flightMap = new FlightMap(mapImage, bounds, fixes);
		flightMap.setShowFixes(INITIAL_SHOW_FIXES_OPTION);
		flightMap.setShowFlights(INITIAL_SHOW_FLIGHTS_OPTION);
		flightMap.setShowRoutes(INITIAL_SHOW_ROUTES_OPTION);
		flightMap.setShowTrajectories(INITIAL_SHOW_TRAJS_OPTION);
		return flightMap;
	}


	public void updateWindow(ComputationResults results) {

		flightList.setFlights(results.getFlights());

		// Update the flight map
		synchronized (flightMap) {
			flightMap.setFlights(results.getFlights());
			flightMap.setBlunders(results.getBlunders());
			flightMap.setFlightTrajectoryMap(results.getFlight2TrajectoryMap());
		}

		flightMap.updateNeeded();
		repaint();
	}

}
