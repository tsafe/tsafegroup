package tsafe.client;

import java.util.Collection;

import tsafe.common_datastructures.LatLonBounds;
import tsafe.common_datastructures.client_server_communication.ComputationResults;
import tsafe.common_datastructures.client_server_communication.UserParameters;
import tsafe.server.ServerInterface;

/**
 * @author Christopher Ackermann
 * 
 * An Interface for clients that provides the methods, necessary in order to
 * communicate with the server component. All comunication to classes in the
 * client component must be done through this class.
 */
public abstract class ClientInterface extends Thread {

	//	*** Interface attributes **************************************
	/**
	 * Handle to the interface class of the server, necessary for the
	 * communication between client and server.
	 */
	protected ServerInterface server;

	/**
	 * Reference to this client's partner
	 */
	protected ClientInterface partner;

	/**
	 * The bounds for the area within the client is supposed to show the
	 * flights.
	 */
	protected LatLonBounds bounds;

	/**
	 * Stores the parameters such as thresholds that can be changed by the user
	 * and that are used for calculating the flights.
	 */
	protected UserParameters parameters;

	/**
	 * The graphical main user interface
	 */
	protected WindowInterface window;

	//****************************************************************

	public ClientInterface(ServerInterface server) {
		this.server = server;
		this.parameters = server.getUserParameters();
		this.bounds = new LatLonBounds(0,0,0,0);
		this.partner = null;
	}

	/**
	 * Recieves the notification from the client after the timer goes off.
	 * 
	 * @see tsafe.client.ClientInterface#notifyClient()
	 */
	public void notifyClient() {
		// Get the new flight data after the notification.
		this.getFlightData();
	}

	/**
	 * Sets the bounds and starts the client.
	 */
	public void setBounds(LatLonBounds bounds) {
		this.bounds = bounds;
	}

	/**
	 * Queries the server for new flight data.
	 * 
	 * @see tsafe.client.ClientInterface#getFlightData()
	 */
	public void getFlightData() {

		ComputationResults results = this.server.getFlightData(this.bounds,
				this.parameters);
		updateClient(results);

	}

	/**
	 * @return Returns the parameters.
	 */
	public UserParameters getParameters() {
		return parameters;
	}

	public abstract void updateClient(ComputationResults results);

	/**
	 * Interface method for the client for reading the fixes.
	 * 
	 * @return All fixes, stored in the database.
	 */
	public Collection getFixes() {
		return this.server.getFixes();
	}

	/**
	 * @return Returns the server.
	 */
	public ServerInterface getServer() { return server; }

	/**
	 * @param server
	 *            The server to set.
	 */
	public void setServer(ServerInterface server) { this.server = server; }

	/**
	 * @return Returns the bounds.
	 */
	public LatLonBounds getBounds() { return bounds; }

	/**
	 * @author sscout
	 * @return Returns the window.
	 */
	public WindowInterface getWindow() { return window; }

	/**
	 * @author sscout
	 * @return Returns the partner client.
	 */
	public ClientInterface getPartner() { return partner; }

	/**
	 * @author sscout
	 *
	 * @param ClientInterface partner
	 * 		the textual client associated with this graphical client
	 */
	public void setPartner(ClientInterface partner) { this.partner = partner; }

}