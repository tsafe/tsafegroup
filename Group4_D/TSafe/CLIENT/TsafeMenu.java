/*
 TSAFE Prototype: A decision support tool for air traffic controllers
 Copyright (C) 2003  Gregory D. Dennis

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package tsafe.client;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Enumeration;
import java.util.LinkedList;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


/**
 * The menu bar for the Tsafe client
 */
public class TsafeMenu extends JMenuBar implements ItemListener {

	/**
	 * Show submenus
	 */
	public static final String FIXES_TEXT = "Fixes";

	public static final String FLIGHTS_TEXT = "Flights";

	public static final String ROUTES_TEXT = "Routes";

	public static final String TRAJS_TEXT = "Trajectories";

	/**
	 * Show Submenu items
	 */
	public static final String ALL_TEXT = "all";

	public static final String SELECTED_TEXT = "selected";

	public static final String WITH_PLAN_TEXT = "with flight plans";

	public static final String CONFORMING_TEXT = "conforming";

	public static final String BLUNDERING_TEXT = "blundering";

	public static final String NONE_TEXT = "none";

	/**
	 * Parameters Menu items
	 */
	public static final String CONF_MONITOR_TEXT = "Conformance Monitor . . .";

	public static final String TRAJ_SYNTH_TEXT = "Trajectory Synthesizer . . .";

	/**
	 * Client type selection 
	 */	
	public static final String DISPLAY_GRAPHIC_CLIENT_TEXT = "Graphical Client";
	
	public static final String DISPLAY_TEXT_CLIENT_TEXT = "Textual Client";
	
	/**
	 * Tsafe Client
	 */
	private WindowInterface window;

	/**
	 * Submenu Items
	 */
	private JMenuItem fixesAll, fixesNone;

	private JMenuItem flightsAll, flightsSelected, flightsWithPlan,
			flightsConforming, flightsBlundering, flightsNone;

	private JMenuItem routesAll, routesSelected, routesConforming,
			routesBlundering, routesNone;

	private JMenuItem trajsAll, trajsSelected, trajsWithPlan, trajsConforming,
			trajsBlundering, trajsNone;

	
	/*
	 * Global display Menu Items list
	 */
	private static LinkedList gDisplayMenuItems = new LinkedList();
	private static LinkedList tDisplayMenuItems = new LinkedList();
	
	/*
	 * Global show Menu Items List
	 */
	private static LinkedList fixesAllItems = new LinkedList(), fixesNoneItems = new LinkedList();

	private static LinkedList flightsAllItems = new LinkedList(), flightsSelectedItems = new LinkedList(), flightsWithPlanItems = new LinkedList(),
			flightsConformingItems = new LinkedList(), flightsBlunderingItems = new LinkedList(), flightsNoneItems = new LinkedList();

	private static LinkedList routesAllItems = new LinkedList(), routesSelectedItems = new LinkedList(), routesConformingItems = new LinkedList(),
			routesBlunderingItems = new LinkedList(), routesNoneItems = new LinkedList();

	private static LinkedList trajsAllItems = new LinkedList(), trajsSelectedItems = new LinkedList(), trajsWithPlanItems = new LinkedList(), trajsConformingItems = new LinkedList(),
			trajsBlunderingItems = new LinkedList(), trajsNoneItems = new LinkedList();
	
	
	/**
	 * Constructs the TsafeMenu
	 */
	public TsafeMenu(WindowInterface window, String initialFixesText,
			String initialFlightsText, String initialRoutesText,
			String initialTrajectoriesText) {
		super();
		this.window = window;
		super.add(makeShowMenu(initialFixesText, initialFlightsText,
				initialRoutesText, initialTrajectoriesText));
		super.add(makeParametersMenu(window));
		super.add(makeClientSelectMenu(window));
	}
	
	/**
	 * Constructs the TsafeMenu
	 */
	/*
	private static TsafeMenu menu;

	public static TsafeMenu getTsafeMenu(WindowInterface window, String initialFixesText,
			String initialFlightsText, String initialRoutesText,
			String initialTrajectoriesText) {
		if(menu == null) {
			menu = new TsafeMenu(window, initialFixesText,
					initialFlightsText, initialRoutesText,
					initialTrajectoriesText);
		}
			
		return menu;
	}
	*/
	
	/**
	 * Handles the menu events
	 */
	public void itemStateChanged(ItemEvent e) {
		Object item = e.getItem();
		String text = ((JMenuItem) e.getItem()).getText();

		if (item.equals(fixesAll) || item.equals(fixesNone)) {
			window.showMenuChanged(FIXES_TEXT, text);
		} else if (item.equals(flightsAll) || item.equals(flightsSelected)
				|| item.equals(flightsWithPlan)
				|| item.equals(flightsConforming)
				|| item.equals(flightsBlundering) || item.equals(flightsNone)) {
			window.showMenuChanged(FLIGHTS_TEXT, text);
		} else if (item.equals(routesAll) || item.equals(routesSelected)
				|| item.equals(routesConforming)
				|| item.equals(routesBlundering) || item.equals(routesNone)) {
			window.showMenuChanged(ROUTES_TEXT, text);
		} else if (item.equals(trajsAll) || item.equals(trajsSelected)
				|| item.equals(trajsWithPlan) || item.equals(trajsConforming)
				|| item.equals(trajsBlundering) || item.equals(trajsNone)) {
			window.showMenuChanged(TRAJS_TEXT, text);
		} else {
			throw new RuntimeException("Invalid Menu Item Changed");
		}
	}

	/**
	 * Construct the show menu
	 */
	private JMenu makeShowMenu(String initialFixesText,
			String initialFlightsText, String initialRoutesText,
			String initialTrajectoriesText) {
		// Make Show submenus
		JMenu fixes = makeFixesSubmenu(initialFixesText);
		JMenu flights = makeFlightsSubmenu(initialFlightsText);
		JMenu routes = makeRoutesSubmenu(initialRoutesText);
		JMenu trajs = makeTrajectoriesSubmenu(initialTrajectoriesText);

		// Make Show Menu
		JMenu showMenu = new JMenu("Show");
		showMenu.add(fixes);
		showMenu.add(flights);
		showMenu.add(routes);
		showMenu.add(trajs);

		return showMenu;
	}

	/**
	 * Construct the fixes submenu
	 */
	private JMenu makeFixesSubmenu(String initialText) {
		// Make the radio button menu items
		fixesAll = new ReplicatableJRadioButtonMenuItem(fixesAllItems,ALL_TEXT);
		fixesNone = new ReplicatableJRadioButtonMenuItem(fixesNoneItems, NONE_TEXT);

		// Add thiss to the buttons
		fixesAll.addItemListener(this);
		fixesNone.addItemListener(this);

		// Make a button group out of the buttons
		ButtonGroup bg = new ButtonGroup();
		bg.add(fixesAll);
		bg.add(fixesNone);
		setSelectedButton(bg, initialText);

		// Make the flights submenu
		JMenu fixes = new JMenu(FIXES_TEXT);
		fixes.add(fixesAll);
		fixes.add(fixesNone);
		return fixes;
	}

	/**
	 * Construct the flights submenu
	 */
	private JMenu makeFlightsSubmenu(String initialText) {
		// Make the radio button menu items
		flightsAll = new ReplicatableJRadioButtonMenuItem(flightsAllItems, ALL_TEXT);
		flightsSelected = new ReplicatableJRadioButtonMenuItem(flightsSelectedItems, SELECTED_TEXT);
		flightsWithPlan = new ReplicatableJRadioButtonMenuItem(flightsWithPlanItems, WITH_PLAN_TEXT);
		flightsConforming = new ReplicatableJRadioButtonMenuItem(flightsConformingItems, CONFORMING_TEXT);
		flightsBlundering = new ReplicatableJRadioButtonMenuItem(flightsBlunderingItems, BLUNDERING_TEXT);
		flightsNone = new ReplicatableJRadioButtonMenuItem(flightsNoneItems, NONE_TEXT);

		// Add thiss to the buttons
		flightsAll.addItemListener(this);
		flightsSelected.addItemListener(this);
		flightsWithPlan.addItemListener(this);
		flightsConforming.addItemListener(this);
		flightsBlundering.addItemListener(this);
		flightsNone.addItemListener(this);

		// Make a button group out of the buttons
		ButtonGroup bg = new ButtonGroup();
		bg.add(flightsAll);
		bg.add(flightsSelected);
		bg.add(flightsWithPlan);
		bg.add(flightsConforming);
		bg.add(flightsBlundering);
		bg.add(flightsNone);
		setSelectedButton(bg, initialText);

		// Make the flights submenu
		JMenu flights = new JMenu(FLIGHTS_TEXT);
		flights.add(flightsAll);
		flights.add(flightsSelected);
		flights.add(flightsWithPlan);
		flights.add(flightsConforming);
		flights.add(flightsBlundering);
		flights.add(flightsNone);
		return flights;
	}

	/**
	 * Construct the routes submenu
	 */
	private JMenu makeRoutesSubmenu(String initialText) {
		// Make the radio button menu items
		routesAll = new ReplicatableJRadioButtonMenuItem(routesAllItems, ALL_TEXT);
		routesSelected = new ReplicatableJRadioButtonMenuItem(routesSelectedItems, SELECTED_TEXT);
		routesConforming = new ReplicatableJRadioButtonMenuItem(routesConformingItems, CONFORMING_TEXT);
		routesBlundering = new ReplicatableJRadioButtonMenuItem(routesBlunderingItems, BLUNDERING_TEXT);
		routesNone = new ReplicatableJRadioButtonMenuItem(routesNoneItems, NONE_TEXT);

		// Add thiss to the buttons
		routesAll.addItemListener(this);
		routesSelected.addItemListener(this);
		routesConforming.addItemListener(this);
		routesBlundering.addItemListener(this);
		routesNone.addItemListener(this);

		// Make a button group out of the buttons
		ButtonGroup bg = new ButtonGroup();
		bg.add(routesAll);
		bg.add(routesSelected);
		bg.add(routesConforming);
		bg.add(routesBlundering);
		bg.add(routesNone);
		setSelectedButton(bg, initialText);

		// Make the routes submenu
		JMenu routes = new JMenu(ROUTES_TEXT);
		routes.add(routesAll);
		routes.add(routesSelected);
		routes.add(routesConforming);
		routes.add(routesBlundering);
		routes.add(routesNone);
		return routes;
	}

	/**
	 * Construct the trajectories submenu
	 */
	private JMenu makeTrajectoriesSubmenu(String initialText) {
		// Make the radio button menu items
		trajsAll = new ReplicatableJRadioButtonMenuItem(trajsAllItems,ALL_TEXT);
		trajsSelected = new ReplicatableJRadioButtonMenuItem(trajsSelectedItems,SELECTED_TEXT);
		trajsWithPlan = new ReplicatableJRadioButtonMenuItem(trajsWithPlanItems,WITH_PLAN_TEXT);
		trajsConforming = new ReplicatableJRadioButtonMenuItem(trajsConformingItems, CONFORMING_TEXT);
		trajsBlundering = new ReplicatableJRadioButtonMenuItem(trajsBlunderingItems, BLUNDERING_TEXT);
		trajsNone = new ReplicatableJRadioButtonMenuItem(trajsNoneItems, NONE_TEXT);

		// Add thiss to the buttons
		trajsAll.addItemListener(this);
		trajsSelected.addItemListener(this);
		trajsWithPlan.addItemListener(this);
		trajsConforming.addItemListener(this);
		trajsBlundering.addItemListener(this);
		trajsNone.addItemListener(this);

		// Make a button group out of the buttons
		ButtonGroup bg = new ButtonGroup();
		bg.add(trajsAll);
		bg.add(trajsSelected);
		bg.add(trajsWithPlan);
		bg.add(trajsConforming);
		bg.add(trajsBlundering);
		bg.add(trajsNone);
		setSelectedButton(bg, initialText);

		// Make the trajectories submenu
		JMenu trajectories = new JMenu(TRAJS_TEXT);
		trajectories.add(trajsAll);
		trajectories.add(trajsSelected);
		trajectories.add(trajsWithPlan);
		trajectories.add(trajsConforming);
		trajectories.add(trajsBlundering);
		trajectories.add(trajsNone);
		return trajectories;
	}

	/**
	 * Selects the button with the given text in the button group
	 */
	private static void setSelectedButton(ButtonGroup bg, String text) {
		Enumeration buttonEnum = bg.getElements();

		while (buttonEnum.hasMoreElements()) {
			AbstractButton button = (AbstractButton) buttonEnum.nextElement();
			if (button.getText().equals(text)) {
				button.setSelected(true);
				return;
			}
		}

		throw new RuntimeException("Selected button not found: " + text);
	}

	/**
	 * Construct the parameters menu
	 */
	private static JMenu makeParametersMenu(WindowInterface window) {
		// Make Preferences Menu Items
		JMenuItem confMonitor = new JMenuItem(CONF_MONITOR_TEXT);
		JMenuItem trajSynth = new JMenuItem(TRAJ_SYNTH_TEXT);

		// Add Preferences Menu Item Listeners
		confMonitor.addActionListener(window);
		trajSynth.addActionListener(window);

		// Make Preferences Menu
		JMenu prefMenu = new JMenu("Parameters");
		prefMenu.add(confMonitor);
		prefMenu.add(trajSynth);
		
		return prefMenu;
	}

	ReplicatableJCheckBoxMenuItem displayGraphicSelection;
	ReplicatableJCheckBoxMenuItem displayTextSelection;
	
	/**
	 *  Client selection menu
	 * @author sscout
	 *
	 */

	private JMenu makeClientSelectMenu(WindowInterface window) {
		displayGraphicSelection = new ReplicatableJCheckBoxMenuItem(gDisplayMenuItems,DISPLAY_GRAPHIC_CLIENT_TEXT);
		displayTextSelection = new ReplicatableJCheckBoxMenuItem(tDisplayMenuItems,DISPLAY_TEXT_CLIENT_TEXT);

		displayGraphicSelection.setSelected(window.client.getParameters().displayGraphicalClient);
		displayTextSelection.setSelected(window.client.getParameters().displayTextualClient);

		displayGraphicSelection.addActionListener(window);
		displayTextSelection.addActionListener(window);
		
		JMenu displayMenu = new JMenu("Display");
		displayMenu.add(displayGraphicSelection);
		displayMenu.add(displayTextSelection);
		
		return displayMenu;
	}

	public void toggleGraphicalWindow() {
		displayGraphicSelection.doClick();
	}
	
	public void toggleTextualWindow() {
		displayTextSelection.doClick();
	}
	
	public void toggleGraphicalWindowMenuItem() {
		displayGraphicSelection.setSelected(!displayGraphicSelection.isSelected());
	}
	
	public void toggleTextualWindowMenuItem() {
		displayTextSelection.setSelected(!displayTextSelection.isSelected());
	}
	
	public void setFixesAll() {
		fixesAll.doClick();
	}

	public void setFixesNone() {
		fixesNone.doClick();
	}
	
	public void setFlightsAll() {
		flightsAll.doClick();
	}
	
	public void setFlightsSelected() {
		flightsSelected.doClick();
	}
	
	public void setFlightsWithPlan() {
		flightsWithPlan.doClick() ;
	}
	
	public void setFlightsConforming() {
		flightsConforming.doClick() ;
	}
	
	public void setFlightsBlundering() {
		flightsBlundering.doClick() ;
	}
	
	public void setFlightsNone() {
		flightsNone.doClick() ;
	}
	
	public void setRoutesAll() {
		routesAll.doClick() ;
	}
	
	public void setRoutesSelected() {
		routesSelected.doClick() ;
	}
	
	public void setRoutesConforming() {
		routesConforming.doClick() ;
	}
	
	public void setRoutesBlundering() {
		routesBlundering.doClick() ;
	}
	
	public void setRoutesNone() {
		routesNone.doClick() ;
	}

	public void setTrajsAll() {
		trajsAll.doClick() ;
	}
	
	public void setTrajsSelected() {
		trajsSelected.doClick() ;
	}
	
	public void setTrajsConforming() {
		trajsConforming.doClick() ;
	}
	
	public void setTrajsBlundering() {
		trajsBlundering.doClick() ;
	}
	
	public void setTrajsNone() {
		trajsNone.doClick() ;
	}
	
	public void setTrajsWithPlan() {
		trajsWithPlan.doClick() ;
	}
	
	public boolean getFixesAll() { return fixesAll.isSelected(); }
	public boolean getFixesNone() {	return fixesNone.isSelected(); }
	
	public boolean getFlightsAll() { return flightsAll.isSelected(); } 
	public boolean getFlightsSelected() { return flightsSelected.isSelected(); } 
	public boolean getFlightsWithPlan() { return flightsWithPlan.isSelected(); }
	public boolean getFlightsConforming() { return flightsConforming.isSelected(); } 
	public boolean getFlightsBlundering() { return flightsBlundering.isSelected(); } 
	public boolean getFlightsNone() { return flightsNone.isSelected(); } 

	public boolean getRoutesAll() { return routesAll.isSelected(); } 
	public boolean getRoutesSelected() { return routesSelected.isSelected(); } 
	public boolean getRoutesConforming() { return routesConforming.isSelected(); }
	public boolean getRoutesBlundering() { return routesBlundering.isSelected(); } 
	public boolean getRoutesNone() { return routesNone.isSelected(); } 

 	public boolean getTrajsAll() { return trajsAll.isSelected(); } 
 	public boolean getTrajsSelected() { return trajsSelected.isSelected(); } 
 	public boolean getTrajsWithPlan() { return trajsWithPlan.isSelected(); } 
 	public boolean getTrajsConforming() { return trajsConforming.isSelected(); }
	public boolean getTrajsBlundering() { return trajsBlundering.isSelected(); } 
	public boolean getTrajsNone() { return trajsNone.isSelected(); } 

}
