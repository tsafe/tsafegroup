/*
 TSAFE Prototype: A decision support tool for air traffic controllers
 Copyright (C) 2003  Gregory D. Dennis

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package tsafe.common_datastructures;

import java.text.NumberFormat;

/**
 * A 2-Dimensional point with latitude and longitude components
 * This data structure is immutable
 */
public class Point2D {

    private double lat, lon;

    public Point2D(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLatitude()  {return lat;}
    public double getLongitude() {return lon;}
    
    public String toString() {
    	StringBuffer sb = new StringBuffer();
    	
    	char dirNS = (lat >= 0)?'N':'S';
    	char dirEW = (lon >= 0)?'E':'W';
    	double latTemp = Math.abs(lat);
    	double lonTemp = Math.abs(lon);
    	int latDegrees = (int)latTemp;
    	int lonDegrees = (int)lonTemp;
    	latTemp -= (double)latDegrees;
    	lonTemp -= (double)lonDegrees;
    	int latMinutes = (int)(latTemp*60.0);
    	int lonMinutes = (int)(lonTemp*60.0);
    	
    	NumberFormat minFormat = NumberFormat.getIntegerInstance();
    	minFormat.setMinimumIntegerDigits(2);
    	
    	sb.append(latDegrees);
    	sb.append("\u00b0 ");
    	sb.append(minFormat.format(latMinutes));
    	sb.append("\' ");
    	sb.append(dirNS);
    	sb.append(" ");
    	sb.append(lonDegrees);
    	sb.append("\u00b0 ");
    	sb.append(minFormat.format(lonMinutes));
    	sb.append("\' ");
    	sb.append(dirEW);
    	return sb.toString();
    }
}
