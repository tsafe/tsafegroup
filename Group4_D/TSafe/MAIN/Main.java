/*
 TSAFE Prototype: A decision support tool for air traffic controllers
 Copyright (C) 2003  Gregory D. Dennis

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package tsafe.main;

import java.awt.EventQueue;
import java.awt.Toolkit;

import tsafe.client.ClientInterface;
import tsafe.client.graphical_client.GraphicalClient;
import tsafe.client.textual_client.TextualClient;
import tsafe.common_datastructures.TSAFEProperties;
import tsafe.server.ServerInterface;
import tsafe.server.server_gui.SplashScreen;
import tsafe.server.server_gui.utils.WaitCursorEventQueue;

/**
 * The TSAFE configuration console.
 */
public class Main {

	//
	// MAIN METHOD
	//

	//-------------------------------------------
	public static void main(String args[]) {
		
		//Test Zone
//		System.out.println("test ---------------------") ;
//		String input = new String("select sAF2");
//		InputParser i = new InputParser() ;
//		i.parse(input) ;

		ServerInterface server;
		
		long hideSplashTime = 0;
		
		// CASE #1: Startup using splash screen.
		if (TSAFEProperties.getShowSplashScreenFlag()) {

			// Show the splash screen (for a minimum of 3.3 seconds).
			hideSplashTime = System.currentTimeMillis() + 3300;
			SplashScreen.show();
		}

			// Start the server.
			server = new ServerInterface();
			
			ClientInterface graphicalClient = null;
			ClientInterface textualClient = null;
			
			
			// Create the clients and, if creating both, link them.
			if(server.getUserParameters().displayGraphicalClient) {
				graphicalClient = new GraphicalClient(server);
				server.attachObserver(graphicalClient);
			}
			if(server.getUserParameters().displayTextualClient) {
				textualClient = new TextualClient(server);
				server.attachObserver(textualClient);
				textualClient.setPartner(graphicalClient);
				
				if(graphicalClient != null)
					((GraphicalClient)graphicalClient).setPartner(textualClient);
			}

			if (TSAFEProperties.getShowSplashScreenFlag()) {
			// Wait for time to expire.
			while (System.currentTimeMillis() < hideSplashTime) {
			}

			// Show the console.

			SplashScreen.hide();
		}
		
		
		// Install the Event Queue decorator to automatically change the cursor
		// to an hourglass if a GUI-event takes too much time (exceeds 5
		// seconds).
		EventQueue waitQueue = new WaitCursorEventQueue(500);
		Toolkit.getDefaultToolkit().getSystemEventQueue().push(waitQueue);
	}

}